<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Regression Test - RRU</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>60</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>67198811-7ee6-4df9-a5e3-55439d6184d6</testSuiteGuid>
   <testCaseLink>
      <guid>bb041401-a87d-4115-b2b7-430a89ba3e74</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 12/0071. GA - MA - GL Akhir - Validasi Coverage MA Client E-GL (Eli Uncov, Cov Cov)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1dc5f25f-5e3d-4611-8a21-fb17847c51e4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 12/0072. GA - MA - GL Akhir - Validasi Coverage MA Client Non E-GL (Eli Uncov, Cov Cov)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>553d54d5-747c-4f0d-9a9f-21ba08de4d65</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 14/0019. GA - IP - GL Awal - Validasi New Member Client E-GL</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7817f4d9-e856-40b9-8f59-1b835642e08a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 14/0020. GA - IP - GL Awal - Validasi New Member Client Non E-GL</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a4a79098-aec7-4f75-bc8e-b3c31f075eec</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 14/0021. GA - IP - GL Revisi - Validasi Product Client E-GL</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>be7f0cb9-2670-4512-9bc9-6e5473d74bd5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 14/0022. GA - IP - GL Revisi - Validasi Product Client Non E-GL</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5252ca03-be93-4a7d-83b0-c7937160c77b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 15/0031. GA - IP - GL Akhir - Validasi Pre-Admission Client E-GL</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>49096cef-c5cc-4f05-a12c-9a96fa3c4f38</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 15/0032. GA - IP - GL Akhir - Validasi Pre-Admission Client Non E-GL</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d9c98982-e6a0-41f9-9446-3d0387c28dca</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 15/0067. GA - MA - GL Akhir - Validasi Pre-Admission Client E-GL</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ad1f96c4-54f6-4fbf-858b-5365906ddac3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 15/0068. GA - MA - GL Akhir - Validasi Pre-Admission Client Non E-GL</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2d502838-857b-450d-af44-facfea22b98d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 15/0073. GA - MA - GL Akhir - Validasi Coverage MA Client E-GL (Eli Uncov, Cov Cov)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>834accf3-e4ea-4d19-b491-7d4f5c1ab75c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 15/0074. GA - MA - GL Akhir - Validasi Coverage MA Client Non E-GL (Eli Uncov, Cov Cov)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>246e5394-6fd3-49cd-afa3-bfe1b4ecae00</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 16/0094. GMA - IP - GL Revisi - Validasi Product Client E-GL</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d66d7d81-4db6-4aa4-b4e9-15e28440d798</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 16/0095. GMA - IP - GL Revisi - Validasi Product Client Non E-GL</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d67ebfa3-3d02-4e72-b49e-703ed0215351</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 16/0122. GMA - MA - GL Revisi - Validasi Product Client E-GL (Eli Uncov, Cov Uncov)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b51f7784-9a44-4f9c-a28d-711321c7409c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 16/0123. GMA - MA - GL Revisi - Validasi Product Client Non E-GL (Eli Uncov, Cov Uncov)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cadc2705-0643-4e3b-a6f8-4b6c21a2e070</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 16/0134. GMA - MA - GL Akhir - Validasi Coverage Client E-GL (Eli Cov, Cov Uncov)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6039f1ce-54fc-431a-9725-1bb982b2412b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 16/0135. GMA - MA - GL Akhir - Validasi Coverage Client Non E-GL (Eli Cov, Cov Uncov)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>53d09b01-5128-48d0-ae18-4c53b3621fb6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 16/0136. GMA - MA - GL Akhir - Validasi Coverage Client E-GL (Eli Uncov, Cov Cov)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4200e000-da09-4fd1-9a9a-c089fd679b2a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 16/0137. GMA - MA - GL Akhir - Validasi Coverage Client Non E-GL (Eli Uncov, Cov Cov)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7227fc87-9eea-47c9-b235-275b2559992d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 16/0140. GA - IP - GL Akhir - Pre-Admission New Member Client E-GL</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9883d709-0040-4d29-b991-8f0776fc1430</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 16/0141. GA - IP - GL Akhir - Pre-Admission New Member Client Non E-GL</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e97fd7ec-223b-4e29-9ddf-688a1b826e7d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 16/0142. GA - MA - GL Akhir - Pre-Admission New Member Client E-GL</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1c80de0f-fe19-4880-866a-d1c5274745cd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 16/0143. GA - MA - GL Akhir - Pre-Admission New Member Client Non E-GL</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>089cb5be-413a-4c2f-8f61-b19d4278f64b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 17/0126. GMA - MA - GL Akhir - Validasi Non Medical Item Client E-GL</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>29030b45-fe32-49da-9d77-d87202ba4bea</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 17/0127. GMA - MA - GL Akhir - Validasi Non Medical Item Client Non E-GL</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a01899d3-bec2-4832-8474-0233050cba03</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 17/0144. GA - IP - GL Revisi - Validasi ODS Reject Client E-GL</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>81a37e82-49fc-4730-8b8d-de8c722c5461</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 17/0145. GA - IP - GL Revisi - Validasi ODS Reject Client Non E-GL</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dd14c237-df5d-47e0-baa5-5515c0017a7c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 17/0146. GA - MA - GL Revisi - Validasi ODS Reject Client E-GL</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f2e99556-2d92-4ef8-bbc0-e8bb5cf6d7d0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 17/0147. GA - MA - GL Revisi - Validasi ODS Reject Client Non E-GL</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
