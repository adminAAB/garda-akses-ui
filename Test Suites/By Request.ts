<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>By Request</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>50</numberOfRerun>
   <pageLoadTimeout>60</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>5933a245-4428-48f3-94b0-c87e8b18b754</testSuiteGuid>
   <testCaseLink>
      <guid>4aacbfbb-7c1d-4791-b728-04c33de62735</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 13/Sprint 13 - 0001</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cb879d6c-7641-45d7-a00b-5d2ab8c60325</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 13/Sprint 13 - 0002</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9795a0a2-b7cf-46ca-97f9-fedc14c1aab9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 13/Sprint 13 - 0037</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d08a232d-1d33-4ad1-845a-8ad4641bf6e0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 13/Sprint 13 - 0038</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
