<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Regression Test - OAS</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>60</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>fdd6cf0d-ee26-4ca3-923b-b5930a943d74</testSuiteGuid>
   <testCaseLink>
      <guid>8005f6a2-7a85-4deb-a7e1-f22991bfd810</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 11/Sprint 11 - 0059</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>812e57ed-9f91-4395-9276-ab3dd2ef64e8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 11/Sprint 11 - 0060</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>12e0b869-f26c-491b-aa4d-8dcc38af6d7c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 12/Sprint 12 - 0075</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9f6f7968-cfcc-468a-a66b-3d1e7dbce817</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 12/Sprint 12 - 0076</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>88d4aa73-c261-4a77-89b0-81e41aa93a0d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 13/Sprint 13 - 0013</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>89d32836-8b35-4c54-b5ff-649d5201dc38</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 13/Sprint 13 - 0014</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f21e3738-d522-400f-b206-2b10fe2128a4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 14/Sprint 14 - 0005</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0a13ade7-d36b-4abf-b109-26a63b77869f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 14/Sprint 14 - 0006</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1090eed6-914a-419f-9fa4-ef3d2c1d67f4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 14/Sprint 14 - 0041</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>223f28aa-34ff-489a-a32a-0fee3f038cbb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 14/Sprint 14 - 0042</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>91d28491-04f3-4d9c-b50b-fedf976ee463</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 15/Sprint 15 - 0024</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cf346d82-188c-4669-9431-b8bbb8059ab5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 15/Sprint 15 - 0027</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>60bbc1b6-ac7f-494d-8df9-a687ce0e12ca</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 15/Sprint 15 - 0049</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5494d44c-b2e2-43aa-8f31-6457b44f6327</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 15/Sprint 15 - 0050</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>593db9ce-cf0b-4938-9fe0-5925a79abf4e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 15/Sprint 15 - 0063</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7ffbe786-23dc-40b2-9218-8f44fd6a28ff</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 16/Sprint 16 - 0028</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>26f19611-c12f-4b04-9480-e8de9dcf02d5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 16/Sprint 16 - 0029</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>14166c7d-a30b-44ba-9f62-fd07bd820cb9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 16/Sprint 16 - 0030</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a8f3b531-a146-4af2-9877-7dabbfd3ca0b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 16/Sprint 16 - 0064</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>67474b87-f2c9-4715-9071-172ae2b5517e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 16/Sprint 16 - 0065</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f0dc794f-1b53-42c2-9af9-33a65e3c402e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 16/Sprint 16 - 0065 Revisi</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>28fb8161-6294-4dac-ae95-4b0cea69059f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 16/Sprint 16 - 0066</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>eade5408-0dc3-47fe-8a7c-a1e4a05c1b67</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 16/Sprint 16 - 0029 IP GL Lanjutan ODC Reject Client EGL</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fbc23808-acef-4477-b460-a4f012797660</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 16/Sprint 16 - 00XX. IP - ODS Confirm Client EGL</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>68538cd6-1c61-4ea2-9416-5bce198d6939</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 16/Sprint 16 - 00XX. IP - ODS Reject Client Non EGL</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fa9d8eae-49ef-49ad-a753-9f6d1cb8f7e1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 16/Sprint 16 - 0100</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>96238399-20cf-475c-9d5d-a73081bb3dc2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 16/Sprint 16 - 0102</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b6cbcb1b-9c6d-4d46-a142-ec691ed7bf47</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 16/Sprint 16 - 0103</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>201d58cb-56cd-492c-a079-0f81de15abce</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 16/Sprint 16 - 0128</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b34f5aa9-935b-4b26-b6c7-ebca36a62971</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 16/Sprint 16 - 0130</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>efa618f9-0cd8-4e55-860b-3c3d171eb70e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 16/Sprint 16 - 0131</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cc3863ff-b111-40b0-949f-9353cb409bc3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 17/Sprint 17 - 0138</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d179d43b-a55c-453d-9ffb-555d0b7d512a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 17/Sprint 17 - 0139</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aa159119-0f42-4c64-80dd-bb0258f487ca</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 17/Sprint 17 - 0220</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2edb61f3-5146-4ffe-971f-9840e71339fc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 17/Sprint 17 - 0221</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>021461fc-d8c7-4de2-9bd6-828fd690fff5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 17/Sprint 17 - 0223</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>66221a5d-d58d-4eeb-8977-b77b7df75430</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 17/Sprint 17 - 0227</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>acd06dee-41df-42e1-93f7-1510a7778ab4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 17/Sprint 17 - 0249</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>366c9003-1661-4c01-aeaa-e4e59f804d74</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 17/Sprint 17 - 0250</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e7c96fc8-8e4e-4bd1-b2ae-9f8256bb774c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 17/Sprint 17 - 0251</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c320b929-cc7e-4e1b-af5f-c5d3a0f929b2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 17/Sprint 17 - 0252</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bbc09848-8eeb-494a-9d4a-a7c5650960da</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 17/Sprint 17 - 0256</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
