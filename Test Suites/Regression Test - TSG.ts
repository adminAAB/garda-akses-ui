<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Regression Test - TSG</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>60</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>3bda0f3a-14fe-467f-badb-78f229db2543</testSuiteGuid>
   <testCaseLink>
      <guid>d06da716-5856-4fd8-982b-001d70a8e5f3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 11/Sprint 11 - 0015</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5b9c7d66-58ab-4bfc-b6fe-475a02fddb1d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 11/Sprint 11 - 0016</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>913d2844-5259-4f2f-80a3-f0a4cb15c58b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 11/Sprint 11 - 0051</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e7a94130-28f0-454a-b3a7-644b1bf66fe5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 11/Sprint 11 - 0052</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
