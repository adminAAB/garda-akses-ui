<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TSG - Cek Katalon - Sprint 17</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>60</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>a0f0881a-72f0-4ee3-82c8-11c451cbcd12</testSuiteGuid>
   <testCaseLink>
      <guid>9f1150fc-9028-4717-9582-13924d687fc2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 11/Sprint 11 - 0015</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>024c9571-a428-49ae-97f9-3c1a5d263b08</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 14/Sprint 14 - 001</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9dfede09-fd2e-4368-af97-3d990c156395</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 14/Sprint 14 - 002</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f8908362-2b10-4831-a917-0561a603f077</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 14/Sprint 14 - 003</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>78c348c3-eda0-4cc5-993f-d95d1fc7c0ce</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 14/Sprint 14 - 004</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f6c964e1-51be-4e97-bbe4-7194bdaad2c9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 15/Sprint 15 - 0025</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7863f11b-9ff0-4d1e-8d58-ab7ff47b2555</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 15/Sprint 15 - 0026</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2c1f78d6-9afb-4ac9-b0ee-aecc74485212</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 15/Sprint 15 - 0035</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>94519d33-8b36-4c9a-9d53-ff1f59140634</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 15/Sprint 15 - 0036</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b3a8a506-64f0-46a0-8d88-f0e72cb05496</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 15/Sprint 15 - 0061</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>55f17db2-f410-43ae-a3f0-60d293f2826b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 16/Sprint 16 - 0089</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8813e889-8456-478f-802c-edc0404f87fd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 16/Sprint 16 - 0085</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>782557dc-23ee-4732-85f2-9151dc81e581</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 15/Sprint 15 - 0062</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fef64375-14fd-4ba1-8b5c-6f5080366561</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 16/Sprint 16 - 0092</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f4267917-8f41-4425-8eed-600e3e9ef6de</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 16/Sprint 16 - 0121</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e23394ef-c60d-499c-bc30-42a2342ac337</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 16/Sprint 16 - 0113</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9379a223-0549-412c-9c07-417670d7159d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 16/Sprint 16 - 0118</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bb90661b-7b66-4680-9ccf-c9390c8cea10</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 16/Sprint 16 - 0119</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f77602b1-d867-4403-8d9d-6798f9893dd6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 16/Sprint 16 - 0091</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>60b69863-3437-4dcc-ad06-e41acec95768</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 16/Sprint 16 - 0120</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>87571838-e34a-4bed-873e-942cb50469fe</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 16/Sprint 16 - 0117</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>000c54c3-4253-4c38-8907-47300eeb7b81</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 16/Sprint 16 - 0116</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5da12344-5d11-4638-87a5-cf4aa90e6f25</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 16/Sprint 16 - 0112</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aa4dc678-9275-4d8a-adf0-4d66477bf7e5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 16/Sprint 16 - 0097</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ff165310-384a-4507-bcb7-33495b394bff</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 16/Sprint 16 - 0096</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7c30b50e-7780-4167-9ecc-474f9f76e9c8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 16/Sprint 16 - 0090</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>653ef0f1-3794-4b55-9a5c-9aa122ccc65c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 16/Sprint 16 - 0088</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b00f200e-bd61-4b0a-a3d0-9c1c2bce9235</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 16/Sprint 16 - 0093</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2404444f-f2c1-43d6-8f59-9db5cd411617</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 16/Sprint 16 - 0084</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
