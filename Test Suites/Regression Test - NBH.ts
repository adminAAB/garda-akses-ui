<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Regression Test - NBH</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>60</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>a7a65247-a153-40f1-a783-4336f75be77f</testSuiteGuid>
   <testCaseLink>
      <guid>839106b9-86d7-413c-83f0-3a024bbbd5ff</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 11/Sprint 11 - 0007</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8d386491-8636-489a-9979-c1a9b5969983</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 11/Sprint 11 - 0008</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c1019cfb-2971-44af-b5bc-506ba44dfc1f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 11/Sprint 11 - 0011</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e67365df-23f1-4a05-bcc8-d13e5a17e8f2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 11/Sprint 11 - 0012</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>273a2f56-2595-47e0-87f5-8053bc907936</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 11/Sprint 11 - 0043</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1cc87c8d-891d-49c0-a5f8-aa9b49dcb206</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 11/Sprint 11 - 0044</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e753e7f6-f3d4-4460-b861-e856b89061e7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 11/Sprint 11 - 0047</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d874a217-bf7a-42f1-92dc-78d998f6751d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 11/Sprint 11 - 0048</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a8ef767d-d6e2-4927-bb5d-5a50e5f2ba23</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 12/Sprint 12 - 0009</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>223b39d1-f63e-4dcd-9dc5-f36a04cbb304</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 12/Sprint 12 - 0010</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>db00bcd8-dc08-4646-941e-c6fac0c2bb01</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 12/Sprint 12 - 0045</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b298ea96-6287-4951-aeee-cd0fee30e3db</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 12/Sprint 12 - 0046</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fa8fc7f7-681e-47ed-ac99-f292e47489de</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 13/Sprint 13 - 0001</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b26a7a92-5094-4eb3-914e-18f7a21976a3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 13/Sprint 13 - 0002</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3f219feb-866f-42b6-9947-33c39af50a88</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 13/Sprint 13 - 0037</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>66707861-405d-42f2-a364-acdec051ad38</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 13/Sprint 13 - 0038</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6f39a420-250e-4a18-948c-ea3350121195</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 13/Sprint 13 - 0078</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>26db6f32-2055-4311-981a-dbb6a41ba5b9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 13/Sprint 13 - 0079</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c20de7ce-f8f7-4e68-9ec8-b8e50b62530d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 13/Sprint 13 - 0106</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ed5cb041-5758-4a05-8dc1-9079c6882a12</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 13/Sprint 13 - 0107</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>05e942ea-2047-4b5d-8e8f-9939c69c53ef</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 14/Sprint 14 - 0086</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>50f009a3-2cb1-4f17-bc00-8a71a3d2af1a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 14/Sprint 14 - 0087</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9cb11912-cb5f-4b11-87a0-87b4eecf0b6e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 14/Sprint 14 - 0114</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>01764d17-73d9-47fa-b8fe-39655f8b1501</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 14/Sprint 14 - 0115</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3ed96d87-24f3-4809-bc40-dbc6102e5cb5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 15/Sprint 15 - 0003</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cc26a3e0-a035-42c3-8daf-6f9421cfb35f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 15/Sprint 15 - 0004</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b9606525-2fd9-4db6-9ddc-d0043cb0042d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 15/Sprint 15 - 0039</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>455c7643-87fa-4987-86f7-7d4997474650</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 15/Sprint 15 - 0040</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e2484bf0-8c19-47dc-b237-d3e0a2363f31</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 15/Sprint 15 - 0080</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cb093cf9-bca8-4f13-bd3c-9709e2075f0d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 15/Sprint 15 - 0081</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>645a034a-bc08-4155-a3b5-6481ea3279dc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 15/Sprint 15 - 0082</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>77c81dc8-a306-4daf-a1ea-713029e002e5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 15/Sprint 15 - 0083</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>101eb33c-2d7a-4f94-880d-65750feb1aeb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 15/Sprint 15 - 0108</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e81d542e-5d52-4c52-bbe6-9d2d486527c1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 15/Sprint 15 - 0109</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>df94c2ea-2b82-4b81-9089-d4997ec29b73</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 15/Sprint 15 - 0110</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d2122536-e921-4e72-85f8-c43d22b98f8a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 15/Sprint 15 - 0111</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
