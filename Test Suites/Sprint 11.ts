<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Sprint 11</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>60</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>27b5bbe3-7537-47d2-b186-092119b4d8de</testSuiteGuid>
   <testCaseLink>
      <guid>01570a79-0e04-4638-b14f-215c06141248</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 11/Sprint 11 - 0007</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3399d1e9-a4eb-4ecc-9324-298c2c452ebd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 11/Sprint 11 - 0008</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8eb08d5d-31fa-49aa-8485-9441a7232164</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 11/Sprint 11 - 0011</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>20af055c-305d-4ffe-98de-cd2184baeba6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 11/Sprint 11 - 0012</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ff10fd6c-78b4-4e3c-98ab-f9977f5f37f9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 11/Sprint 11 - 0015</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4559f379-e7a6-4310-b93a-11c557854328</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 11/Sprint 11 - 0043</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e3f8bd8b-60c4-4d48-a232-ffcc7b2e4770</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 11/Sprint 11 - 0044</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8d9e05ad-f80d-4728-826a-946edb874bca</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 11/Sprint 11 - 0047</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1ed467b9-b058-48ec-a7b6-f4ed2e28555e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 11/Sprint 11 - 0048</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f5bb7d21-1d0c-4ec4-b609-323f12c6725b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 11/Sprint 11 - 0059</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a25a86a6-a6f9-4e6f-9ce7-73c0b31c7fdb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 11/Sprint 11 - 0060</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
