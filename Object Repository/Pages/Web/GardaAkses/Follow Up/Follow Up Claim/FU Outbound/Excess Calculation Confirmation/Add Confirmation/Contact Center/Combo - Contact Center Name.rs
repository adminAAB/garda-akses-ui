<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Combo - Contact Center Name</name>
   <tag></tag>
   <elementGuidId>47bc0389-a216-4ff0-a557-6c23a62f4d9b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;CCODiagnosis&quot;]//span[text() = 'Contact Center Name']/parent::label/following-sibling::div//span[@class=&quot;select2-selection__rendered&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;CCODiagnosis&quot;]//span[text() = 'Contact Center Name']/parent::label/following-sibling::div//span[@class=&quot;select2-selection__rendered&quot;]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Pages/Web/GEN5/Frame</value>
   </webElementProperties>
</WebElementEntity>
