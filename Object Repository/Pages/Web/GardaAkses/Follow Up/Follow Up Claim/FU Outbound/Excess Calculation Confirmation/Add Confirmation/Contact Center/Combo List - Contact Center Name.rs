<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Combo List - Contact Center Name</name>
   <tag></tag>
   <elementGuidId>70b64349-e0dd-4ccf-b1f6-8a232cb00617</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//li[contains(text(),'${value}')][count(. | //*[@ref_element = 'Object Repository/Pages/Web/GEN5/Frame']) = count(//*[@ref_element = 'Object Repository/Pages/Web/GEN5/Frame'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//li[contains(text(),'${value}')]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Pages/Web/GEN5/Frame</value>
   </webElementProperties>
</WebElementEntity>
