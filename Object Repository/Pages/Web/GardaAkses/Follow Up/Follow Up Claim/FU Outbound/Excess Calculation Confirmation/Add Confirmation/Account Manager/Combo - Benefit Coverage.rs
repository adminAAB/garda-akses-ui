<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Combo - Benefit Coverage</name>
   <tag></tag>
   <elementGuidId>cbd13e0d-e7f8-4b0b-a348-cfced3eaf2dd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;AMDiagnosis&quot;]//span[text() = 'Benefit Coverage']/parent::label/following-sibling::div//span[text() = 'Please Select One']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;AMDiagnosis&quot;]//span[text() = 'Benefit Coverage']/parent::label/following-sibling::div//span[text() = 'Please Select One']</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Pages/Web/GEN5/Frame</value>
   </webElementProperties>
</WebElementEntity>
