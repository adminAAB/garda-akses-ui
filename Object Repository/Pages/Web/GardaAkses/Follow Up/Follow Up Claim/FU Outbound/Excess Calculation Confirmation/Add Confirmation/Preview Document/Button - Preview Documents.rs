<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Button - Preview Documents</name>
   <tag></tag>
   <elementGuidId>80942583-149d-4384-917a-5450e4252afd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;popUpPreviewDocuments&quot;]//button[text() = '${value}']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;popUpPreviewDocuments&quot;]//button[text() = '${value}']</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Pages/Web/GEN5/Frame</value>
   </webElementProperties>
</WebElementEntity>
