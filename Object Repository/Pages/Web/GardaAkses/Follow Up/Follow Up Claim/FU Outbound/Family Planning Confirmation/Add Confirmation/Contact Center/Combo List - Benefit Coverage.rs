<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Combo List - Benefit Coverage</name>
   <tag></tag>
   <elementGuidId>dc5a31b1-0988-498b-bb48-8cf581dbe8c8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;fieldBenefitCoverageFamilyPlanningCCO&quot;]//span[text() = 'Benefit Coverage']/parent::label/following-sibling::div//span[text() = 'Please Select One'][count(. | //*[@ref_element = 'Object Repository/Pages/Web/GEN5/Frame']) = count(//*[@ref_element = 'Object Repository/Pages/Web/GEN5/Frame'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;fieldBenefitCoverageFamilyPlanningCCO&quot;]//span[text() = 'Benefit Coverage']/parent::label/following-sibling::div//span[text() = 'Please Select One']</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Pages/Web/GEN5/Frame</value>
   </webElementProperties>
</WebElementEntity>
