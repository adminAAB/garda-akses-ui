<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Apakah anda ingin melakukan pembentukan LK?</description>
   <name>Pop Up - Confirmation Yes</name>
   <tag></tag>
   <elementGuidId>c13ee4c6-7be8-4e91-98a6-b9994a8e3861</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;ModalSaveClaim&quot;]//button[text() = 'Yes']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;ModalSaveClaim&quot;]//button[text() = 'Yes']</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Pages/Web/GEN5/Frame</value>
   </webElementProperties>
</WebElementEntity>
