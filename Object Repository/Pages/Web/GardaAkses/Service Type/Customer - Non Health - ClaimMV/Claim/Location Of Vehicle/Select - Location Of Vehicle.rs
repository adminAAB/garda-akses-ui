<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Select - Location Of Vehicle</name>
   <tag></tag>
   <elementGuidId>4b9f031c-9338-41ed-8296-27f822cdb7b4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;LocationVehiclePopUpContent-0&quot;]/div[1]/div[2]/a2is-datatable/div[2]/div/table/tbody/tr[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;LocationVehiclePopUpContent-0&quot;]/div[1]/div[2]/a2is-datatable/div[2]/div/table/tbody/tr[1]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Pages/Web/GEN5/Frame</value>
   </webElementProperties>
</WebElementEntity>
