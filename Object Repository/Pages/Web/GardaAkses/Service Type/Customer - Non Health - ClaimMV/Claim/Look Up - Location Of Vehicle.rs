<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Look Up - Location Of Vehicle</name>
   <tag></tag>
   <elementGuidId>d9c45f43-f0a6-40ef-8b2c-2124fb98b781</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;ClaimTab-0&quot;]/div[1]/div/div[3]/div[1]/a2is-textbox-wide-dc[1]/div[2]/label/span[text() = 'Location Of Vehicle']/parent::label/following-sibling::div//span/button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;ClaimTab-0&quot;]/div[1]/div/div[3]/div[1]/a2is-textbox-wide-dc[1]/div[2]/label/span[text() = 'Location Of Vehicle']/parent::label/following-sibling::div//span/button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Pages/Web/GEN5/Frame</value>
   </webElementProperties>
</WebElementEntity>
