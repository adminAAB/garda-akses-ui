<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Table - Diagnosis Treatment Question</name>
   <tag></tag>
   <elementGuidId>951cfb06-a27a-40b6-bdc4-1e4c7e7526e2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;DiagnosisTreatmentQuestion&quot;]/a2is-datatable/div[2]/div/table/tbody</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;DiagnosisTreatmentQuestion&quot;]/a2is-datatable/div[2]/div/table/tbody</value>
   </webElementProperties>
</WebElementEntity>
