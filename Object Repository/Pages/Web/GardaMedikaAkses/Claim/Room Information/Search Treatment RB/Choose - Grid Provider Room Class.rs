<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Choose - Grid Provider Room Class</name>
   <tag></tag>
   <elementGuidId>7b006464-1450-4621-94b3-3702efeeb5b0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;ProviderRoomBoardInfoPopUpSection-0&quot;]/div[1]/div/div[1]/a2is-datatable/div[2]/div/table/tbody/tr[1]/td[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;ProviderRoomBoardInfoPopUpSection-0&quot;]/div[1]/div/div[1]/a2is-datatable/div[2]/div/table/tbody/tr[1]/td[2]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Pages/Web/GEN5/Frame</value>
   </webElementProperties>
</WebElementEntity>
