<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Input - Billed</name>
   <tag></tag>
   <elementGuidId>f2bfdd4b-5d19-4cfb-939e-d7b30bcddeb4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;MTBilled&quot;]/div[2]/div[1]/div/input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;MTBilled&quot;]/div[2]/div[1]/div/input</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Pages/Web/GEN5/Frame</value>
   </webElementProperties>
</WebElementEntity>
