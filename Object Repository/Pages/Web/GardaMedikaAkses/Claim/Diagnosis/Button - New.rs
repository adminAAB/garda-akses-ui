<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Button - New</name>
   <tag></tag>
   <elementGuidId>9b1cf6c1-5fab-47b6-9b59-f4dea1533642</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;DiagnosisInformation-0&quot;]//span[@data-original-title = &quot;Create new&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;DiagnosisInformation-0&quot;]//span[@data-original-title = &quot;Create new&quot;]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Pages/Web/GEN5/Frame</value>
   </webElementProperties>
</WebElementEntity>
