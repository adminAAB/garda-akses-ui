<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Combo - Diagnosis Question</name>
   <tag></tag>
   <elementGuidId>b74e465e-c26d-44da-8a5f-b8dee262abea</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;popUpDiagnosisInfoFullText-0&quot;]//h3[text() = 'Diagnosis Question']/following-sibling::div//span[@role = 'presentation']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;popUpDiagnosisInfoFullText-0&quot;]//h3[text() = 'Diagnosis Question']/following-sibling::div//span[@role = 'presentation']</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Pages/Web/GEN5/Frame</value>
   </webElementProperties>
</WebElementEntity>
