<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Input - Remarks Diagnosis Question</name>
   <tag></tag>
   <elementGuidId>07b4b6fd-5df8-457e-973c-afb8ab2046ae</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;popUpDiagnosisInfoFullText-0&quot;]//span[contains(text(), '${value}')]/parent::td/following-sibling::td//input[@maxlength=&quot;1000&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;popUpDiagnosisInfoFullText-0&quot;]//span[contains(text(), '${value}')]/parent::td/following-sibling::td//input[@maxlength=&quot;1000&quot;]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Pages/Web/GEN5/Frame</value>
   </webElementProperties>
</WebElementEntity>
