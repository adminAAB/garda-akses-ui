<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Auto Complete - ID or Name</name>
   <tag></tag>
   <elementGuidId>c0d33181-f034-4145-a48e-6066bcc758a3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;DiagnosisInfoPopUpSectionFullText.DiagnosisInfoautocomplete-list&quot;]/div[count(. | //*[@ref_element = 'Object Repository/Pages/Web/GEN5/Frame']) = count(//*[@ref_element = 'Object Repository/Pages/Web/GEN5/Frame'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;DiagnosisInfoPopUpSectionFullText.DiagnosisInfoautocomplete-list&quot;]/div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Pages/Web/GEN5/Frame</value>
   </webElementProperties>
</WebElementEntity>
