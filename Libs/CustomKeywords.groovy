
/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */

import java.lang.String

import java.util.ArrayList

import com.kms.katalon.core.testobject.TestObject


def static "querySQL.Query.QueryContactName"() {
    (new querySQL.Query()).QueryContactName()
}

def static "querySQL.Query.QueryNewMemberName"() {
    (new querySQL.Query()).QueryNewMemberName()
}

def static "querySQL.Query.QueryEmployeeID"() {
    (new querySQL.Query()).QueryEmployeeID()
}

def static "querySQL.Query.QueryFinishFollowUp"(
    	String RemarksTreatmentInformation	) {
    (new querySQL.Query()).QueryFinishFollowUp(
        	RemarksTreatmentInformation)
}

def static "querySQL.Query.QueryGetTrID"(
    	String RemarksTreatmentInformation	) {
    (new querySQL.Query()).QueryGetTrID(
        	RemarksTreatmentInformation)
}

def static "gardaAkses.HandleTicketFU.FollowUp"(
    	String PhaseFU	
     , 	String PhaseGL	
     , 	String UserID	
     , 	String Password	
     , 	String Menu	
     , 	String SubMenuFU	) {
    (new gardaAkses.HandleTicketFU()).FollowUp(
        	PhaseFU
         , 	PhaseGL
         , 	UserID
         , 	Password
         , 	Menu
         , 	SubMenuFU)
}

def static "com.keyword.UI.verifyStaging"() {
    (new com.keyword.UI()).verifyStaging()
}

def static "com.keyword.UI.connectDB"(
    	String IP	
     , 	String dbname	) {
    (new com.keyword.UI()).connectDB(
        	IP
         , 	dbname)
}

def static "com.keyword.UI.executeQuery"(
    	String queryString	) {
    (new com.keyword.UI()).executeQuery(
        	queryString)
}

def static "com.keyword.UI.CallableState"(
    	String queryString	) {
    (new com.keyword.UI()).CallableState(
        	queryString)
}

def static "com.keyword.UI.execute"(
    	String queryString	) {
    (new com.keyword.UI()).execute(
        	queryString)
}

def static "com.keyword.UI.countdbColumn"(
    	String queryTable	) {
    (new com.keyword.UI()).countdbColumn(
        	queryTable)
}

def static "com.keyword.UI.countdbRow"(
    	String queryTable	) {
    (new com.keyword.UI()).countdbRow(
        	queryTable)
}

def static "com.keyword.UI.getValueDatabase"(
    	String IP	
     , 	String dbname	
     , 	String queryTable	
     , 	String ColumnName	) {
    (new com.keyword.UI()).getValueDatabase(
        	IP
         , 	dbname
         , 	queryTable
         , 	ColumnName)
}

def static "com.keyword.UI.updateValueDatabase"(
    	String IP	
     , 	String dbname	
     , 	String updateQuery	) {
    (new com.keyword.UI()).updateValueDatabase(
        	IP
         , 	dbname
         , 	updateQuery)
}

def static "com.keyword.UI.getOneRowDatabase"(
    	String IP	
     , 	String dbname	
     , 	String queryTable	) {
    (new com.keyword.UI()).getOneRowDatabase(
        	IP
         , 	dbname
         , 	queryTable)
}

def static "com.keyword.UI.getOneColumnDatabase"(
    	String IP	
     , 	String dbname	
     , 	String queryTable	
     , 	String ColumnName	) {
    (new com.keyword.UI()).getOneColumnDatabase(
        	IP
         , 	dbname
         , 	queryTable
         , 	ColumnName)
}

def static "com.keyword.UI.getAllDataDatabase"(
    	String IP	
     , 	String dbname	
     , 	String queryTable	) {
    (new com.keyword.UI()).getAllDataDatabase(
        	IP
         , 	dbname
         , 	queryTable)
}

def static "com.keyword.UI.getSpecificDatabase"(
    	String IP	
     , 	String dbname	
     , 	String queryTable	
     , 	int row	
     , 	int column	) {
    (new com.keyword.UI()).getSpecificDatabase(
        	IP
         , 	dbname
         , 	queryTable
         , 	row
         , 	column)
}

def static "com.keyword.UI.compareRowDBtoArray"(
    	String url	
     , 	String dbname	
     , 	String queryTable	
     , 	ArrayList listData	) {
    (new com.keyword.UI()).compareRowDBtoArray(
        	url
         , 	dbname
         , 	queryTable
         , 	listData)
}

def static "com.keyword.UI.CompareFieldtoDatabase"(
    	ArrayList ObjRep	
     , 	String url	
     , 	String dbname	
     , 	String queryTable	) {
    (new com.keyword.UI()).CompareFieldtoDatabase(
        	ObjRep
         , 	url
         , 	dbname
         , 	queryTable)
}

def static "com.keyword.UI.getFieldsValue"(
    	ArrayList ObjRep	) {
    (new com.keyword.UI()).getFieldsValue(
        	ObjRep)
}

def static "com.keyword.UI.closeDatabaseConnection"() {
    (new com.keyword.UI()).closeDatabaseConnection()
}

def static "com.keyword.UI.newTestObject"(
    	String locator	) {
    (new com.keyword.UI()).newTestObject(
        	locator)
}

def static "com.keyword.UI.AccessURL"(
    	String App	) {
    (new com.keyword.UI()).AccessURL(
        	App)
}

def static "com.keyword.UI.AccessBrowser"(
    	String URL	) {
    (new com.keyword.UI()).AccessBrowser(
        	URL)
}

def static "com.keyword.UI.Sleep"(
    	int timeOut	) {
    (new com.keyword.UI()).Sleep(
        	timeOut)
}

def static "com.keyword.UI.Write"(
    	TestObject xpath	
     , 	String text	) {
    (new com.keyword.UI()).Write(
        	xpath
         , 	text)
}

def static "com.keyword.UI.WaitElement"(
    	TestObject xpath	) {
    (new com.keyword.UI()).WaitElement(
        	xpath)
}

def static "com.keyword.UI.Click"(
    	TestObject xpath	) {
    (new com.keyword.UI()).Click(
        	xpath)
}

def static "com.keyword.UI.DoubleClick"(
    	TestObject xpath	) {
    (new com.keyword.UI()).DoubleClick(
        	xpath)
}

def static "com.keyword.UI.DragAndDrop"(
    	TestObject sourceXpath	
     , 	TestObject destinationXpath	) {
    (new com.keyword.UI()).DragAndDrop(
        	sourceXpath
         , 	destinationXpath)
}

def static "com.keyword.UI.Back"() {
    (new com.keyword.UI()).Back()
}

def static "com.keyword.UI.HoverItem"(
    	TestObject xpath	) {
    (new com.keyword.UI()).HoverItem(
        	xpath)
}

def static "com.keyword.UI.DeleteWrite"(
    	TestObject xpath	
     , 	String text	) {
    (new com.keyword.UI()).DeleteWrite(
        	xpath
         , 	text)
}

def static "com.keyword.UI.SkipWrite"(
    	TestObject xpath	
     , 	String text	) {
    (new com.keyword.UI()).SkipWrite(
        	xpath
         , 	text)
}

def static "com.keyword.UI.ComboBoxSearch"(
    	TestObject Combo1	
     , 	TestObject Combo2	
     , 	String Value	) {
    (new com.keyword.UI()).ComboBoxSearch(
        	Combo1
         , 	Combo2
         , 	Value)
}

def static "com.keyword.UI.ComboBoxSearchSkip"(
    	TestObject comboOpen	
     , 	TestObject comboSearch	
     , 	String Value	) {
    (new com.keyword.UI()).ComboBoxSearchSkip(
        	comboOpen
         , 	comboSearch
         , 	Value)
}

def static "com.keyword.UI.ComboBox"(
    	TestObject Combo	
     , 	String Value	) {
    (new com.keyword.UI()).ComboBox(
        	Combo
         , 	Value)
}

def static "com.keyword.UI.MultiSelectComboBox"(
    	TestObject Combo	
     , 	String Value1	
     , 	String Value2	
     , 	String Value3	
     , 	String Value4	) {
    (new com.keyword.UI()).MultiSelectComboBox(
        	Combo
         , 	Value1
         , 	Value2
         , 	Value3
         , 	Value4)
}

def static "com.keyword.UI.CheckDisableandWrite"(
    	TestObject Xpath	
     , 	String text	) {
    (new com.keyword.UI()).CheckDisableandWrite(
        	Xpath
         , 	text)
}

def static "com.keyword.UI.RunningPhoneNumber"(
    	TestObject xpath	) {
    (new com.keyword.UI()).RunningPhoneNumber(
        	xpath)
}

def static "com.keyword.UI.UploadFile"(
    	String fileLocation	
     , 	String pictureName	) {
    (new com.keyword.UI()).UploadFile(
        	fileLocation
         , 	pictureName)
}

def static "com.keyword.UI.UploadFile2"(
    	String pictureName	) {
    (new com.keyword.UI()).UploadFile2(
        	pictureName)
}

def static "com.keyword.UI.RunScheduler"(
    	String path	) {
    (new com.keyword.UI()).RunScheduler(
        	path)
}

def static "com.keyword.UI.WriteAllRowsXls"(
    	String path	
     , 	int row	
     , 	ArrayList value	) {
    (new com.keyword.UI()).WriteAllRowsXls(
        	path
         , 	row
         , 	value)
}

def static "com.keyword.UI.WriteSingleCellXls"(
    	String path	
     , 	int row	
     , 	int column	
     , 	Object value	) {
    (new com.keyword.UI()).WriteSingleCellXls(
        	path
         , 	row
         , 	column
         , 	value)
}

def static "com.keyword.UI.AccessURLwithPlugin"(
    	String url	
     , 	String Plugin	) {
    (new com.keyword.UI()).AccessURLwithPlugin(
        	url
         , 	Plugin)
}

def static "com.keyword.UI.readQRCode"() {
    (new com.keyword.UI()).readQRCode()
}

def static "com.keyword.UI.getDateToday"(
    	String format	) {
    (new com.keyword.UI()).getDateToday(
        	format)
}

def static "com.keyword.UI.ScreenShot"(
    	String FileName	) {
    (new com.keyword.UI()).ScreenShot(
        	FileName)
}

def static "com.keyword.UI.GlobalVar"(
    	String name	
     , 	Object value	) {
    (new com.keyword.UI()).GlobalVar(
        	name
         , 	value)
}

def static "com.keyword.UI.SetGlobal"(
    	String varName	
     , 	String App	) {
    (new com.keyword.UI()).SetGlobal(
        	varName
         , 	App)
}

def static "com.keyword.UI.Note"(
    	Object variable	) {
    (new com.keyword.UI()).Note(
        	variable)
}

def static "gardaAkses.PopUp.TrayCTI"() {
    (new gardaAkses.PopUp()).TrayCTI()
}

def static "gardaAkses.CreateGL.InputMemberName"(
    	TestObject Xpath	
     , 	String MemberName	) {
    (new gardaAkses.CreateGL()).InputMemberName(
        	Xpath
         , 	MemberName)
}

def static "gardaAkses.CreateGL.DatePicketStart"(
    	String Tanggal	
     , 	String Bulan	
     , 	int Tahun	) {
    (new gardaAkses.CreateGL()).DatePicketStart(
        	Tanggal
         , 	Bulan
         , 	Tahun)
}

def static "com.kms.katalon.keyword.uploadfile.UploadFile.uploadFile"(
    	TestObject object	
     , 	String file	) {
    (new com.kms.katalon.keyword.uploadfile.UploadFile()).uploadFile(
        	object
         , 	file)
}

def static "com.kms.katalon.keyword.uploadfile.UploadFile.uploadFileUsingRobot"(
    	TestObject object	
     , 	String file	) {
    (new com.kms.katalon.keyword.uploadfile.UploadFile()).uploadFileUsingRobot(
        	object
         , 	file)
}

def static "gardaAkses.GetTicketID.GetTicketNumber"(
    	String Phase	) {
    (new gardaAkses.GetTicketID()).GetTicketNumber(
        	Phase)
}

def static "gardaAkses.GetTicketID.ProviderHealthClaim"(
    	String Phase	) {
    (new gardaAkses.GetTicketID()).ProviderHealthClaim(
        	Phase)
}

def static "gardaAkses.GetTicketID.ProviderNonHealthCancellation"(
    	String Phase	) {
    (new gardaAkses.GetTicketID()).ProviderNonHealthCancellation(
        	Phase)
}

def static "com.keyword.GEN5.SideMenu"(
    	String ParentMenu	
     , 	String ChildMenu	) {
    (new com.keyword.GEN5()).SideMenu(
        	ParentMenu
         , 	ChildMenu)
}

def static "com.keyword.GEN5.DatePicker"(
    	String DateNow	
     , 	TestObject DatePickerDiv	) {
    (new com.keyword.GEN5()).DatePicker(
        	DateNow
         , 	DatePickerDiv)
}

def static "com.keyword.GEN5.getAllColumnValue"(
    	TestObject tableXpath	
     , 	String gridColumn	) {
    (new com.keyword.GEN5()).getAllColumnValue(
        	tableXpath
         , 	gridColumn)
}

def static "com.keyword.GEN5.getAllRowsValue"(
    	TestObject tableXpath	
     , 	String columnHeader	
     , 	String RowsValue	) {
    (new com.keyword.GEN5()).getAllRowsValue(
        	tableXpath
         , 	columnHeader
         , 	RowsValue)
}

def static "com.keyword.GEN5.CompareRowsValue"(
    	TestObject tableXpath	
     , 	String columnHeader	
     , 	String RowsValue	
     , 	ArrayList RowsCompare	) {
    (new com.keyword.GEN5()).CompareRowsValue(
        	tableXpath
         , 	columnHeader
         , 	RowsValue
         , 	RowsCompare)
}

def static "com.keyword.GEN5.CompareColumnsValue"(
    	TestObject tableXpath	
     , 	String gridColumn	
     , 	ArrayList CompareColumn	) {
    (new com.keyword.GEN5()).CompareColumnsValue(
        	tableXpath
         , 	gridColumn
         , 	CompareColumn)
}

def static "com.keyword.GEN5.ClickExpectedRow"(
    	TestObject tableXpath	
     , 	String gridColumn	
     , 	String columnValue	) {
    (new com.keyword.GEN5()).ClickExpectedRow(
        	tableXpath
         , 	gridColumn
         , 	columnValue)
}

def static "com.keyword.GEN5.ClickExpectedRowWithNext"(
    	TestObject tableXpath	
     , 	String gridColumn	
     , 	String columnValue	
     , 	TestObject ButtonNext	) {
    (new com.keyword.GEN5()).ClickExpectedRowWithNext(
        	tableXpath
         , 	gridColumn
         , 	columnValue
         , 	ButtonNext)
}

def static "com.keyword.GEN5.ProcessingCommand"() {
    (new com.keyword.GEN5()).ProcessingCommand()
}

def static "com.keyword.GEN5.CompareColumnToDatabase"(
    	TestObject tableXpath	
     , 	String gridColumn	
     , 	String url	
     , 	String dbname	
     , 	String queryTable	
     , 	String getColumn	) {
    (new com.keyword.GEN5()).CompareColumnToDatabase(
        	tableXpath
         , 	gridColumn
         , 	url
         , 	dbname
         , 	queryTable
         , 	getColumn)
}

def static "com.keyword.GEN5.CompareRowToDatabase"(
    	TestObject tableXpath	
     , 	String columnHeader	
     , 	String RowsValue	
     , 	String url	
     , 	String dbname	
     , 	String queryTable	) {
    (new com.keyword.GEN5()).CompareRowToDatabase(
        	tableXpath
         , 	columnHeader
         , 	RowsValue
         , 	url
         , 	dbname
         , 	queryTable)
}

def static "com.keyword.GEN5.InsertIntoDataHealth"(
    	String type	
     , 	String AppName	
     , 	String value	) {
    (new com.keyword.GEN5()).InsertIntoDataHealth(
        	type
         , 	AppName
         , 	value)
}

def static "com.keyword.GEN5.getDataFromDataHealth"(
    	String Parameter	
     , 	String ParamValue	
     , 	String Value	) {
    (new com.keyword.GEN5()).getDataFromDataHealth(
        	Parameter
         , 	ParamValue
         , 	Value)
}

def static "com.keyword.GEN5.getPopUpText"(
    	TestObject TextOnPopup	) {
    (new com.keyword.GEN5()).getPopUpText(
        	TextOnPopup)
}

def static "com.keyword.GEN5.tickAllCheckboxInTable"(
    	TestObject Object	) {
    (new com.keyword.GEN5()).tickAllCheckboxInTable(
        	Object)
}

def static "training.Katalon.loginGen5"(
    	String URL	
     , 	String Username	
     , 	String Password	) {
    (new training.Katalon()).loginGen5(
        	URL
         , 	Username
         , 	Password)
}

def static "training.Katalon.select"(
    	Object hbt	
     , 	Object hbt2	) {
    (new training.Katalon()).select(
        	hbt
         , 	hbt2)
}

def static "querySQL.DefaultQuery.connectDB"(
    	String url	
     , 	String dbname	
     , 	String username	
     , 	String password	) {
    (new querySQL.DefaultQuery()).connectDB(
        	url
         , 	dbname
         , 	username
         , 	password)
}

def static "querySQL.DefaultQuery.executeQuery"(
    	String queryString	) {
    (new querySQL.DefaultQuery()).executeQuery(
        	queryString)
}

def static "querySQL.DefaultQuery.execute"(
    	String queryString	) {
    (new querySQL.DefaultQuery()).execute(
        	queryString)
}

def static "gardaAkses.CreateTicket.ContactName"(
    	String ContactName	) {
    (new gardaAkses.CreateTicket()).ContactName(
        	ContactName)
}

def static "gardaAkses.CreateTicket.ProviderName"(
    	String ProviderName	) {
    (new gardaAkses.CreateTicket()).ProviderName(
        	ProviderName)
}

def static "gardaAkses.General.waitProcessingCommand"() {
    (new gardaAkses.General()).waitProcessingCommand()
}

def static "gardaAkses.General.ShowSuggestion"(
    	TestObject Xpath	
     , 	String Name	) {
    (new gardaAkses.General()).ShowSuggestion(
        	Xpath
         , 	Name)
}

def static "gardaAkses.General.UpdateFieldText"(
    	TestObject Xpath	
     , 	String Variable	) {
    (new gardaAkses.General()).UpdateFieldText(
        	Xpath
         , 	Variable)
}

def static "gardaAkses.General.UpdateFieldCombo"(
    	TestObject XpathCombo	
     , 	TestObject XpathList	
     , 	String Variable	) {
    (new gardaAkses.General()).UpdateFieldCombo(
        	XpathCombo
         , 	XpathList
         , 	Variable)
}
