package internal

import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.main.TestCaseMain


/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */
public class GlobalVariable {
     
    /**
     * <p></p>
     */
    public static Object URLGardaAksesProduction
     
    /**
     * <p></p>
     */
    public static Object URLGardaAksesQC
     
    /**
     * <p></p>
     */
    public static Object URLGardaAkses4
     
    /**
     * <p></p>
     */
    public static Object URLGardaAkses5
     
    /**
     * <p></p>
     */
    public static Object URLGardaMedikaAkses
     
    /**
     * <p></p>
     */
    public static Object Delay1
     
    /**
     * <p></p>
     */
    public static Object Delay2
     
    /**
     * <p></p>
     */
    public static Object Delay3
     
    /**
     * <p></p>
     */
    public static Object NewMemberName
     
    /**
     * <p></p>
     */
    public static Object TicketID1
     
    /**
     * <p></p>
     */
    public static Object TicketID2
     
    /**
     * <p></p>
     */
    public static Object TicketID3
     
    /**
     * <p></p>
     */
    public static Object TicketID4
     
    /**
     * <p></p>
     */
    public static Object TicketID5
     
    /**
     * <p></p>
     */
    public static Object PhoneNumber
     
    /**
     * <p></p>
     */
    public static Object Email
     
    /**
     * <p></p>
     */
    public static Object FaxNumber
     
    /**
     * <p></p>
     */
    public static Object ExtNumber
     
    /**
     * <p></p>
     */
    public static Object SocialMedia
     
    /**
     * <p>Profile default : Popup Message Bila status Produce
Profile NBH : Popup Message Bila status Produce
Profile OAS : Popup Message Bila status Produce</p>
     */
    public static Object ValidasiDijaminkan
     
    /**
     * <p></p>
     */
    public static Object ValidasiTidakDijaminkan
     
    /**
     * <p></p>
     */
    public static Object ValidasiDiagnosa
     
    /**
     * <p></p>
     */
    public static Object ValidasiPreAdmission
     
    /**
     * <p></p>
     */
    public static Object ValidasiReject
     
    /**
     * <p></p>
     */
    public static Object ValidasiNewMember
     
    /**
     * <p></p>
     */
    public static Object ValidasiAnnualLimit
     
    /**
     * <p></p>
     */
    public static Object ValidasiDocument
     
    /**
     * <p></p>
     */
    public static Object ValidasiClientNonEGL
     
    /**
     * <p></p>
     */
    public static Object ValidasiGMAClientNonEGL
     
    /**
     * <p></p>
     */
    public static Object ValidasiEligibility
     
    /**
     * <p></p>
     */
    public static Object ValidasiClaimOS
     
    /**
     * <p></p>
     */
    public static Object ValidasiMedicalTreatment
     
    /**
     * <p></p>
     */
    public static Object ValidasiNonClient
     
    /**
     * <p></p>
     */
    public static Object ValidasiProductIP
     
    /**
     * <p></p>
     */
    public static Object ValidasiProductMA
     
    /**
     * <p></p>
     */
    public static Object ValidasiCoverageCovMA
     
    /**
     * <p></p>
     */
    public static Object ValidasiCoverageUncovMA
     
    /**
     * <p></p>
     */
    public static Object ValidasiGMAAppropriateBlank
     
    /**
     * <p></p>
     */
    public static Object ValidasiODS
     
    /**
     * <p></p>
     */
    public static Object ValidasiODC
     
    /**
     * <p></p>
     */
    public static Object ValidasiNonClientGMA
     
    /**
     * <p></p>
     */
    public static Object ValidasiODSConfirm
     
    /**
     * <p></p>
     */
    public static Object ValidasiFamilyPlanning
     
    /**
     * <p></p>
     */
    public static Object ValidasiPerhitunganExcess
     
    /**
     * <p></p>
     */
    public static Object TrID1
     
    /**
     * <p></p>
     */
    public static Object TrID2
     
    /**
     * <p></p>
     */
    public static Object TrID3
     
    /**
     * <p></p>
     */
    public static Object TrID4
     
    /**
     * <p></p>
     */
    public static Object TrID5
     
    /**
     * <p></p>
     */
    public static Object Username
     
    /**
     * <p></p>
     */
    public static Object Password
     
    /**
     * <p></p>
     */
    public static Object SummaryMenungguKonfirmasi
     
    /**
     * <p></p>
     */
    public static Object dataGasi
     
    /**
     * <p></p>
     */
    public static Object usernameGMA
     
    /**
     * <p></p>
     */
    public static Object passwordGMA
     

    static {
        try {
            def selectedVariables = TestCaseMain.getGlobalVariables("default")
			selectedVariables += TestCaseMain.getGlobalVariables(RunConfiguration.getExecutionProfile())
            selectedVariables += RunConfiguration.getOverridingParameters()
    
            URLGardaAksesProduction = selectedVariables['URLGardaAksesProduction']
            URLGardaAksesQC = selectedVariables['URLGardaAksesQC']
            URLGardaAkses4 = selectedVariables['URLGardaAkses4']
            URLGardaAkses5 = selectedVariables['URLGardaAkses5']
            URLGardaMedikaAkses = selectedVariables['URLGardaMedikaAkses']
            Delay1 = selectedVariables['Delay1']
            Delay2 = selectedVariables['Delay2']
            Delay3 = selectedVariables['Delay3']
            NewMemberName = selectedVariables['NewMemberName']
            TicketID1 = selectedVariables['TicketID1']
            TicketID2 = selectedVariables['TicketID2']
            TicketID3 = selectedVariables['TicketID3']
            TicketID4 = selectedVariables['TicketID4']
            TicketID5 = selectedVariables['TicketID5']
            PhoneNumber = selectedVariables['PhoneNumber']
            Email = selectedVariables['Email']
            FaxNumber = selectedVariables['FaxNumber']
            ExtNumber = selectedVariables['ExtNumber']
            SocialMedia = selectedVariables['SocialMedia']
            ValidasiDijaminkan = selectedVariables['ValidasiDijaminkan']
            ValidasiTidakDijaminkan = selectedVariables['ValidasiTidakDijaminkan']
            ValidasiDiagnosa = selectedVariables['ValidasiDiagnosa']
            ValidasiPreAdmission = selectedVariables['ValidasiPreAdmission']
            ValidasiReject = selectedVariables['ValidasiReject']
            ValidasiNewMember = selectedVariables['ValidasiNewMember']
            ValidasiAnnualLimit = selectedVariables['ValidasiAnnualLimit']
            ValidasiDocument = selectedVariables['ValidasiDocument']
            ValidasiClientNonEGL = selectedVariables['ValidasiClientNonEGL']
            ValidasiGMAClientNonEGL = selectedVariables['ValidasiGMAClientNonEGL']
            ValidasiEligibility = selectedVariables['ValidasiEligibility']
            ValidasiClaimOS = selectedVariables['ValidasiClaimOS']
            ValidasiMedicalTreatment = selectedVariables['ValidasiMedicalTreatment']
            ValidasiNonClient = selectedVariables['ValidasiNonClient']
            ValidasiProductIP = selectedVariables['ValidasiProductIP']
            ValidasiProductMA = selectedVariables['ValidasiProductMA']
            ValidasiCoverageCovMA = selectedVariables['ValidasiCoverageCovMA']
            ValidasiCoverageUncovMA = selectedVariables['ValidasiCoverageUncovMA']
            ValidasiGMAAppropriateBlank = selectedVariables['ValidasiGMAAppropriateBlank']
            ValidasiODS = selectedVariables['ValidasiODS']
            ValidasiODC = selectedVariables['ValidasiODC']
            ValidasiNonClientGMA = selectedVariables['ValidasiNonClientGMA']
            ValidasiODSConfirm = selectedVariables['ValidasiODSConfirm']
            ValidasiFamilyPlanning = selectedVariables['ValidasiFamilyPlanning']
            ValidasiPerhitunganExcess = selectedVariables['ValidasiPerhitunganExcess']
            TrID1 = selectedVariables['TrID1']
            TrID2 = selectedVariables['TrID2']
            TrID3 = selectedVariables['TrID3']
            TrID4 = selectedVariables['TrID4']
            TrID5 = selectedVariables['TrID5']
            Username = selectedVariables['Username']
            Password = selectedVariables['Password']
            SummaryMenungguKonfirmasi = selectedVariables['SummaryMenungguKonfirmasi']
            dataGasi = selectedVariables['dataGasi']
            usernameGMA = selectedVariables['usernameGMA']
            passwordGMA = selectedVariables['passwordGMA']
            
        } catch (Exception e) {
            TestCaseMain.logGlobalVariableError(e)
        }
    }
}
