package gardaAkses

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.kms.katalon.core.testdata.DBData as DBData
import com.keyword.GEN5 as GEN5
import com.keyword.UI as UI

import internal.GlobalVariable

public class HandleTicketFU  {

	@Keyword
	public static def FollowUp(String PhaseFU, String PhaseGL, String UserID, String Password, String Menu, String SubMenuFU){
		//TrID Exist
		if (PhaseFU == '1') {
			GlobalVariable.TrID1 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
		} else if (PhaseFU == '2') {
			GlobalVariable.TrID2 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
		} else if (PhaseFU == '3') {
			GlobalVariable.TrID3 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
		} else if (PhaseFU == '4') {
			GlobalVariable.TrID4 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
		} else if (PhaseFU == '5') {
			GlobalVariable.TrID5 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
		}

		String FUTrID1

		String FUTrID2

		String FUTrID3

		String FUTrID4

		String FUTrID5

		//GetTrID
		if (PhaseGL == '1') {
			DBData dbTestData = findTestData('GetTrID')

			dbTestData.query = dbTestData.query.replace('_TrID_', GlobalVariable.TicketID1)

			dbTestData.fetchedData = dbTestData.fetchData()

			FUTrID1 = dbTestData.getValue(1, 1)

			WebUI.comment(FUTrID1)
		} else if (PhaseGL == '2') {
			DBData dbTestData = findTestData('GetTrID')

			dbTestData.query = dbTestData.query.replace('_TrID_', GlobalVariable.TicketID2)

			dbTestData.fetchedData = dbTestData.fetchData()

			FUTrID2 = dbTestData.getValue(1, 1)

			WebUI.comment(FUTrID2)
		} else if (PhaseGL == '3') {
			DBData dbTestData = findTestData('GetTrID')

			dbTestData.query = dbTestData.query.replace('_TrID_', GlobalVariable.TicketID3)

			dbTestData.fetchedData = dbTestData.fetchData()

			FUTrID3 = dbTestData.getValue(1, 1)

			WebUI.comment(FUTrID3)
		} else if (PhaseGL == '4') {
			DBData dbTestData = findTestData('GetTrID')

			dbTestData.query = dbTestData.query.replace('_TrID_', GlobalVariable.TicketID4)

			dbTestData.fetchedData = dbTestData.fetchData()

			FUTrID4 = dbTestData.getValue(1, 1)

			WebUI.comment(FUTrID4)
		} else if (PhaseGL == '5') {
			DBData dbTestData = findTestData('GetTrID')

			dbTestData.query = dbTestData.query.replace('_TrID_', GlobalVariable.TicketID5)

			dbTestData.fetchedData = dbTestData.fetchData()

			FUTrID5 = dbTestData.getValue(1, 1)

			WebUI.comment(FUTrID5)
		}

		if (PhaseGL == '1') {
			if (PhaseFU == '1') {
				while (FUTrID1 != GlobalVariable.TrID1) {
					def QueryFinish = (('UPDATE dbo.CALLIN SET CallStatus = \'Finished\' WHERE TrID in (\'' + GlobalVariable.TrID1) + '\')')

					GEN5.updateValueDatabase('172.16.94.70', 'SEA', QueryFinish)

					def QueryUpdatePIC = (((('UPDATE  ContactCenter.FollowUpTicketHistory SET UserID = \'' + UserID) + '\' WHERE FollowUpTicketHistoryID IN ( \'') +
							FUTrID1) + '\')')

					GEN5.updateValueDatabase('172.16.94.70', 'SEA', QueryUpdatePIC)

					WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Login/Login'), [('UserID') : UserID, ('Password') : Password])

					WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Home/Home'), [('Menu') : Menu, ('SubMenu') : SubMenuFU])

					WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Follow Up/Follow Up - Inquiry'), [:])

					//TrID
					if (PhaseFU == '1') {
						GlobalVariable.TrID1 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '2') {
						GlobalVariable.TrID2 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '3') {
						GlobalVariable.TrID3 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '4') {
						GlobalVariable.TrID4 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '5') {
						GlobalVariable.TrID5 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					}
				}
			} else if (PhaseFU == '2') {
				while (FUTrID1 != GlobalVariable.TrID2) {
					def QueryFinish = (('UPDATE dbo.CALLIN SET CallStatus = \'Finished\' WHERE TrID in (\'' + GlobalVariable.TrID2) + '\')')

					GEN5.updateValueDatabase('172.16.94.70', 'SEA', QueryFinish)

					def QueryUpdatePIC = (((('UPDATE  ContactCenter.FollowUpTicketHistory SET UserID = \'' + UserID) + '\' WHERE FollowUpTicketHistoryID IN ( \'') +
							FUTrID1) + '\')')

					GEN5.updateValueDatabase('172.16.94.70', 'SEA', QueryUpdatePIC)

					WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Login/Login'), [('UserID') : UserID, ('Password') : Password])

					WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Home/Home'), [('Menu') : Menu, ('SubMenu') : SubMenuFU])

					WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Follow Up/Follow Up - Inquiry'), [:])

					//TrID
					if (PhaseFU == '1') {
						GlobalVariable.TrID1 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '2') {
						GlobalVariable.TrID2 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '3') {
						GlobalVariable.TrID3 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '4') {
						GlobalVariable.TrID4 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '5') {
						GlobalVariable.TrID5 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					}
				}
			} else if (PhaseFU == '3') {
				while (FUTrID1 != GlobalVariable.TrID3) {
					def QueryFinish = (('UPDATE dbo.CALLIN SET CallStatus = \'Finished\' WHERE TrID in (\'' + GlobalVariable.TrID3) + '\')')

					GEN5.updateValueDatabase('172.16.94.70', 'SEA', QueryFinish)

					def QueryUpdatePIC = (((('UPDATE  ContactCenter.FollowUpTicketHistory SET UserID = \'' + UserID) + '\' WHERE FollowUpTicketHistoryID IN ( \'') +
							FUTrID1) + '\')')

					GEN5.updateValueDatabase('172.16.94.70', 'SEA', QueryUpdatePIC)

					WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Login/Login'), [('UserID') : UserID, ('Password') : Password])

					WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Home/Home'), [('Menu') : Menu, ('SubMenu') : SubMenuFU])

					WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Follow Up/Follow Up - Inquiry'), [:])

					//TrID
					if (PhaseFU == '1') {
						GlobalVariable.TrID1 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '2') {
						GlobalVariable.TrID2 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '3') {
						GlobalVariable.TrID3 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '4') {
						GlobalVariable.TrID4 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '5') {
						GlobalVariable.TrID5 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					}
				}
			} else if (PhaseFU == '4') {
				while (FUTrID1 != GlobalVariable.TrID4) {
					def QueryFinish = (('UPDATE dbo.CALLIN SET CallStatus = \'Finished\' WHERE TrID in (\'' + GlobalVariable.TrID4) + '\')')

					GEN5.updateValueDatabase('172.16.94.70', 'SEA', QueryFinish)

					def QueryUpdatePIC = (((('UPDATE  ContactCenter.FollowUpTicketHistory SET UserID = \'' + UserID) + '\' WHERE FollowUpTicketHistoryID IN ( \'') +
							FUTrID1) + '\')')

					GEN5.updateValueDatabase('172.16.94.70', 'SEA', QueryUpdatePIC)

					WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Login/Login'), [('UserID') : UserID, ('Password') : Password])

					WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Home/Home'), [('Menu') : Menu, ('SubMenu') : SubMenuFU])

					WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Follow Up/Follow Up - Inquiry'), [:])

					//TrID
					if (PhaseFU == '1') {
						GlobalVariable.TrID1 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '2') {
						GlobalVariable.TrID2 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '3') {
						GlobalVariable.TrID3 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '4') {
						GlobalVariable.TrID4 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '5') {
						GlobalVariable.TrID5 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					}
				}
			} else if (PhaseFU == '5') {
				while (FUTrID1 != GlobalVariable.TrID5) {
					def QueryFinish = (('UPDATE dbo.CALLIN SET CallStatus = \'Finished\' WHERE TrID in (\'' + GlobalVariable.TrID5) + '\')')

					GEN5.updateValueDatabase('172.16.94.70', 'SEA', QueryFinish)

					def QueryUpdatePIC = (((('UPDATE  ContactCenter.FollowUpTicketHistory SET UserID = \'' + UserID) + '\' WHERE FollowUpTicketHistoryID IN ( \'') +
							FUTrID1) + '\')')

					GEN5.updateValueDatabase('172.16.94.70', 'SEA', QueryUpdatePIC)

					WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Login/Login'), [('UserID') : UserID, ('Password') : Password])

					WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Home/Home'), [('Menu') : Menu, ('SubMenu') : SubMenuFU])

					WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Follow Up/Follow Up - Inquiry'), [:])

					//TrID
					if (PhaseFU == '1') {
						GlobalVariable.TrID1 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '2') {
						GlobalVariable.TrID2 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '3') {
						GlobalVariable.TrID3 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '4') {
						GlobalVariable.TrID4 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '5') {
						GlobalVariable.TrID5 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					}
				}
			}
		} else if (PhaseGL == '2') {
			if (PhaseFU == '1') {
				while (FUTrID2 != GlobalVariable.TrID1) {
					def QueryFinish = (('UPDATE dbo.CALLIN SET CallStatus = \'Finished\' WHERE TrID in (\'' + GlobalVariable.TrID1) + '\')')

					GEN5.updateValueDatabase('172.16.94.70', 'SEA', QueryFinish)

					def QueryUpdatePIC = (((('UPDATE  ContactCenter.FollowUpTicketHistory SET UserID = \'' + UserID) + '\' WHERE FollowUpTicketHistoryID IN ( \'') +
							FUTrID2) + '\')')

					GEN5.updateValueDatabase('172.16.94.70', 'SEA', QueryUpdatePIC)

					WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Login/Login'), [('UserID') : UserID, ('Password') : Password])

					WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Home/Home'), [('Menu') : Menu, ('SubMenu') : SubMenuFU])

					WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Follow Up/Follow Up - Inquiry'), [:])

					//TrID
					if (PhaseFU == '1') {
						GlobalVariable.TrID1 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '2') {
						GlobalVariable.TrID2 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '3') {
						GlobalVariable.TrID3 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '4') {
						GlobalVariable.TrID4 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '5') {
						GlobalVariable.TrID5 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					}
				}
			} else if (PhaseFU == '2') {
				while (FUTrID2 != GlobalVariable.TrID2) {
					def QueryFinish = (('UPDATE dbo.CALLIN SET CallStatus = \'Finished\' WHERE TrID in (\'' + GlobalVariable.TrID2) + '\')')

					GEN5.updateValueDatabase('172.16.94.70', 'SEA', QueryFinish)

					def QueryUpdatePIC = (((('UPDATE  ContactCenter.FollowUpTicketHistory SET UserID = \'' + UserID) + '\' WHERE FollowUpTicketHistoryID IN ( \'') +
							FUTrID2) + '\')')

					GEN5.updateValueDatabase('172.16.94.70', 'SEA', QueryUpdatePIC)

					WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Login/Login'), [('UserID') : UserID, ('Password') : Password])

					WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Home/Home'), [('Menu') : Menu, ('SubMenu') : SubMenuFU])

					WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Follow Up/Follow Up - Inquiry'), [:])

					//TrID
					if (PhaseFU == '1') {
						GlobalVariable.TrID1 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '2') {
						GlobalVariable.TrID2 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '3') {
						GlobalVariable.TrID3 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '4') {
						GlobalVariable.TrID4 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '5') {
						GlobalVariable.TrID5 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					}
				}
			} else if (PhaseFU == '3') {
				while (FUTrID2 != GlobalVariable.TrID3) {
					def QueryFinish = (('UPDATE dbo.CALLIN SET CallStatus = \'Finished\' WHERE TrID in (\'' + GlobalVariable.TrID3) + '\')')

					GEN5.updateValueDatabase('172.16.94.70', 'SEA', QueryFinish)

					def QueryUpdatePIC = (((('UPDATE  ContactCenter.FollowUpTicketHistory SET UserID = \'' + UserID) + '\' WHERE FollowUpTicketHistoryID IN ( \'') +
							FUTrID2) + '\')')

					GEN5.updateValueDatabase('172.16.94.70', 'SEA', QueryUpdatePIC)

					WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Login/Login'), [('UserID') : UserID, ('Password') : Password])

					WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Home/Home'), [('Menu') : Menu, ('SubMenu') : SubMenuFU])

					WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Follow Up/Follow Up - Inquiry'), [:])

					//TrID
					if (PhaseFU == '1') {
						GlobalVariable.TrID1 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '2') {
						GlobalVariable.TrID2 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '3') {
						GlobalVariable.TrID3 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '4') {
						GlobalVariable.TrID4 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '5') {
						GlobalVariable.TrID5 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					}
				}
			} else if (PhaseFU == '4') {
				while (FUTrID2 != GlobalVariable.TrID4) {
					def QueryFinish = (('UPDATE dbo.CALLIN SET CallStatus = \'Finished\' WHERE TrID in (\'' + GlobalVariable.TrID4) + '\')')

					GEN5.updateValueDatabase('172.16.94.70', 'SEA', QueryFinish)

					def QueryUpdatePIC = (((('UPDATE  ContactCenter.FollowUpTicketHistory SET UserID = \'' + UserID) + '\' WHERE FollowUpTicketHistoryID IN ( \'') +
							FUTrID2) + '\')')

					GEN5.updateValueDatabase('172.16.94.70', 'SEA', QueryUpdatePIC)

					WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Login/Login'), [('UserID') : UserID, ('Password') : Password])

					WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Home/Home'), [('Menu') : Menu, ('SubMenu') : SubMenuFU])

					WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Follow Up/Follow Up - Inquiry'), [:])

					//TrID
					if (PhaseFU == '1') {
						GlobalVariable.TrID1 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '2') {
						GlobalVariable.TrID2 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '3') {
						GlobalVariable.TrID3 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '4') {
						GlobalVariable.TrID4 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '5') {
						GlobalVariable.TrID5 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					}
				}
			} else if (PhaseFU == '5') {
				while (FUTrID2 != GlobalVariable.TrID5) {
					def QueryFinish = (('UPDATE dbo.CALLIN SET CallStatus = \'Finished\' WHERE TrID in (\'' + GlobalVariable.TrID5) + '\')')

					GEN5.updateValueDatabase('172.16.94.70', 'SEA', QueryFinish)

					def QueryUpdatePIC = (((('UPDATE  ContactCenter.FollowUpTicketHistory SET UserID = \'' + UserID) + '\' WHERE FollowUpTicketHistoryID IN ( \'') +
							FUTrID2) + '\')')

					GEN5.updateValueDatabase('172.16.94.70', 'SEA', QueryUpdatePIC)

					WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Login/Login'), [('UserID') : UserID, ('Password') : Password])

					WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Home/Home'), [('Menu') : Menu, ('SubMenu') : SubMenuFU])

					WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Follow Up/Follow Up - Inquiry'), [:])

					//TrID
					if (PhaseFU == '1') {
						GlobalVariable.TrID1 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '2') {
						GlobalVariable.TrID2 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '3') {
						GlobalVariable.TrID3 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '4') {
						GlobalVariable.TrID4 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '5') {
						GlobalVariable.TrID5 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					}
				}
			}
		} else if (PhaseGL == '3') {
			if (PhaseFU == '1') {
				while (FUTrID3 != GlobalVariable.TrID1) {
					def QueryFinish = (('UPDATE dbo.CALLIN SET CallStatus = \'Finished\' WHERE TrID in (\'' + GlobalVariable.TrID1) + '\')')

					GEN5.updateValueDatabase('172.16.94.70', 'SEA', QueryFinish)

					def QueryUpdatePIC = (((('UPDATE  ContactCenter.FollowUpTicketHistory SET UserID = \'' + UserID) + '\' WHERE FollowUpTicketHistoryID IN ( \'') +
							FUTrID3) + '\')')

					GEN5.updateValueDatabase('172.16.94.70', 'SEA', QueryUpdatePIC)

					WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Login/Login'), [('UserID') : UserID, ('Password') : Password])

					WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Home/Home'), [('Menu') : Menu, ('SubMenu') : SubMenuFU])

					WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Follow Up/Follow Up - Inquiry'), [:])

					//TrID
					if (PhaseFU == '1') {
						GlobalVariable.TrID1 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '2') {
						GlobalVariable.TrID2 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '3') {
						GlobalVariable.TrID3 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '4') {
						GlobalVariable.TrID4 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '5') {
						GlobalVariable.TrID5 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					}
				}
			} else if (PhaseFU == '2') {
				while (FUTrID3 != GlobalVariable.TrID2) {
					def QueryFinish = (('UPDATE dbo.CALLIN SET CallStatus = \'Finished\' WHERE TrID in (\'' + GlobalVariable.TrID2) + '\')')

					GEN5.updateValueDatabase('172.16.94.70', 'SEA', QueryFinish)

					def QueryUpdatePIC = (((('UPDATE  ContactCenter.FollowUpTicketHistory SET UserID = \'' + UserID) + '\' WHERE FollowUpTicketHistoryID IN ( \'') +
							FUTrID3) + '\')')

					GEN5.updateValueDatabase('172.16.94.70', 'SEA', QueryUpdatePIC)

					WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Login/Login'), [('UserID') : UserID, ('Password') : Password])

					WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Home/Home'), [('Menu') : Menu, ('SubMenu') : SubMenuFU])

					WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Follow Up/Follow Up - Inquiry'), [:])

					//TrID
					if (PhaseFU == '1') {
						GlobalVariable.TrID1 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '2') {
						GlobalVariable.TrID2 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '3') {
						GlobalVariable.TrID3 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '4') {
						GlobalVariable.TrID4 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '5') {
						GlobalVariable.TrID5 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					}
				}
			} else if (PhaseFU == '3') {
				while (FUTrID3 != GlobalVariable.TrID3) {
					def QueryFinish = (('UPDATE dbo.CALLIN SET CallStatus = \'Finished\' WHERE TrID in (\'' + GlobalVariable.TrID3) + '\')')

					GEN5.updateValueDatabase('172.16.94.70', 'SEA', QueryFinish)

					def QueryUpdatePIC = (((('UPDATE  ContactCenter.FollowUpTicketHistory SET UserID = \'' + UserID) + '\' WHERE FollowUpTicketHistoryID IN ( \'') +
							FUTrID3) + '\')')

					GEN5.updateValueDatabase('172.16.94.70', 'SEA', QueryUpdatePIC)

					WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Login/Login'), [('UserID') : UserID, ('Password') : Password])

					WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Home/Home'), [('Menu') : Menu, ('SubMenu') : SubMenuFU])

					WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Follow Up/Follow Up - Inquiry'), [:])

					//TrID
					if (PhaseFU == '1') {
						GlobalVariable.TrID1 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '2') {
						GlobalVariable.TrID2 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '3') {
						GlobalVariable.TrID3 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '4') {
						GlobalVariable.TrID4 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '5') {
						GlobalVariable.TrID5 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					}
				}
			} else if (PhaseFU == '4') {
				while (FUTrID3 != GlobalVariable.TrID4) {
					def QueryFinish = (('UPDATE dbo.CALLIN SET CallStatus = \'Finished\' WHERE TrID in (\'' + GlobalVariable.TrID4) + '\')')

					GEN5.updateValueDatabase('172.16.94.70', 'SEA', QueryFinish)

					def QueryUpdatePIC = (((('UPDATE  ContactCenter.FollowUpTicketHistory SET UserID = \'' + UserID) + '\' WHERE FollowUpTicketHistoryID IN ( \'') +
							FUTrID3) + '\')')

					GEN5.updateValueDatabase('172.16.94.70', 'SEA', QueryUpdatePIC)

					WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Login/Login'), [('UserID') : UserID, ('Password') : Password])

					WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Home/Home'), [('Menu') : Menu, ('SubMenu') : SubMenuFU])

					WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Follow Up/Follow Up - Inquiry'), [:])

					//TrID
					if (PhaseFU == '1') {
						GlobalVariable.TrID1 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '2') {
						GlobalVariable.TrID2 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '3') {
						GlobalVariable.TrID3 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '4') {
						GlobalVariable.TrID4 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '5') {
						GlobalVariable.TrID5 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					}
				}
			} else if (PhaseFU == '5') {
				while (FUTrID3 != GlobalVariable.TrID5) {
					def QueryFinish = (('UPDATE dbo.CALLIN SET CallStatus = \'Finished\' WHERE TrID in (\'' + GlobalVariable.TrID5) + '\')')

					GEN5.updateValueDatabase('172.16.94.70', 'SEA', QueryFinish)

					def QueryUpdatePIC = (((('UPDATE  ContactCenter.FollowUpTicketHistory SET UserID = \'' + UserID) + '\' WHERE FollowUpTicketHistoryID IN ( \'') +
							FUTrID3) + '\')')

					GEN5.updateValueDatabase('172.16.94.70', 'SEA', QueryUpdatePIC)

					WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Login/Login'), [('UserID') : UserID, ('Password') : Password])

					WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Home/Home'), [('Menu') : Menu, ('SubMenu') : SubMenuFU])

					WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Follow Up/Follow Up - Inquiry'), [:])

					//TrID
					if (PhaseFU == '1') {
						GlobalVariable.TrID1 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '2') {
						GlobalVariable.TrID2 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '3') {
						GlobalVariable.TrID3 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '4') {
						GlobalVariable.TrID4 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '5') {
						GlobalVariable.TrID5 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					}
				}
			}
		} else if (PhaseGL == '4') {
			if (PhaseFU == '1') {
				while (FUTrID4 != GlobalVariable.TrID1) {
					def QueryFinish = (('UPDATE dbo.CALLIN SET CallStatus = \'Finished\' WHERE TrID in (\'' + GlobalVariable.TrID1) + '\')')

					GEN5.updateValueDatabase('172.16.94.70', 'SEA', QueryFinish)

					def QueryUpdatePIC = (((('UPDATE  ContactCenter.FollowUpTicketHistory SET UserID = \'' + UserID) + '\' WHERE FollowUpTicketHistoryID IN ( \'') +
							FUTrID4) + '\')')

					GEN5.updateValueDatabase('172.16.94.70', 'SEA', QueryUpdatePIC)

					WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Login/Login'), [('UserID') : UserID, ('Password') : Password])

					WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Home/Home'), [('Menu') : Menu, ('SubMenu') : SubMenuFU])

					WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Follow Up/Follow Up - Inquiry'), [:])

					//TrID
					if (PhaseFU == '1') {
						GlobalVariable.TrID1 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '2') {
						GlobalVariable.TrID2 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '3') {
						GlobalVariable.TrID3 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '4') {
						GlobalVariable.TrID4 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '5') {
						GlobalVariable.TrID5 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					}
				}
			} else if (PhaseFU == '2') {
				while (FUTrID4 != GlobalVariable.TrID2) {
					def QueryFinish = (('UPDATE dbo.CALLIN SET CallStatus = \'Finished\' WHERE TrID in (\'' + GlobalVariable.TrID2) + '\')')

					GEN5.updateValueDatabase('172.16.94.70', 'SEA', QueryFinish)

					def QueryUpdatePIC = (((('UPDATE  ContactCenter.FollowUpTicketHistory SET UserID = \'' + UserID) + '\' WHERE FollowUpTicketHistoryID IN ( \'') +
							FUTrID4) + '\')')

					GEN5.updateValueDatabase('172.16.94.70', 'SEA', QueryUpdatePIC)

					WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Login/Login'), [('UserID') : UserID, ('Password') : Password])

					WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Home/Home'), [('Menu') : Menu, ('SubMenu') : SubMenuFU])

					WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Follow Up/Follow Up - Inquiry'), [:])

					//TrID
					if (PhaseFU == '1') {
						GlobalVariable.TrID1 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '2') {
						GlobalVariable.TrID2 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '3') {
						GlobalVariable.TrID3 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '4') {
						GlobalVariable.TrID4 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '5') {
						GlobalVariable.TrID5 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					}
				}
			} else if (PhaseFU == '3') {
				while (FUTrID4 != GlobalVariable.TrID3) {
					def QueryFinish = (('UPDATE dbo.CALLIN SET CallStatus = \'Finished\' WHERE TrID in (\'' + GlobalVariable.TrID3) + '\')')

					GEN5.updateValueDatabase('172.16.94.70', 'SEA', QueryFinish)

					def QueryUpdatePIC = (((('UPDATE  ContactCenter.FollowUpTicketHistory SET UserID = \'' + UserID) + '\' WHERE FollowUpTicketHistoryID IN ( \'') +
							FUTrID4) + '\')')

					GEN5.updateValueDatabase('172.16.94.70', 'SEA', QueryUpdatePIC)

					WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Login/Login'), [('UserID') : UserID, ('Password') : Password])

					WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Home/Home'), [('Menu') : Menu, ('SubMenu') : SubMenuFU])

					WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Follow Up/Follow Up - Inquiry'), [:])

					//TrID
					if (PhaseFU == '1') {
						GlobalVariable.TrID1 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '2') {
						GlobalVariable.TrID2 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '3') {
						GlobalVariable.TrID3 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '4') {
						GlobalVariable.TrID4 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '5') {
						GlobalVariable.TrID5 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					}
				}
			} else if (PhaseFU == '4') {
				while (FUTrID4 != GlobalVariable.TrID4) {
					def QueryFinish = (('UPDATE dbo.CALLIN SET CallStatus = \'Finished\' WHERE TrID in (\'' + GlobalVariable.TrID4) + '\')')

					GEN5.updateValueDatabase('172.16.94.70', 'SEA', QueryFinish)

					def QueryUpdatePIC = (((('UPDATE  ContactCenter.FollowUpTicketHistory SET UserID = \'' + UserID) + '\' WHERE FollowUpTicketHistoryID IN ( \'') +
							FUTrID4) + '\')')

					GEN5.updateValueDatabase('172.16.94.70', 'SEA', QueryUpdatePIC)

					WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Login/Login'), [('UserID') : UserID, ('Password') : Password])

					WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Home/Home'), [('Menu') : Menu, ('SubMenu') : SubMenuFU])

					WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Follow Up/Follow Up - Inquiry'), [:])

					//TrID
					if (PhaseFU == '1') {
						GlobalVariable.TrID1 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '2') {
						GlobalVariable.TrID2 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '3') {
						GlobalVariable.TrID3 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '4') {
						GlobalVariable.TrID4 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '5') {
						GlobalVariable.TrID5 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					}
				}
			} else if (PhaseFU == '5') {
				while (FUTrID4 != GlobalVariable.TrID5) {
					def QueryFinish = (('UPDATE dbo.CALLIN SET CallStatus = \'Finished\' WHERE TrID in (\'' + GlobalVariable.TrID5) + '\')')

					GEN5.updateValueDatabase('172.16.94.70', 'SEA', QueryFinish)

					def QueryUpdatePIC = (((('UPDATE  ContactCenter.FollowUpTicketHistory SET UserID = \'' + UserID) + '\' WHERE FollowUpTicketHistoryID IN ( \'') +
							FUTrID4) + '\')')

					GEN5.updateValueDatabase('172.16.94.70', 'SEA', QueryUpdatePIC)

					WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Login/Login'), [('UserID') : UserID, ('Password') : Password])

					WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Home/Home'), [('Menu') : Menu, ('SubMenu') : SubMenuFU])

					WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Follow Up/Follow Up - Inquiry'), [:])

					//TrID
					if (PhaseFU == '1') {
						GlobalVariable.TrID1 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '2') {
						GlobalVariable.TrID2 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '3') {
						GlobalVariable.TrID3 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '4') {
						GlobalVariable.TrID4 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '5') {
						GlobalVariable.TrID5 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					}
				}
			}
		} else if (PhaseGL == '5') {
			if (PhaseFU == '1') {
				while (FUTrID5 != GlobalVariable.TrID1) {
					def QueryFinish = (('UPDATE dbo.CALLIN SET CallStatus = \'Finished\' WHERE TrID in (\'' + GlobalVariable.TrID1) + '\')')

					GEN5.updateValueDatabase('172.16.94.70', 'SEA', QueryFinish)

					def QueryUpdatePIC = (((('UPDATE  ContactCenter.FollowUpTicketHistory SET UserID = \'' + UserID) + '\' WHERE FollowUpTicketHistoryID IN ( \'') +
							FUTrID5) + '\')')

					GEN5.updateValueDatabase('172.16.94.70', 'SEA', QueryUpdatePIC)

					WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Login/Login'), [('UserID') : UserID, ('Password') : Password])

					WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Home/Home'), [('Menu') : Menu, ('SubMenu') : SubMenuFU])

					WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Follow Up/Follow Up - Inquiry'), [:])

					//TrID
					if (PhaseFU == '1') {
						GlobalVariable.TrID1 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '2') {
						GlobalVariable.TrID2 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '3') {
						GlobalVariable.TrID3 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '4') {
						GlobalVariable.TrID4 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '5') {
						GlobalVariable.TrID5 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					}
				}
			} else if (PhaseFU == '2') {
				while (FUTrID5 != GlobalVariable.TrID2) {
					def QueryFinish = (('UPDATE dbo.CALLIN SET CallStatus = \'Finished\' WHERE TrID in (\'' + GlobalVariable.TrID2) + '\')')

					GEN5.updateValueDatabase('172.16.94.70', 'SEA', QueryFinish)

					def QueryUpdatePIC = (((('UPDATE  ContactCenter.FollowUpTicketHistory SET UserID = \'' + UserID) + '\' WHERE FollowUpTicketHistoryID IN ( \'') +
							FUTrID5) + '\')')

					GEN5.updateValueDatabase('172.16.94.70', 'SEA', QueryUpdatePIC)

					WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Login/Login'), [('UserID') : UserID, ('Password') : Password])

					WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Home/Home'), [('Menu') : Menu, ('SubMenu') : SubMenuFU])

					WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Follow Up/Follow Up - Inquiry'), [:])

					//TrID
					if (PhaseFU == '1') {
						GlobalVariable.TrID1 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '2') {
						GlobalVariable.TrID2 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '3') {
						GlobalVariable.TrID3 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '4') {
						GlobalVariable.TrID4 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '5') {
						GlobalVariable.TrID5 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					}
				}
			} else if (PhaseFU == '3') {
				while (FUTrID5 != GlobalVariable.TrID3) {
					def QueryFinish = (('UPDATE dbo.CALLIN SET CallStatus = \'Finished\' WHERE TrID in (\'' + GlobalVariable.TrID3) + '\')')

					GEN5.updateValueDatabase('172.16.94.70', 'SEA', QueryFinish)

					def QueryUpdatePIC = (((('UPDATE  ContactCenter.FollowUpTicketHistory SET UserID = \'' + UserID) + '\' WHERE FollowUpTicketHistoryID IN ( \'') +
							FUTrID5) + '\')')

					GEN5.updateValueDatabase('172.16.94.70', 'SEA', QueryUpdatePIC)

					WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Login/Login'), [('UserID') : UserID, ('Password') : Password])

					WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Home/Home'), [('Menu') : Menu, ('SubMenu') : SubMenuFU])

					WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Follow Up/Follow Up - Inquiry'), [:])

					//TrID
					if (PhaseFU == '1') {
						GlobalVariable.TrID1 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '2') {
						GlobalVariable.TrID2 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '3') {
						GlobalVariable.TrID3 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '4') {
						GlobalVariable.TrID4 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '5') {
						GlobalVariable.TrID5 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					}
				}
			} else if (PhaseFU == '4') {
				while (FUTrID5 != GlobalVariable.TrID4) {
					def QueryFinish = (('UPDATE dbo.CALLIN SET CallStatus = \'Finished\' WHERE TrID in (\'' + GlobalVariable.TrID4) + '\')')

					GEN5.updateValueDatabase('172.16.94.70', 'SEA', QueryFinish)

					def QueryUpdatePIC = (((('UPDATE  ContactCenter.FollowUpTicketHistory SET UserID = \'' + UserID) + '\' WHERE FollowUpTicketHistoryID IN ( \'') +
							FUTrID5) + '\')')

					GEN5.updateValueDatabase('172.16.94.70', 'SEA', QueryUpdatePIC)

					WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Login/Login'), [('UserID') : UserID, ('Password') : Password])

					WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Home/Home'), [('Menu') : Menu, ('SubMenu') : SubMenuFU])

					WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Follow Up/Follow Up - Inquiry'), [:])

					//TrID
					if (PhaseFU == '1') {
						GlobalVariable.TrID1 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '2') {
						GlobalVariable.TrID2 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '3') {
						GlobalVariable.TrID3 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '4') {
						GlobalVariable.TrID4 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '5') {
						GlobalVariable.TrID5 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					}
				}
			} else if (PhaseFU == '5') {
				while (FUTrID5 != GlobalVariable.TrID5) {
					def QueryFinish = (('UPDATE dbo.CALLIN SET CallStatus = \'Finished\' WHERE TrID in (\'' + GlobalVariable.TrID5) + '\')')

					GEN5.updateValueDatabase('172.16.94.70', 'SEA', QueryFinish)

					def QueryUpdatePIC = (((('UPDATE  ContactCenter.FollowUpTicketHistory SET UserID = \'' + UserID) + '\' WHERE FollowUpTicketHistoryID IN ( \'') +
							FUTrID5) + '\')')

					GEN5.updateValueDatabase('172.16.94.70', 'SEA', QueryUpdatePIC)

					WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Login/Login'), [('UserID') : UserID, ('Password') : Password])

					WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Home/Home'), [('Menu') : Menu, ('SubMenu') : SubMenuFU])

					WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Follow Up/Follow Up - Inquiry'), [:])

					//TrID
					if (PhaseFU == '1') {
						GlobalVariable.TrID1 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '2') {
						GlobalVariable.TrID2 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '3') {
						GlobalVariable.TrID3 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '4') {
						GlobalVariable.TrID4 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					} else if (PhaseFU == '5') {
						GlobalVariable.TrID5 = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Text - TrID'))
					}
				}
			}
		}
	}
}
