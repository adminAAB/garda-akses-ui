package querySQL

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.keyword.GEN5 as GEN5
import com.keyword.UI as UI

import internal.GlobalVariable

public class Query {

	DefaultQuery defaultQuery = new DefaultQuery()

	@Keyword
	def QueryContactName () {
		def queryContactName = 'UPDATE GardaAkses_MasterID SET Number = (SELECT Number FROM GardaAkses_MasterID WHERE Name = \'Automation Tester\')+1 WHERE Name = \'Automation Tester\''

		defaultQuery.connectDB('172.16.94.48', 'litt', 'ITAPP', 'It5Fu#H0m317')

		defaultQuery.execute(queryContactName)
	}

	@Keyword
	def QueryNewMemberName () {
		def queryNewMemberName = 'UPDATE GardaAkses_MasterID SET Number = (SELECT Number FROM GardaAkses_MasterID WHERE Name = \'Automation GA\')+1 WHERE Name = \'Automation GA\''

		defaultQuery.connectDB('172.16.94.48', 'litt', 'ITAPP', 'It5Fu#H0m317')

		defaultQuery.execute(queryNewMemberName)
	}

	@Keyword
	def QueryEmployeeID () {
		def queryEmployeeID = 'UPDATE GardaAkses_MasterID SET Number = (SELECT Number FROM GardaAkses_MasterID WHERE Name = \'EmployeeID\')+1 WHERE Name = \'EmployeeID\''

		defaultQuery.connectDB('172.16.94.48', 'litt', 'ITAPP', 'It5Fu#H0m317')

		defaultQuery.execute(queryEmployeeID)
	}

	@Keyword
	def QueryFinishFollowUp (String RemarksTreatmentInformation) {
		def queryGetTrID = 'SELECT TOP 1 TrID FROM CONTACTCENTER.eGLGardaAkses WHERE ValidationResult = \'' + RemarksTreatmentInformation +'\' ORDER BY CreatedDate DESC'

		def TrID = UI.getValueDatabase('172.16.94.70', 'SEA', queryGetTrID, 'TrID')

		def queryFinishFollowUp1 = 'UPDATE dbo.CALLIN SET CallStatus = \'Finished\' WHERE TrID = \'' + TrID + '\''
		
		def queryFinishFollowUp2 = 'UPDATE contactcenter.followupticket SET CallStatus = \'1\' WHERE TrID = \'' + TrID + '\''

		UI.updateValueDatabase('172.16.94.70', 'SEA', queryFinishFollowUp1)
		
		UI.updateValueDatabase('172.16.94.70', 'SEA', queryFinishFollowUp2)
	}

	@Keyword
	def QueryGetTrID (String RemarksTreatmentInformation) {

		def queryGetTrID = 'SELECT TOP 1 TrID FROM CONTACTCENTER.eGLGardaAkses WHERE ValidationResult = \'' + RemarksTreatmentInformation +'\' ORDER BY CreatedDate DESC'

		UI.getValueDatabase('172.16.94.70', 'SEA', queryGetTrID, 'TrID')
	}
}