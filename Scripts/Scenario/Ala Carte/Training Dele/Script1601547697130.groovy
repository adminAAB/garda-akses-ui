import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

import com.keyword.UI as UI
import java.text.SimpleDateFormat

def date = new Date()
def sdf = new SimpleDateFormat ("d MMMM yyyy")

UI.Note("Tanggal Test Katalon - " + sdf.format(date) + " - RRU")

//UI.Note(sdf.format(date))

//UI.Note("Testing Katalon -") + UI.Note(sdf.format(date)) + UI.Note("- RRU")


/*
String Tes = UI.getValueDatabase("172.16.94.48", "LiTT", "SELECT NAME + ' ' + Number AS ContactName FROM dbo.GardaAkses_MasterID WHERE Name = 'Automation Tester'", "ContactName")

println Tes

UI.Note(Tes)
*/

