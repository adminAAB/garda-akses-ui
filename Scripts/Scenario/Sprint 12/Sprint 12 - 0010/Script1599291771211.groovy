import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5 as GEN5
import com.keyword.UI as UI

//Login//
String UserID = 'LSO'

String Password = 'ITG@nt1P455QC'

//Home
String Menu = 'General'

String SubMenu = 'Create Ticket'

//Create Ticket//
String ContactLine = 'Provider'

String Product = 'Health'

String ChannelType = 'Call'

String ContactName = findTestData('ContactName').getValue(1, 1)

String ContactType = 'Lainnya'

String ServiceType = 'Claim'

String InterruptedCall = 'No'

String ProviderName = 'OJKSH00001 SILOAM HOSPITALS KEBON JERUK'

String ActionCT = 'Next'

//Inquiry//
String Phase = '1'

String MemberNo = findTestData('MemberNameNonClientShowaIndonesiaManufacturing').getValue(3, 1)

//Claim
String Member = 'Existing' //Member = Existing  / New / Check

String MemberName = findTestData('MemberNameNonClientShowaIndonesiaManufacturing').getValue(1, 1)

String ProductType = 'Inpatient (Rawat Inap)' //'Maternity (Persalinan)'

String GLType = 'Awal'

String IsODSODC = ''

String EditTreatmentPeriodStart = 'No'

String TreatmentPeriodStart = '27/Feb/2020'

String EditTreatmentPeriodEnd = 'No'

String EditTreatmentDuration = 'No'

String SpecialCondition = 'No'

String SpecialConditionReason = ''

ArrayList Diagnosis = ['New']

ArrayList StatusDiagnosa = ['Initial Primary (Diagnosa Utama)']

ArrayList DiagnosisID = ['A36']

String Gravida = '' //MA

String Partus = '' //MA

String Abortus = '' //MA

String GestationalAge = '' //MA

String RemarksDiagnosa = 'Currently testing Remarks Diagnosa by Automation. Thanks. Regards - Me'

String RemarksTreatmentInformation = ContactName + ' GL ' + GLType + ' ' + MemberNo + ' By NBH'

String RemarksTreatmentInformation2 = ContactName + ' GL ' + GLType + ' ' + MemberNo + ' Claim OS By NBH'

String MaternityTreatment = ''

String DoctorName = 'NBH ' + MemberNo

String Rujuk = 'No'

String Reason = ''

String AddFP = ''

String MedicalTreatment = ''

String TotalBilled = ''

String AppropriateRBClass = 'STANDARD'

String TreatmentRBClass = 'STANDARD'

String RoomOptionAvailability = 'On Plan'

String PackagePrice = '' //MA

String NeedFollowUp = ''

String NonMedicalItem = ''

String NewDocument = 'No'

String EditDocument = 'No'

String DeleteDocument = 'No'

String ActionGL = 'Process'

String PreviewDocument = 'Process'

String NeedFU = 'Tidak'

String NeedFU2 = 'Tidak'

ArrayList Validasi = [GlobalVariable.ValidasiDijaminkan]

ArrayList Validasi2 = [GlobalVariable.ValidasiDijaminkan]

//Exit Confirmation
String ECAction1 = 'Tidak'

String ECAction2 = 'Puas'

String Comment = 'Currently testing by Automation. Thanks. Regards - Me'

//Script//
WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Login/Login'), [('UserID') : UserID, ('Password') : Password])

//==================== PHASE 1 ====================
WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Home/Home'), [('Menu') : Menu, ('SubMenu') : SubMenu])

WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Create Ticket/Create Ticket'), [('ContactLine') : ContactLine, ('Product') : Product
        , ('ChannelType') : ChannelType, ('ContactName') : ContactName, ('ContactType') : ContactType, ('ServiceType') : ServiceType
        , ('InterruptedCall') : InterruptedCall, ('ProviderName') : ProviderName, ('Action') : ActionCT])

WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim Inquiry'), [('Phase') : '1'])

WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim'), [('Member') : Member, ('MemberName') : MemberName
        , ('ProductType') : ProductType, ('GLType') : GLType, ('IsODSODC') : IsODSODC, ('SpecialCondition') : SpecialCondition
        , ('EditTreatmentPeriodStart') : EditTreatmentPeriodStart, ('EditTreatmentPeriodEnd') : EditTreatmentPeriodEnd, ('EditTreatmentDuration') : EditTreatmentDuration
        , ('Diagnosis') : Diagnosis, ('StatusDiagnosa') : StatusDiagnosa, ('DiagnosisID') : DiagnosisID, ('Gravida') : Gravida
        , ('Partus') : Partus, ('Abortus') : Abortus, ('GestationalAge') : GestationalAge, ('RemarksDiagnosa') : RemarksDiagnosa
        , ('RemarksTreatmentInformation') : RemarksTreatmentInformation, ('MaternityTreatment') : MaternityTreatment, ('DoctorName') : DoctorName
        , ('TotalBilled') : TotalBilled, ('Rujuk') : Rujuk, ('AddFP') : AddFP, ('MedicalTreatment') : MedicalTreatment, ('Reason') : Reason
        , ('SpecialCondition') : SpecialCondition, ('SpecialConditionReason') : SpecialConditionReason, ('AppropriateRBClass') : AppropriateRBClass
        , ('TreatmentRBClass') : TreatmentRBClass, ('RoomOptionAvailability') : RoomOptionAvailability, ('PackagePrice') : PackagePrice
        , ('NeedFollowUp') : NeedFollowUp, ('NonMedicalItem') : NonMedicalItem, ('NewDocument') : NewDocument, ('EditDocument') : EditDocument
        , ('DeleteDocument') : DeleteDocument, ('ActionGL') : ActionGL, ('NeedFU') : NeedFU, ('PreviewDocument') : PreviewDocument
        , ('Validasi') : Validasi, ('Phase') : '1'])

WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Exit Confirmation/Exit Confirmation'), [('ECAction1') : ECAction1, ('ECAction2') : ECAction2
        , ('Comment') : Comment])

WebUI.comment(MemberName)

WebUI.comment(GlobalVariable.TicketID1)

def TrID = CustomKeywords.'querySQL.Query.QueryGetTrID'(RemarksTreatmentInformation)

WebUI.comment(TrID)

CustomKeywords.'querySQL.Query.QueryFinishFollowUp'(RemarksTreatmentInformation)

//==================== PHASE 2 ====================
WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Home/Home'), [('Menu') : Menu, ('SubMenu') : SubMenu])

WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Create Ticket/Create Ticket'), [('ContactLine') : ContactLine, ('Product') : Product
        , ('ChannelType') : ChannelType, ('ContactName') : ContactName, ('ContactType') : ContactType, ('ServiceType') : ServiceType
        , ('InterruptedCall') : InterruptedCall, ('ProviderName') : ProviderName, ('Action') : ActionCT])

WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim Inquiry'), [('Phase') : '1'])

WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim'), [('Member') : Member, ('MemberName') : MemberName
        , ('ProductType') : ProductType, ('GLType') : GLType, ('IsODSODC') : IsODSODC, ('SpecialCondition') : SpecialCondition
        , ('EditTreatmentPeriodStart') : EditTreatmentPeriodStart, ('EditTreatmentPeriodEnd') : EditTreatmentPeriodEnd, ('EditTreatmentDuration') : EditTreatmentDuration
        , ('Diagnosis') : Diagnosis, ('StatusDiagnosa') : StatusDiagnosa, ('DiagnosisID') : DiagnosisID, ('Gravida') : Gravida
        , ('Partus') : Partus, ('Abortus') : Abortus, ('GestationalAge') : GestationalAge, ('RemarksDiagnosa') : RemarksDiagnosa
        , ('RemarksTreatmentInformation') : RemarksTreatmentInformation2, ('MaternityTreatment') : MaternityTreatment, ('DoctorName') : DoctorName
        , ('TotalBilled') : TotalBilled, ('Rujuk') : Rujuk, ('AddFP') : AddFP, ('MedicalTreatment') : MedicalTreatment, ('Reason') : Reason
        , ('SpecialCondition') : SpecialCondition, ('SpecialConditionReason') : SpecialConditionReason, ('AppropriateRBClass') : AppropriateRBClass
        , ('TreatmentRBClass') : TreatmentRBClass, ('RoomOptionAvailability') : RoomOptionAvailability, ('PackagePrice') : PackagePrice
        , ('NeedFollowUp') : NeedFollowUp, ('NonMedicalItem') : NonMedicalItem, ('NewDocument') : NewDocument, ('EditDocument') : EditDocument
        , ('DeleteDocument') : DeleteDocument, ('ActionGL') : ActionGL, ('NeedFU') : NeedFU2, ('PreviewDocument') : PreviewDocument
        , ('Validasi') : Validasi2, ('Phase') : '2'])

WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Exit Confirmation/Exit Confirmation'), [('ECAction1') : ECAction1, ('ECAction2') : ECAction2
        , ('Comment') : Comment])

WebUI.comment(MemberName)

WebUI.comment(GlobalVariable.TicketID2)

def TrID2 = CustomKeywords.'querySQL.Query.QueryGetTrID'(RemarksTreatmentInformation2)

WebUI.comment(TrID2)

CustomKeywords.'querySQL.Query.QueryFinishFollowUp'(RemarksTreatmentInformation2)

CustomKeywords.'querySQL.Query.QueryContactName'()