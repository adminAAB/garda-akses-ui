import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

//Login//
String UserID = 'amumtazah@asuransiastra.com'

String Password = 'P@ssw0rd'

//Home
String Menu = 'Health'

String SubMenu = 'Create E-GL'

//GL Inquiry
String MemberNo = findTestData('MemberNameNonClientAstraAgroLestari').getValue(3, 51)

//Claim
String Member = findTestData('MemberNameNonClientAstraAgroLestari').getValue(1, 51)

String ProductType = 'Inpatient (Rawat Inap)' //'Maternity (Persalinan)'

String GLType = 'Awal'

String IsODSODC = '' //New

String EditTreatmentPeriod = 'No'

String TreatmentPeriodStart = ''

String TreatmentPeriodEnd = ''

String TreatmentDuration = '3'

String AdminName = findTestData('ContactName').getValue(1, 1)

String Email = 'ehu@beyond.asuransi.astra.co.id'

String Phone = ''

String ProviderPhoneExt = '17241'

ArrayList Diagnosis = ['New']

ArrayList StatusDiagnosa = ['Initial Primary (Diagnosa Utama)']

ArrayList DiagnosisID = ['A36']

String Gravida = '' //MA

String Partus = '' //MA

String Abortus = '' //MA

String GestationalAge = '' //MA

String RemarksDiagnosa = 'Currently testing Remarks Diagnosa by Automation. Thanks. Regards - Me'

String RemarksTreatmentInformation = ((((AdminName + ' GL ') + GLType) + ' ') + MemberNo) + ' By NBH'

String MaternityTreatment = ''

String DoctorName = 'NBH ' + MemberNo

String TreatmentRBClass = 'Kelas I'

String RoomOptionAvailability = 'On Plan'

String PackagePrice = '500000'

String NonMedicalItem = ''

String Action = 'Process'

String PreviewDocument = 'Process'

ArrayList Validasi = [GlobalVariable.ValidasiNonClientGMA]

//Claim Revisi

String GLTypeR = 'Revisi'

ArrayList DiagnosisR = ['']

ArrayList StatusDiagnosaR = ['']

ArrayList DiagnosisIDR = ['']

ArrayList ValidasiR = [GlobalVariable.ValidasiDijaminkan]

WebUI.comment(Member)

//Script
WebUI.callTestCase(findTestCase('Pages/Web/GardaMedikaAkses/Login/Login'), [('UserID') : UserID, ('Password') : Password])

//Phase 1
WebUI.callTestCase(findTestCase('Pages/Web/GardaMedikaAkses/Home/Home'), [('Menu') : Menu, ('SubMenu') : SubMenu])

WebUI.callTestCase(findTestCase('Pages/Web/GardaMedikaAkses/GL Inquiry/GL Inquiry'), [('Phase') : '1'])

WebUI.callTestCase(findTestCase('Pages/Web/GardaMedikaAkses/Claim/Claim'), [
	('Phase') : '1'
	, ('Member') : Member
	, ('ProductType') : ProductType
	, ('GLType') : GLType
	, ('IsODSODC') : IsODSODC
	, ('EditTreatmentPeriod') : EditTreatmentPeriod
	, ('AdminName') : AdminName
	, ('Email') : Email
	, ('ProviderPhoneExt') : ProviderPhoneExt
	, ('Diagnosis') : Diagnosis
	, ('StatusDiagnosa') : StatusDiagnosa
	, ('DiagnosisID') : DiagnosisID
	, ('Gravida') : Gravida, ('Partus') : Partus
	, ('Abortus') : Abortus, ('GestationalAge') : GestationalAge, ('RemarksDiagnosa') : RemarksDiagnosa
	, ('RemarksDiagnosa') : RemarksDiagnosa
	, ('MaternityTreatment') : MaternityTreatment
	, ('RemarksTreatmentInformation') : RemarksTreatmentInformation
	, ('DoctorName') : DoctorName
	, ('TreatmentRBClass') : TreatmentRBClass
	, ('RoomOptionAvailability') : RoomOptionAvailability
	, ('PackagePrice') : PackagePrice
	, ('NonMedicalItem') : NonMedicalItem
	, ('Action') : Action
	, ('PreviewDocument') : PreviewDocument
	, ('Validasi') : Validasi])

def TrID = CustomKeywords.'querySQL.Query.QueryGetTrID'(RemarksTreatmentInformation)

WebUI.comment(Member)

WebUI.comment(TrID)

CustomKeywords.'querySQL.Query.QueryContactName'()

//Belum bisa GL Revisi, karna GL Awal pasti kena validasi
//
//Phase 2
//WebUI.callTestCase(findTestCase('Pages/Web/GardaMedikaAkses/Home/Home'), [('Menu') : Menu, ('SubMenu') : SubMenu])
//
//WebUI.callTestCase(findTestCase('Pages/Web/GardaMedikaAkses/GL Inquiry/GL Inquiry'), [('Phase') : '2', ('MemberNo') : MemberNo])
//
//WebUI.callTestCase(findTestCase('Pages/Web/GardaMedikaAkses/Claim/Claim'), 
//	[('Phase') : '2'
//	, ('Member') : Member
//	, ('ProductType') : ProductType
//	, ('GLType') : GLTypeR
//	, ('IsODSODC') : IsODSODC 
//	, ('EditTreatmentPeriod') : EditTreatmentPeriod
//	, ('AdminName') : AdminName
//	, ('Email') : Email
//	, ('ProviderPhoneExt') : ProviderPhoneExt
//	, ('Diagnosis') : DiagnosisR
//	, ('StatusDiagnosa') : StatusDiagnosaR
//	, ('DiagnosisID') : DiagnosisIDR
//	, ('RemarksDiagnosa') : RemarksDiagnosa
//	, ('RemarksTreatmentInformation') : RemarksTreatmentInformation
//	, ('DoctorName') : DoctorName
//	, ('TreatmentRBClass') : TreatmentRBClass
//	, ('RoomOptionAvailability') : RoomOptionAvailability
//	, ('NonMedicalItem') : NonMedicalItem
//	, ('Action') : Action
//	, ('PreviewDocument') : PreviewDocument
//	, ('Validasi') : ValidasiR])