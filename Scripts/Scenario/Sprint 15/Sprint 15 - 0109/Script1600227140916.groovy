import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

//Login//
String UserID = 'amumtazah@asuransiastra.com'

String Password = 'P@ssw0rd'

//Home
String Menu = 'Health'

String SubMenu = 'Create E-GL'

//GL Inquiry
String MemberNo = findTestData('MemberNameNonClientMA').getValue(3, 15)

//Claim
String Member = findTestData('MemberNameNonClientMA').getValue(1, 15)

String ProductType = 'Maternity (Persalinan)' //'Inpatient (Rawat Inap)' //

String GLType = 'Awal'

String IsODSODC = ''

String EditTreatmentPeriod = 'No'

String TreatmentPeriodStart = ''

String TreatmentPeriodEnd = ''

String TreatmentDuration = '3'

String AdminName = findTestData('ContactName').getValue(1, 1)

String Email = 'ehu@beyond.asuransi.astra.co.id'

String Phone = ''

String ProviderPhoneExt = '17241'

ArrayList Diagnosis = ['New']

ArrayList StatusDiagnosa = ['Initial Primary (Diagnosa Utama)']

ArrayList DiagnosisID = ['O80']

String Gravida = '1'

String Partus = '1'

String Abortus = '1'

String GestationalAge = '1'

String RemarksDiagnosa = 'Currently testing Remarks Diagnosa by Automation. Thanks. Regards - NBH'

String RemarksTreatmentInformation = 'Currently testing by Automation. Thanks. Regards - NBH'

String MaternityTreatment = 'Persalinan Normal dengan Dokter'

String DoctorName = 'NBH ' + findTestData('MemberNameNonClientMA').getValue(3, 15)

String TreatmentRBClass = 'Kelas II'

String RoomOptionAvailability = 'On Plan'

String NonMedicalItem = ''

String PackagePrice = '1000000'

String Action = 'Process'

String PreviewDocument = 'Process'

ArrayList Validasi = [GlobalVariable.ValidasiNonClientGMA]

//Claim Revisi

String GLType2 = 'Revisi'

ArrayList Diagnosis2 = ['']

ArrayList StatusDiagnosa2 = ['']

ArrayList DiagnosisID2 = ['']

String Gravida2 = '1'

String Partus2 = '1'

String Abortus2 = '1'

String GestationalAge2 = '1'

ArrayList Validasi2 = [GlobalVariable.ValidasiNonClientGMA]

WebUI.comment(Member)

WebUI.comment(Member)

WebUI.callTestCase(findTestCase('Pages/Web/GardaMedikaAkses/Login/Login'), [('UserID') : UserID, ('Password') : Password])

WebUI.callTestCase(findTestCase('Pages/Web/GardaMedikaAkses/Home/Home'), [('Menu') : Menu, ('SubMenu') : SubMenu])

WebUI.callTestCase(findTestCase('Pages/Web/GardaMedikaAkses/GL Inquiry/GL Inquiry'), [('Phase') : '1', ('MemberNo') : MemberNo])

WebUI.callTestCase(findTestCase('Pages/Web/GardaMedikaAkses/Claim/Claim'), 
	[('Phase') : '1'
	, ('Member') : Member
	, ('ProductType') : ProductType
	, ('GLType') : GLType
	, ('IsODSODC') : IsODSODC
	, ('EditTreatmentPeriod') : EditTreatmentPeriod
	, ('AdminName') : AdminName
	, ('Email') : Email
	, ('ProviderPhoneExt') : ProviderPhoneExt
	, ('Diagnosis') : Diagnosis
	, ('StatusDiagnosa') : StatusDiagnosa
	, ('DiagnosisID') : DiagnosisID
	, ('Gravida') : Gravida
	, ('Partus') : Partus
	, ('Abortus') : Partus
	, ('GestationalAge') : GestationalAge
	, ('RemarksDiagnosa') : RemarksDiagnosa
	, ('RemarksTreatmentInformation') : RemarksTreatmentInformation
	, ('MaternityTreatment') : MaternityTreatment
	, ('DoctorName') : DoctorName
	, ('TreatmentRBClass') : TreatmentRBClass //New
	, ('RoomOptionAvailability') : RoomOptionAvailability //New
	, ('NonMedicalItem') : NonMedicalItem //New
	, ('PackagePrice') : PackagePrice
	, ('Action') : Action
	, ('PreviewDocument') : PreviewDocument
	, ('Validasi') : Validasi])

//Belum bisa GL Revisi, karna GL Awal pasti kena validasi
//
//WebUI.callTestCase(findTestCase('Pages/Web/GardaMedikaAkses/Home/Home'), [('Menu') : Menu, ('SubMenu') : SubMenu])
//
//WebUI.callTestCase(findTestCase('Pages/Web/GardaMedikaAkses/GL Inquiry/GL Inquiry'), [('Phase') : '2', ('MemberNo') : MemberNo])
//
//WebUI.callTestCase(findTestCase('Pages/Web/GardaMedikaAkses/Claim/Claim'), 
//	[('Phase') : '2'
//	, ('Member') : Member
//	, ('ProductType') : ProductType
//	, ('GLType') : GLType2
//	, ('IsODSODC') : IsODSODC
//	, ('EditTreatmentPeriod') : EditTreatmentPeriod
//	, ('AdminName') : AdminName
//	, ('Email') : Email
//	, ('ProviderPhoneExt') : ProviderPhoneExt
//	, ('Diagnosis') : Diagnosis2
//	, ('StatusDiagnosa') : StatusDiagnosa2
//	, ('DiagnosisID') : DiagnosisID2
//	, ('Gravida') : Gravida2
//	, ('Partus') : Partus2
//	, ('Abortus') : Partus2
//	, ('GestationalAge') : GestationalAge2
//	, ('RemarksDiagnosa') : RemarksDiagnosa
//	, ('RemarksTreatmentInformation') : RemarksTreatmentInformation
//	, ('MaternityTreatment') : MaternityTreatment
//	, ('DoctorName') : DoctorName
//	, ('TreatmentRBClass') : TreatmentRBClass //New
//	, ('RoomOptionAvailability') : RoomOptionAvailability //New
//	, ('NonMedicalItem') : NonMedicalItem //New
//	, ('PackagePrice') : PackagePrice
//	, ('Action') : Action
//	, ('PreviewDocument') : PreviewDocument
//	, ('Validasi') : Validasi2])

WebUI.comment(Member)