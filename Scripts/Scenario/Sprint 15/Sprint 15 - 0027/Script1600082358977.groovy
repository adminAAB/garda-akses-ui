import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5 as GEN5
import com.keyword.UI as UI

//Login//
String UserID = 'LKT'

String Password = 'ITG@nt1P455QC'

//Home
String Menu = 'General'

String SubMenu = 'Create Ticket'

//Create Ticket//
String ContactLine = 'Provider'

String Product = 'Health'

String ChannelType = 'Call'

String ContactName = findTestData('ContactName').getValue(1, 1)

String ContactType = 'Farmasi'

String ServiceType = 'Claim'

String InterruptedCall = 'No'

String ProviderName = 'TJKKG0020 KLINIK GARDA MEDIKA'

String ActionCT = 'Next'

//Inquiry//
String Phase = '1'

String MemberNo = findTestData('MemberNameBankPermata').getValue(3, 1)

//Claim
String Member = 'Existing' //Member = Existing  / New / Check

String MemberName = findTestData('MemberNameBankPermata').getValue(1, 1)

String ProductType = 'Inpatient (Rawat Inap)'

String GLType = 'Awal'

String EditTreatmentPeriodStart = 'No'

String EditTreatmentPeriodEnd = 'No'

String EditTreatmentDuration = 'No'

String PatientODSODC = 'Ya' //Tidak

String IsODSODC = 'ODS' // ODC

String SpecialCondition = 'No'

String SpecialConditionReason = ''

ArrayList Diagnosis = ['New']

ArrayList StatusDiagnosa = ['Initial Primary (Diagnosa Utama)']

ArrayList DiagnosisID = ['A36']

String Gravida = '1'

String Partus = '0'

String Abortus = '0'

String GestationalAge = '34'

String RemarksDiagnosa = 'Testing Diagnosis - Katalon'

String RemarksTreatmentInformation = ContactName + ' GL ' + GLType + ' ' + MemberNo + ' By OAS'

String DiagnosisQuestion = 'Kepala bayi masih diatas PAP pada BSC 1x'

String RemarksDiagnosis = 'Testing Additional Info - Katalon'

String MaternityTreatment = 'Persalinan Caesar'

String DoctorName = 'Automation Doctor - Me'

String Rujuk = 'No'

String Reason = ''

ArrayList MTTindakanMedis = ['']

ArrayList MTDiagnosis = ['DIPTHERIA']

ArrayList MedicalTreatment = ['']

String UnregisteredMT = 'tindakan tambahan OAS'

String Billed = '15000000'

//Family planning Item

String AddFP = 'No'

ArrayList FamilyPlanningItem = ['New']

ArrayList FamilyPlanning = ['KONDOM / DIAFRAGMA / CUP SERVIKS']

String FPBilled = '300000'

String AppropriateRBClass = 'ODS (One Day Surgery)'

String AppropriateRBRate = '500000'

String TreatmentRBClass = 'ODS (One Day Surgery)'

String TreatmentRBRate = '500000'

String RoomOptionAvailability = 'On Plan'

String PackagePrice = '1000000'

String TotalBilled = '1000000'

String NeedFollowUp = 'Tidak'

String NonMedicalItem = ''

//Document
String NewDocument = 'No'

ArrayList UploadDocument = ['New','New']

ArrayList InputDocType = ['Billing Perawatan Asli', 'Billing Perbandingan']

ArrayList DocumentType = ['Billing Perawatan Asli', 'Billing Perbandingan']

String DocSource = 'WhatsApp'

String DocValidity = 'Dokumen Valid'

String FileLocation = 'D:\\'

String FileName = 'foto ktp'

String EditDocument = 'No'

String DeleteDocument = 'No'

String ActionGL = 'Process'

String PreviewDocument = 'Process'

ArrayList Validasi = [GlobalVariable.ValidasiDijaminkan]

//Exit Confirmation
String ECAction1 = 'Tidak'

String ECAction2 = 'Puas'

String Comment = 'Currently testing by Automation. Thanks. Regards - Me'

WebUI.comment(MemberName)

//Script//
WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Login/Login'), [('UserID') : UserID, ('Password') : Password])

//==================== PHASE 1 ====================
WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Home/Home'), [('Menu') : Menu, ('SubMenu') : SubMenu])

WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Create Ticket/Create Ticket'), [('ContactLine') : ContactLine, ('Product') : Product
        , ('ChannelType') : ChannelType, ('ContactName') : ContactName, ('ContactType') : ContactType, ('ServiceType') : ServiceType
        , ('InterruptedCall') : InterruptedCall, ('ProviderName') : ProviderName, ('Action') : ActionCT])

CustomKeywords.'querySQL.Query.QueryContactName'()

WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim Inquiry'), [('Phase') : '1'])

WebUI.comment(MemberName)

WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim'), 
	[('Member') : Member
	  	, ('MemberName') : MemberName
        , ('ProductType') : ProductType
		, ('GLType') : GLType
		, ('SpecialCondition') : SpecialCondition
		, ('EditTreatmentPeriodStart') : EditTreatmentPeriodStart
        , ('EditTreatmentPeriodEnd') : EditTreatmentPeriodEnd
		, ('EditTreatmentDuration') : EditTreatmentDuration
		, ('IsODSODC') : IsODSODC
		, ('Diagnosis') : Diagnosis
		, ('StatusDiagnosa') : StatusDiagnosa
        , ('DiagnosisID') : DiagnosisID
		, ('Gravida') : Gravida
		, ('Partus') : Partus
		, ('Abortus') : Abortus
		, ('GestationalAge') : GestationalAge
        , ('RemarksDiagnosa') : RemarksDiagnosa
		, ('DiagnosisQuestion') : DiagnosisQuestion
		, ('RemarksDiagnosis') : RemarksDiagnosis
		, ('RemarksTreatmentInformation') : RemarksTreatmentInformation
        , ('MaternityTreatment') : MaternityTreatment
		, ('DoctorName') : DoctorName
		, ('Rujuk') : Rujuk
		, ('MedicalTreatment') : MedicalTreatment
		, ('Reason') : Reason
		, ('SpecialCondition') : SpecialCondition
		, ('MTTindakanMedis') : MTTindakanMedis
		, ('MTDiagnosis') : MTDiagnosis
		, ('UnregisteredMT') : UnregisteredMT
		, ('Billed') : Billed
		, ('AddFP'): AddFP, ('FamilyPlanningItem') : FamilyPlanningItem,('FamilyPlanning') : FamilyPlanning , ('FPBilled') : FPBilled
		, ('AppropriateRBClass') : AppropriateRBClass
        , ('AppropriateRBRate') : AppropriateRBRate
		, ('TreatmentRBClass') : TreatmentRBClass
		, ('TreatmentRBRate') : TreatmentRBRate
        , ('SpecialConditionReason') : SpecialConditionReason
		, ('RoomOptionAvailability') : RoomOptionAvailability
		, ('PackagePrice') : PackagePrice
		, ('TotalBilled'):TotalBilled
		, ('NeedFollowUp') : NeedFollowUp
		, ('NonMedicalItem') : NonMedicalItem
        , ('NewDocument') : NewDocument, ('UploadDocument') : UploadDocument, ('InputDocType'): InputDocType, ('DocumentType') : DocumentType, ('DocSource') : DocSource
        , ('DocValidity') : DocValidity, ('FileLocation') : FileLocation, ('FileName'): FileLocation
		, ('EditDocument') : EditDocument
		, ('DeleteDocument') : DeleteDocument
		, ('ActionGL') : ActionGL
        , ('Validasi') : Validasi
		, ('PreviewDocument') : PreviewDocument
		, ('Phase') : '1'])

WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Exit Confirmation/Exit Confirmation'), [('ECAction1') : ECAction1
        , ('ECAction2') : ECAction2, ('Comment') : Comment])

WebUI.comment(GlobalVariable.TicketID1)