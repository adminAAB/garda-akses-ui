import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5 as GEN5
import com.keyword.UI as UI

//Login//
String UserID = 'LKT'

String Password = 'ITG@nt1P455QC'

//Home
String Menu = 'General'

String SubMenu = 'Create Ticket'

//Create Ticket//
String ContactLine = 'Provider'

String Product = 'Health'

String ChannelType = 'Call'

String ContactName = findTestData('ContactName').getValue(1, 1)

String ContactType = 'Lainnya'

String ServiceType = 'Claim'

String InterruptedCall = 'No'

String ProviderName = 'OJKSH00001 SILOAM HOSPITALS KEBON JERUK'

String ActionCT = 'Next'

//Inquiry//
String Phase = '1'

//Claim
String Member = 'Existing' //Member = Existing  / New / Check

String MemberName = findTestData('MemberNameNonClient').getValue(1, 1)

String ProductType = 'Inpatient (Rawat Inap)' //'Maternity (Persalinan)'

String GLType = 'Akhir'

String EditTreatmentPeriodStart = 'No'

String TreatmentPeriodStart = '27/Feb/2020'

String EditTreatmentPeriodEnd = 'No'

String TreatmentDuration = '3'

String IsODSODC = ''

String SpecialCondition = 'No'

String SpecialConditionReason = ''

ArrayList Diagnosis = ['New']

ArrayList StatusDiagnosa = ['Initial Primary (Diagnosa Utama)']

ArrayList DiagnosisID = ['A36']

String RemarksTreatmentInformation = 'OAS Automation'

String RemarksDiagnosa = 'Currently testing Remarks Diagnosa by Automation. Thanks. Regards - Me'

String DoctorName = 'OAS ' + findTestData('MemberNameNonClient').getValue(3, 1)

String Rujuk = 'No'

String Reason = ''

//Medical Treatment

ArrayList MTTindakanMedis = ['']

ArrayList MTDiagnosis = ['SINGLE SPONTANEOUS DELIVERY/NORMAL DELIVERY']

ArrayList MedicalTreatment = ['']

String UnregisteredMT = 'tindakan tambahan caesar'

String Billed = '15000000'

//Family Planning Item

String AddFP = 'No'

ArrayList FamilyPlanningItem = ['New']

ArrayList FamilyPlanning = ['KONDOM / DIAFRAGMA / CUP SERVIKS']

String FPBilled = '300000'

String TotalBilled = '1000000'

String AppropriateRBClass = 'STANDARD'

String TreatmentRBClass = 'STANDARD'

String RoomOptionAvailability = 'On Plan'

String NeedFollowUp = 'No'

String NonMedicalItem = ''

String NewDocument = 'No'

ArrayList UploadDocument = ['New','New']

ArrayList InputDocType = ['Billing Perawatan Asli', 'Billing Perbandingan']

ArrayList DocumentType = ['Billing Perawatan Asli', 'Billing Perbandingan']

String DocSource = 'WhatsApp'

String DocValidity = 'Dokumen Valid'

String FileLocation = 'D:\\'

String FileName = 'foto ktp'

String EditDocument = 'No'

String DeleteDocument = 'No'

String ActionGL = 'Process'

String PreviewDocument = 'Process'

String NeedFU = 'Tidak'

ArrayList Validasi = [GlobalVariable.ValidasiClientNonEGL]

//Exit Confirmation
String ECAction1 = 'Tidak'

String ECAction2 = 'Puas'

String Comment = 'Currently testing by Automation. Thanks. Regards - Me'

//Script//
WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Login/Login'), [('UserID') : UserID, ('Password') : Password])

//==================== PHASE 1 ====================
WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Home/Home'), [('Menu') : Menu, ('SubMenu') : SubMenu])

WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Create Ticket/Create Ticket'), [('ContactLine') : ContactLine, ('Product') : Product
, ('ChannelType') : ChannelType, ('ContactName') : ContactName, ('ContactType') : ContactType, ('ServiceType') : ServiceType
, ('InterruptedCall') : InterruptedCall, ('ProviderName') : ProviderName, ('Action') : ActionCT])

WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim Inquiry'), [('Phase') : '1'])

WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim'), [('Member') : Member, ('MemberName') : MemberName
        , ('ProductType') : ProductType, ('GLType') : GLType
		, ('IsODSODC') : IsODSODC
		, ('SpecialCondition') : SpecialCondition, ('EditTreatmentPeriodStart') : EditTreatmentPeriodStart, ('TreatmentPeriodStart') : TreatmentPeriodStart
        , ('EditTreatmentPeriodEnd') : EditTreatmentPeriodEnd, ('TreatmentDuration') : TreatmentDuration, ('Diagnosis') : Diagnosis, ('StatusDiagnosa') : StatusDiagnosa
        , ('DiagnosisID') : DiagnosisID, ('RemarksDiagnosa') : RemarksDiagnosa, ('RemarksTreatmentInformation') : RemarksTreatmentInformation, ('DoctorName') : DoctorName, 
        , ('Rujuk') : Rujuk
		, ('MTTindakanMedis') : MTTindakanMedis, ('MTDiagnosis') : MTDiagnosis, ('MedicalTreatment') : MedicalTreatment, ('UnregisteredMT') : UnregisteredMT, ('Billed') : Billed
		, ('AddFP') : AddFP, ('FamilyPlanningItem') : FamilyPlanningItem, ('FamilyPlanning') : FamilyPlanning, ('FPBilled') : FPBilled
		, ('Reason') : Reason, ('SpecialCondition') : SpecialCondition, ('SpecialConditionReason') : SpecialConditionReason
        , ('AppropriateRBClass') : AppropriateRBClass, ('TreatmentRBClass') : TreatmentRBClass, ('RoomOptionAvailability') : RoomOptionAvailability
		, ('NeedFollowUp') : NeedFollowUp
		, ('NonMedicalItem') : NonMedicalItem, ('TotalBilled') : TotalBilled
		, ('NewDocument') : NewDocument, ('UploadDocument') : UploadDocument, ('InputDocType'): InputDocType, ('DocumentType') : DocumentType, ('DocSource') : DocSource
		, ('DocValidity') : DocValidity, ('FileLocation') : FileLocation, ('FileName'): FileLocation
		, ('EditDocument') : EditDocument, ('DeleteDocument') : DeleteDocument
        , ('ActionGL') : ActionGL, ('NeedFU') : NeedFU, ('PreviewDocument') : PreviewDocument, ('Validasi') : Validasi, ('Phase') : '1'])

WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Exit Confirmation/Exit Confirmation'), [('ECAction1') : ECAction1
        , ('ECAction2') : ECAction2, ('Comment') : Comment])

WebUI.comment(MemberName)

WebUI.comment(GlobalVariable.TicketID1)

CustomKeywords.'querySQL.Query.QueryContactName'()