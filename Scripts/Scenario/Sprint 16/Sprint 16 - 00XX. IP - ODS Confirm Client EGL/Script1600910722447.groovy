import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

//Login//
String UserID = 'amumtazah@asuransiastra.com'

String Password = 'P@ssw0rd'

//Home
String Menu = 'Health'

String SubMenu = 'Create E-GL'

//GL Inquiry
String Phase = '1'

//Claim
//String Member = findTestData('MemberNameNonClientEMP').getValue(1, 1)

String Member = '18 - A/00191492 - AMRUBI TSABIT - YAYASAN AMALIAH ASTRA'

String ProductType = 'Inpatient (Rawat Inap)' //'Maternity (Persalinan)'

String GLType = 'Awal'

String EditTreatmentPeriod = 'No'

String TreatmentPeriodStart = ''

String TreatmentPeriodEnd = ''

String TreatmentDuration = '3'

String IsODSODC = 'ODS' //ODS atau ODC

String AdminName = findTestData('ContactName').getValue(1, 1)

String Email = 'ehu@beyond.asuransi.astra.co.id'

String Phone = ''

String ProviderPhoneExt = '17241'

ArrayList Diagnosis = ['New']

ArrayList StatusDiagnosa = ['Initial Primary (Diagnosa Utama)']

ArrayList DiagnosisID = ['R11']

String RemarksDiagnosa = 'Currently testing Remarks Diagnosa by Automation. Thanks. Regards - OAS'

String RemarksTreatmentInformation = 'Currently testing Remarks Diagnosa by Automation. Thanks. Regards - OAS'

String DoctorName = 'OAS A/00191492'

//Family Planning Item

String AddFP = 'Yes'

ArrayList FamilyPlanningItem = ['New']

ArrayList FamilyPlanning = ['KONDOM / DIAFRAGMA / CUP SERVIKS']

//ArrayList FamilyPlanning = ['KB IMPLAN / SUSUK KB']

String FPBilled = '300000'

String Action = 'Process'

String PreviewDocument = ''

ArrayList Validasi = [GlobalVariable.ValidasiODSConfirm]

WebUI.callTestCase(findTestCase('Pages/Web/GardaMedikaAkses/Login/Login'), [('UserID') : UserID, ('Password') : Password])

WebUI.callTestCase(findTestCase('Pages/Web/GardaMedikaAkses/Home/Home'), [('Menu') : Menu, ('SubMenu') : SubMenu])

WebUI.callTestCase(findTestCase('Pages/Web/GardaMedikaAkses/GL Inquiry/GL Inquiry'), [('Phase') : Phase])

WebUI.callTestCase(findTestCase('Pages/Web/GardaMedikaAkses/Claim/Claim'), [
	('Phase') : Phase
	, ('Member') : Member
	, ('ProductType') : ProductType
	, ('GLType') : GLType
	, ('EditTreatmentPeriod') : EditTreatmentPeriod
	, ('IsODSODC') : IsODSODC
	, ('AdminName') : AdminName
	, ('Email') : Email
	, ('ProviderPhoneExt') : ProviderPhoneExt
	, ('Diagnosis') : Diagnosis
	, ('StatusDiagnosa') : StatusDiagnosa
	, ('DiagnosisID') : DiagnosisID
	, ('RemarksDiagnosa') : RemarksDiagnosa
	, ('RemarksTreatmentInformation') : RemarksTreatmentInformation
	, ('DoctorName') : DoctorName
	, ('AddFP') : AddFP
	, ('FamilyPlanningItem') : FamilyPlanningItem
	, ('FamilyPlanning') : FamilyPlanning
	, ('FPBilled') : FPBilled
	, ('Action') : Action
	, ('PreviewDocument') : PreviewDocument
	, ('Validasi') : Validasi])

//CustomKeywords.'querySQL.Query.QueryContactName'()
CustomKeywords.'querySQL.Query.QueryFinishFollowUp'()

