import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5 as GEN5
import com.keyword.UI as UI

CustomKeywords.'querySQL.Query.QueryFinishFollowUp'()

//Login//
String UserID = GlobalVariable.Username

String Password = GlobalVariable.Password

//Home
String Menu = 'General'

String SubMenu = 'Create Ticket'

String SubMenuFU = 'Follow Up'

//Create Ticket//
String ContactLine = 'Provider'

String Product = 'Health'

String ChannelType = 'Call'

String ContactName = findTestData('ContactName').getValue(1, 1)

String ContactType = 'Lainnya'

String ServiceType = 'Claim'

String InterruptedCall = 'No'

String ProviderName = 'OJKSH00001 SILOAM HOSPITALS KEBON JERUK'

String ActionCT = 'Next'

//Inquiry//
String Phase = '1'

//Claim
String Member = 'Existing' //Member = Existing  / New / Check

String MemberName = findTestData('MemberNameNonClientMA').getValue(1, 1)

String ProductType = 'Inpatient (Rawat Inap)'

String GLType = 'Awal'

String IsODSODC = ''

String EditTreatmentPeriodStart = 'No'

String TreatmentPeriodStart = '27/Feb/2020'

String EditTreatmentPeriodEnd = 'No'

String SpecialCondition = 'No'

String SpecialConditionReason = ''

ArrayList Diagnosis = ['New']

ArrayList StatusDiagnosa = ['Initial Primary (Diagnosa Utama)']

ArrayList DiagnosisID = ['A09']

String RemarksDiagnosa = 'Currently testing Remarks Diagnosa by Automation. Thanks. Regards - Me'

String DoctorName = 'NBH ' + findTestData('MemberNameNonClient').getValue(3, 1)

String Rujuk = 'No'

String MedicalTreatment = ''

String Reason = ''

String AppropriateRBClass = 'STANDARD'

String TreatmentRBClass = 'STANDARD'

String RoomOptionAvailability = 'On Plan'

String NeedFollowUp = ''

String NonMedicalItem = ''

String NewDocument = 'No'

String EditDocument = 'No'

String DeleteDocument = 'No'

String ActionGL = 'Process'

String PreviewDocument = 'Process'

String NeedFU = 'Ya'

ArrayList Validasi = [GlobalVariable.ValidasiClientNonEGL]

//Exit Confirmation
String ECAction1 = 'Tidak'

String ECAction2 = 'Puas'

String Comment = 'Currently testing by Automation. Thanks. Regards - Me'

//Inq Follow Up
//Follow Up
String ContactNameFU = (ContactName + ' - ') + ContactType

String ClientNameFU = findTestData('MemberNameBankPermata').getValue(5, 1)

String MemberNameFU = findTestData('MemberNameBankPermata').getValue(4, 1)

String ProviderNameFU = 'OJKSH00001 - SILOAM HOSPITALS KEBON JERUK'

ArrayList DiagnosisConfirmation = ['New']

ArrayList PIC = ['Head Contact Center']

String HeadCCO = 'Regina Dita Permanawati'

ArrayList Confirmation = ['Covered']

String Channel = 'Call'

String Remarks = 'Yes'

String RemarksValue = ''

String BenefitCoverage = 'IP - Cashless'

String RemarksFUDiagnosis = 'Currently testing by Automation. Thanks. Regards - NBH'

String ActionFUDiagnosis = 'Save'

String ActionPreviewDocumentFU = 'Process'

String FinalConfirmationFU = 'Tidak'

ArrayList ValidasiFU = [GlobalVariable.ValidasiDijaminkan]

WebUI.comment(MemberName)

//Script//
WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Login/Login'), [('UserID') : UserID, ('Password') : Password])

//==================== PHASE 1 ====================
WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Home/Home'), [('Menu') : Menu, ('SubMenu') : SubMenu])

WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Create Ticket/Create Ticket'), [('ContactLine') : ContactLine, ('Product') : Product
        , ('ChannelType') : ChannelType, ('ContactName') : ContactName, ('ContactType') : ContactType, ('ServiceType') : ServiceType
        , ('InterruptedCall') : InterruptedCall, ('ProviderName') : ProviderName, ('Action') : ActionCT])

WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim Inquiry'), [('Phase') : '1'])

WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim'), [('Member') : Member, ('MemberName') : MemberName
        , ('ProductType') : ProductType, ('GLType') : GLType, ('IsODSODC') : IsODSODC, ('SpecialCondition') : SpecialCondition
        , ('EditTreatmentPeriodStart') : EditTreatmentPeriodStart, ('EditTreatmentPeriodEnd') : EditTreatmentPeriodEnd, ('Diagnosis') : Diagnosis
        , ('StatusDiagnosa') : StatusDiagnosa, ('DiagnosisID') : DiagnosisID, ('RemarksDiagnosa') : RemarksDiagnosa, ('DoctorName') : DoctorName
        , ('Rujuk') : Rujuk, ('MedicalTreatment') : MedicalTreatment, ('Reason') : Reason, ('SpecialCondition') : SpecialCondition
        , ('SpecialConditionReason') : SpecialConditionReason, ('AppropriateRBClass') : AppropriateRBClass, ('TreatmentRBClass') : TreatmentRBClass
        , ('RoomOptionAvailability') : RoomOptionAvailability, ('NeedFollowUp') : NeedFollowUp, ('NonMedicalItem') : NonMedicalItem
        , ('NewDocument') : NewDocument, ('EditDocument') : EditDocument, ('DeleteDocument') : DeleteDocument, ('ActionGL') : ActionGL
        , ('NeedFU') : NeedFU, ('PreviewDocument') : PreviewDocument, ('Validasi') : Validasi, ('Phase') : '1'])

WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Exit Confirmation/Exit Confirmation'), [('ECAction1') : ECAction1
        , ('ECAction2') : ECAction2, ('Comment') : Comment])

WebUI.comment(GlobalVariable.TicketID1)

CustomKeywords.'querySQL.Query.QueryContactName'()

WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Home/Home'), [('Menu') : Menu, ('SubMenu') : SubMenuFU])

WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Follow Up/Follow Up - Inquiry'), [:])

WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Follow Up/Follow Up - Provider Health Claim'), [('PhaseFU') : '1' //menentukan dia fu ke berapa
        , ('PhaseGL') : '1' // menentukan dia dari ticket id ke berapa
        , ('UserID') : UserID, ('Password') : Password, ('Menu') : Menu, ('SubMenuFU') : SubMenuFU, ('FUContactName') : ContactNameFU
        , ('FUClientName') : ClientNameFU, ('FUMemberName') : MemberNameFU, ('MemberName') : MemberName, ('ProductType') : ProductType
        , ('GLType') : GLType, ('Provider') : ProviderNameFU, ('Validasi') : Validasi, ('PIC') : PIC, ('DiagnosisConfirmation') : DiagnosisConfirmation
        , ('HeadCCO') : HeadCCO, ('Confirmation') : Confirmation, ('Channel') : Channel, ('Remarks') : Remarks, ('RemarksValue') : RemarksValue
        , ('BenefitCoverage') : BenefitCoverage, ('RemarksFUDiagnosis') : RemarksFUDiagnosis, ('ActionFUDiagnosis') : ActionFUDiagnosis
        , ('ActionPreviewDocumentFU') : ActionPreviewDocumentFU, ('FinalConfirmationFU') : FinalConfirmationFU, ('ValidasiFU') : ValidasiFU])

WebUI.comment(MemberName)

WebUI.comment(GlobalVariable.TicketID1)

WebUI.comment(GlobalVariable.TrID1)