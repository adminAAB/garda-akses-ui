import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

//Login//
String UserID = 'amumtazah@asuransiastra.com'

String Password = 'P@ssw0rd'

//Home
String Menu = 'Health'

String SubMenu = 'Create E-GL'

//GL Inquiry
String Phase = '1'

//Claim
String Member = 'J/00022456'//findTestData('MemberNameClientSahabatFinansialKeluarga').getValue(1, 1)

String ProductType =  'Maternity (Persalinan)' //'Inpatient (Rawat Inap)'

String GLType = 'Awal'

String IsODSODC = ''

String EditTreatmentPeriod = 'No'

String TreatmentPeriodStart = ''

String TreatmentPeriodEnd = ''

String TreatmentDuration = '3'

String AdminName = findTestData('ContactName').getValue(1, 1)

String Email = 'ehu@beyond.asuransi.astra.co.id'

String Phone = ''

String ProviderPhoneExt = '17241'

ArrayList Diagnosis = ['New']

ArrayList StatusDiagnosa = ['Initial Primary (Diagnosa Utama)']
String StatusDiagnosaSecondary = 'Initial Secondary (Diagnosa Tambahan)'

ArrayList DiagnosisID = ['O80']
String DiagnosisSecondaryID = ''

String Gravida = '0'
String Partus = '0'
String Abortus = '0'
String GestationalAge = '0'
String MaternityTreatment = 'Persalinan Normal dengan Dokter'
String RemarksDiagnosa = 'Currently testing Remarks Diagnosa by Automation. Thanks. Regards - TSG'

String RemarksTreatmentInformation = 'Currently testing Remarks Diagnosa by Automation. Thanks. Regards - TSG'

String DoctorName = 'Doctor Maste' //+ findTestData('MemberNameClientSahabatFinansialKeluarga').getValue(3, 1)

String MedicalTreatment = ''

String MedicalTreatment = ''

String MedicalTreatment = ''

String RoomOption = 'APS'
String PackagePrice = '100000'

String Action = 'Process'

String PreviewDocument = 'Process'

ArrayList Validasi = [GlobalVariable.ValidasiDocument]

WebUI.callTestCase(findTestCase('Pages/Web/GardaMedikaAkses/Login/Login'), [('UserID') : UserID, ('Password') : Password])

WebUI.callTestCase(findTestCase('Pages/Web/GardaMedikaAkses/Home/Home'), [('Menu') : Menu, ('SubMenu') : SubMenu])

WebUI.callTestCase(findTestCase('Pages/Web/GardaMedikaAkses/GL Inquiry/GL Inquiry'), [('Phase') : Phase])

WebUI.callTestCase(findTestCase('Pages/Web/GardaMedikaAkses/Claim/Claim'), [
	('Phase') : Phase
	, ('Member') : Member
	, ('ProductType') : ProductType
	, ('GLType') : GLType
	, ('IsODSODC') : IsODSODC
	, ('EditTreatmentPeriod') : EditTreatmentPeriod
	, ('AdminName') : AdminName
	, ('Email') : Email
	, ('ProviderPhoneExt') : ProviderPhoneExt
	, ('Diagnosis') : Diagnosis
	, ('StatusDiagnosa') : StatusDiagnosa
	, ('DiagnosisID') : DiagnosisID
	, ('Gravida') : Gravida
	, ('Partus') : Partus
	, ('Abortus') : Abortus
	, ('GestationalAge') : GestationalAge
	, ('RemarksDiagnosa') : RemarksDiagnosa
	, ('StatusDiagnosaSecondary') : StatusDiagnosaSecondary
	, ('DiagnosisSecondaryID') : DiagnosisSecondaryID
	, ('MaternityTreatment') : MaternityTreatment
	, ('RemarksTreatmentInformation') : RemarksTreatmentInformation
	, ('DoctorName') : DoctorName
	, ('MedicalTreatment') : MedicalTreatment
	, ('RoomOption') : RoomOption
	, ('PackagePrice') : PackagePrice
	, ('Action') : Action
	, ('PreviewDocument') : PreviewDocument
	, ('Validasi') : Validasi])

//CustomKeywords.'querySQL.Query.QueryContactName'()
CustomKeywords.'querySQL.Query.QueryFinishFollowUp'()

