import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

//Login//
String UserID = 'amumtazah@asuransiastra.com'

String Password = 'P@ssw0rd'

//Home
String Menu = 'Health'

String SubMenu = 'Create E-GL'

//GL Inquiry
String Phase = '1'

//Claim
String Member = 'A/00109208'

String ProductType = 'Maternity (Persalinan)' //'Inpatient (Rawat Inap)' //

String NoProduct = 'Continue'

String GLType = 'Akhir'

String IsODSODC = ''

String EditTreatmentPeriod = 'No'

String TreatmentPeriodStart = ''

String TreatmentPeriodEnd = ''

String TreatmentDuration = '3'

String AdminName = findTestData('ContactName').getValue(1, 1)

String Email = 'ehu@beyond.asuransi.astra.co.id'

String Phone = ''

String ProviderPhoneExt = '17241'

ArrayList Diagnosis = ['New']

ArrayList StatusDiagnosa = ['Initial Primary (Diagnosa Utama)']

ArrayList DiagnosisID = ['O80']

String Gravida = '5'

String Partus = '4'

String Abortus = '0'

String GestationalAge = '20'

String RemarksDiagnosa = 'Currently testing GMA - MA - GL Akhir - Validasi Coverage Client Non E-GL (Eli = Cov, Cov = Uncov) by Automation. Thanks. Regards - RRU'

String RemarksTreatmentInformation = 'Currently testing GMA - MA - GL Akhir - Validasi Coverage Client Non E-GL (Eli = Cov, Cov = Uncov) by Automation. Thanks. Regards - RRU'

String MaternityTreatment = 'Persalinan Normal dengan Dokter'

String DoctorName = 'Automation Doctor - RRU'

String TreatmentRBClass = 'Kelas I'

String RoomOptionAvailability = 'On Plan'

String PackagePrice = '100000'

String TotalBilled = '100000'

String NonMedicalItem = ''

String Action = 'Process'

String PreviewDocument = ''

ArrayList Validasi = [GlobalVariable.ValidasiGMAClientNonEGL]

WebUI.callTestCase(findTestCase('Pages/Web/GardaMedikaAkses/Login/Login'), [('UserID') : UserID, ('Password') : Password])

WebUI.callTestCase(findTestCase('Pages/Web/GardaMedikaAkses/Home/Home'), [('Menu') : Menu, ('SubMenu') : SubMenu])

WebUI.callTestCase(findTestCase('Pages/Web/GardaMedikaAkses/GL Inquiry/GL Inquiry'), [('Phase') : Phase])

WebUI.callTestCase(findTestCase('Pages/Web/GardaMedikaAkses/Claim/Claim'), [
	('Phase') : Phase
	, ('Member') : Member
	, ('ProductType') : ProductType
	, ('NoProduct') : NoProduct
	, ('GLType') : GLType
	, ('IsODSODC') : IsODSODC
	, ('EditTreatmentPeriod') : EditTreatmentPeriod
	, ('TreatmentDuration') : TreatmentDuration
	, ('AdminName') : AdminName
	, ('Email') : Email
	, ('ProviderPhoneExt') : ProviderPhoneExt
	, ('Diagnosis') : Diagnosis
	, ('DiagnosisID') : DiagnosisID
	, ('Gravida') : Gravida
	, ('Partus') : Partus
	, ('Abortus') : Abortus
	, ('GestationalAge') : GestationalAge
	, ('RemarksDiagnosa') : RemarksDiagnosa
	, ('StatusDiagnosa') : StatusDiagnosa
	, ('RemarksTreatmentInformation') : RemarksTreatmentInformation
	, ('MaternityTreatment') : MaternityTreatment
	, ('DoctorName') : DoctorName
	, ('TreatmentRBClass') : TreatmentRBClass
	, ('RoomOptionAvailability') : RoomOptionAvailability
	, ('PackagePrice') : PackagePrice
	, ('TotalBilled') : TotalBilled
	, ('NonMedicalItem') : NonMedicalItem
	, ('Action') : Action
	, ('PreviewDocument') : PreviewDocument
	, ('Validasi') : Validasi])

CustomKeywords.'querySQL.Query.QueryFinishFollowUp'()

//CustomKeywords.'querySQL.Query.QueryContactName'()