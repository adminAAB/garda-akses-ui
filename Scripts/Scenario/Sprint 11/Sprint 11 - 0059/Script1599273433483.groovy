import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSfindWindowsObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5 as GEN5

//Login//
String UserID = 'LKT'

String Password = 'ITG@nt1P455QC'

//Home
String Menu = 'General'

String SubMenu = 'Create Ticket'

//Create Ticket//
String ContactLine = 'Provider'

String Product = 'Health'

String ChannelType = 'Call'

String ContactName = findTestData('ContactName').getValue(1, 1)

String ContactType = 'Farmasi'

String ServiceType = 'Claim'

String InterruptedCall = 'No'

String ProviderName = 'OJKSH00001 SILOAM HOSPITALS KEBON JERUK'

String ActionCT = 'Next'

//Inquiry//
String Phase = '1'

//Claim
String Member = 'Existing' //Member = Existing  / New / Check

String MemberName = findTestData('MemberNameFederalInternationalFinanceMA').getValue(1, 50)

//String MemberName = '40272 - C/00012704 - CHAIRUN NISA - PT. ASTRA HONDA MOTOR - GOL. 1-3'

String ProductType = 'Maternity (Persalinan)'

String GLType = 'Awal'

String GLType2 = 'Revisi'

String EditTreatmentPeriodStart = 'No'

String EditTreatmentPeriodEnd = 'No'

String IsODSODC = ''

String SpecialCondition = 'No'

String SpecialConditionReason = ''

ArrayList Diagnosis = ['New']

ArrayList StatusDiagnosa = ['Initial Primary (Diagnosa Utama)']

ArrayList DiagnosisID = ['O80']

String Gravida = '1'

String Partus = '0'

String Abortus = '0'

String GestationalAge = '34'

String RemarksTreatmentInformation ='OAS - Automation'

String RemarksDiagnosa = 'Ini Diagnosis Primary - Katalon'

String RemarksDiagnosa2 = 'Ini Diagnosis Secondary - Katalon'

ArrayList Diagnosis2 = ['New']

ArrayList StatusDiagnosa2 = ['Initial Secondary (Diagnosa Tambahan)']

ArrayList DiagnosisID2 = ['B07']

String DiagnosisQuestion = 'Kepala bayi masih diatas PAP pada BSC 1x'

String RemarksDiagnosis = 'Testing Additional Info - Katalon'

String MaternityTreatment = 'Persalinan Normal dengan Dokter'

String DoctorName = 'OAS ' + findTestData('MemberNameFederalInternationalFinanceMA').getValue(3, 50)

String Rujuk = 'No'

String Reason = ''

ArrayList MTTindakanMedis = ['New']

ArrayList MTDiagnosis = ['SINGLE SPONTANEOUS DELIVERY/NORMAL DELIVERY']

ArrayList MedicalTreatment = ['']

String UnregisteredMT = 'tindakan tambahan caesar'

String Billed = '15000000'

ArrayList MTTindakanMedis2 = ['New']

ArrayList MTDiagnosis2 = ['VIRAL WARTS']

ArrayList MedicalTreatment2 = ['Cryotherapy / Cryosurgery']

//Family planning Item

String AddFP = 'No'

ArrayList FamilyPlanningItem = ['New']

ArrayList FamilyPlanning = ['KONDOM / DIAFRAGMA / CUP SERVIKS']

String FPBilled = '300000'

String AppropriateRBClass = 'BASIC'

String TreatmentRBClass = 'BASIC'

String RoomOptionAvailability = 'On Plan'

String PackagePrice = '100000'

String TotalBilled = '1000000'

String NeedFollowUp = ''

String NewDocument = 'No'

ArrayList UploadDocument = ['New','New']

ArrayList InputDocType = ['Billing Perawatan Asli', 'Billing Perbandingan']

ArrayList DocumentType = ['Billing Perawatan Asli', 'Billing Perbandingan']

String DocSource = 'WhatsApp'

String DocValidity = 'Dokumen Valid'

String FileLocation = 'D:\\'

String FileName = 'foto ktp'

String EditDocument = 'No'

String DeleteDocument = 'No'

String NonMedicalItem = ''

String PreviewDocument = 'Process'

String PreviewDocument2 = ''

String ActionGL = 'Process'

ArrayList Validasi = [GlobalVariable.ValidasiDijaminkan]

ArrayList Validasi2 = [GlobalVariable.ValidasiMedicalTreatment]

//Exit Confirmation
String ECAction1 = 'Tidak'

String ECAction2 = 'Puas'

String Comment = 'Currently testing by Automation. Thanks. Regards - Me'

////Login
//String UserIDFU = 'LKT'
//
//String PasswordFU = 'P@ssw0rd'
//
////Home
//String MenuFU = 'General'
//
//String SubMenuFU = 'Follow Up'
//
////Follow Up
//String FUContactName = 'Siloam Hospitals Kebon Jeruk'
//
//String FUClientName = findTestData('MemberNameLineX').getValue(5, 1)
//
//String FUMemberName = findTestData('MemberNameLineX').getValue(4, 1)
//
//String DiagnosisConfirmation = 'New'
//
//String PIC = 'Doctor'
//
//String EditDateTimeConfirmation = 'No'
//
//String DTC = null
//
//String Confirmation = ''

//Script//
WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Login/Login'), [('UserID') : UserID, ('Password') : Password])

//==================== PHASE 1 ====================
WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Home/Home'), [('Menu') : Menu, ('SubMenu') : SubMenu])

WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Create Ticket/Create Ticket'), [('ContactLine') : ContactLine, ('Product') : Product
        , ('ChannelType') : ChannelType, ('ContactName') : ContactName, ('ContactType') : ContactType, ('ServiceType') : ServiceType
        , ('InterruptedCall') : InterruptedCall, ('ProviderName') : ProviderName, ('Action') : ActionCT])

CustomKeywords.'querySQL.Query.QueryContactName'()

WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim Inquiry'), [('Phase') : '1'])

WebUI.comment(MemberName)

WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim'), [
		  ('Member') : Member
		, ('MemberName') : MemberName
        , ('ProductType') : ProductType
		, ('GLType') : GLType
		, ('SpecialCondition') : SpecialCondition
		, ('EditTreatmentPeriodStart') : EditTreatmentPeriodStart
        , ('EditTreatmentPeriodEnd') : EditTreatmentPeriodEnd
		, ('IsODSODC') : IsODSODC
		, ('Diagnosis') : Diagnosis
		, ('StatusDiagnosa') : StatusDiagnosa
		, ('DiagnosisID') : DiagnosisID
		, ('Gravida') : Gravida
		, ('Partus') : Partus
		, ('Abortus') : Abortus
		, ('GestationalAge') : GestationalAge
		, ('RemarksTreatmentInformation') : RemarksTreatmentInformation
        , ('RemarksDiagnosa') : RemarksDiagnosa
		, ('DiagnosisQuestion') : DiagnosisQuestion
		, ('RemarksDiagnosis') : RemarksDiagnosis
        , ('MaternityTreatment') : MaternityTreatment
		, ('DoctorName') : DoctorName
		, ('Rujuk') : Rujuk
		, ('Reason') : Reason
		, ('SpecialCondition') : SpecialCondition
		, ('MTTindakanMedis') : MTTindakanMedis
		, ('MTDiagnosis') : MTDiagnosis
		, ('MedicalTreatment') : MedicalTreatment
		, ('UnregisteredMT') : UnregisteredMT
		, ('Billed') : Billed
		, ('AddFP'): AddFP, ('FamilyPlanningItem') : FamilyPlanningItem,('FamilyPlanning') : FamilyPlanning , ('FPBilled') : FPBilled
		, ('AppropriateRBClass') : AppropriateRBClass
        , ('TreatmentRBClass') : TreatmentRBClass
		, ('AppropriateRBClass') : AppropriateRBClass
		, ('TreatmentRBClass') : TreatmentRBClass
        , ('SpecialConditionReason') : SpecialConditionReason
		, ('RoomOptionAvailability') : RoomOptionAvailability
		, ('PackagePrice') : PackagePrice
		, ('NeedFollowUp') : NeedFollowUp
        , ('NewDocument') : NewDocument
		, ('NewDocument') : NewDocument, ('UploadDocument') : UploadDocument, ('InputDocType'): InputDocType, ('DocumentType') : DocumentType, ('DocSource') : DocSource
		, ('DocValidity') : DocValidity, ('FileLocation') : FileLocation, ('FileName'): FileLocation
		, ('EditDocument') : EditDocument
		, ('DeleteDocument') : DeleteDocument
		, ('NonMedicalItem') : NonMedicalItem
		, ('ActionGL') : ActionGL
		, ('PreviewDocument') : PreviewDocument
        , ('Validasi') : Validasi
		, ('Phase') : '1'])

WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Exit Confirmation/Exit Confirmation'), [('ECAction1') : ECAction1
        , ('ECAction2') : ECAction2, ('Comment') : Comment])

WebUI.comment(GlobalVariable.TicketID1)
//==================== PHASE 2 ====================
WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Home/Home'), [('Menu') : Menu, ('SubMenu') : SubMenu])

WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Create Ticket/Create Ticket'), [('ContactLine') : ContactLine, ('Product') : Product
		, ('ChannelType') : ChannelType, ('ContactName') : ContactName, ('ContactType') : ContactType, ('ServiceType') : ServiceType
		, ('InterruptedCall') : InterruptedCall, ('ProviderName') : ProviderName, ('Action') : ActionCT])

WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim Inquiry'), [('Phase') : '2'])

WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim'), [
	('Member') : Member
		, ('MemberName') : MemberName
        , ('ProductType') : ProductType
		, ('GLType') : GLType2
		, ('SpecialCondition') : SpecialCondition
		, ('EditTreatmentPeriodStart') : EditTreatmentPeriodStart
        , ('EditTreatmentPeriodEnd') : EditTreatmentPeriodEnd
		, ('IsODSODC') : IsODSODC
		, ('Diagnosis') : Diagnosis2
		, ('StatusDiagnosa') : StatusDiagnosa2
		, ('DiagnosisID') : DiagnosisID2
		, ('Gravida') : Gravida
		, ('Partus') : Partus
		, ('Abortus') : Abortus
		, ('GestationalAge') : GestationalAge
        , ('RemarksDiagnosa') : RemarksDiagnosa
		, ('DiagnosisQuestion') : DiagnosisQuestion
		, ('RemarksDiagnosis') : RemarksDiagnosis
        , ('MaternityTreatment') : MaternityTreatment
		, ('DoctorName') : DoctorName
		, ('Rujuk') : Rujuk
		, ('Reason') : Reason
		, ('SpecialCondition') : SpecialCondition
		, ('MTTindakanMedis') : MTTindakanMedis2
		, ('MTDiagnosis') : MTDiagnosis2
		, ('MedicalTreatment') : MedicalTreatment2
		, ('UnregisteredMT') : UnregisteredMT
		, ('Billed') : Billed
		, ('AppropriateRBClass') : AppropriateRBClass
        , ('TreatmentRBClass') : TreatmentRBClass
		, ('AppropriateRBClass') : AppropriateRBClass
		, ('TreatmentRBClass') : TreatmentRBClass
        , ('SpecialConditionReason') : SpecialConditionReason
		, ('RoomOptionAvailability') : RoomOptionAvailability
		, ('PackagePrice') : PackagePrice
		, ('NeedFollowUp') : NeedFollowUp
        , ('NewDocument') : NewDocument
		, ('EditDocument') : EditDocument
		, ('DeleteDocument') : DeleteDocument
		,('NonMedicalItem') : NonMedicalItem
		, ('ActionGL') : ActionGL
		, ('PreviewDocument') : PreviewDocument2
        , ('Validasi') : Validasi2
		, ('Phase') : '2'])

WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Exit Confirmation/Exit Confirmation'), [('ECAction1') : ECAction1
		, ('ECAction2') : ECAction2, ('Comment') : Comment])

WebUI.comment(MemberName)

WebUI.comment(GlobalVariable.TicketID2)
