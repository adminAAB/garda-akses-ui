import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

//Login//
String UserID = 'amumtazah@asuransiastra.com'

String Password = 'P@ssw0rd'

//Home
String Menu = 'Health'

String SubMenu = 'Create E-GL'

//GL Inquiry
String MemberNo = 'D/00061374'//findTestData('MemberNameSahabatFinansialKeluarga').getValue(3, 1)

//Claim
String Member = 'D/00061374'//findTestData('MemberNameSahabatFinansialKeluarga').getValue(1, 1)

String ProductType = 'Maternity (Persalinan)'//'Inpatient (Rawat Inap)' 

String GLType = 'Awal'

String IsODSODC = ''

String EditTreatmentPeriod = 'No'

String TreatmentPeriodStart = ''

String TreatmentPeriodEnd = ''

String TreatmentDuration = '3'

String AdminName = 'Admin Maste'//findTestData('ContactName').getValue(1, 1)

String Email = 'ehu@beyond.asuransi.astra.co.id'

String Phone = ''

String ProviderPhoneExt = '17241'

ArrayList Diagnosis = ['New']//,'New']

ArrayList StatusDiagnosa = ['Initial Primary (Diagnosa Utama)']//,'Initial Secondary (Diagnosa Tambahan)']

ArrayList DiagnosisID = ['O80']//,'R10']

String Gravida = '0'
String Partus = '0'
String Abortus = '0'
String GestationalAge = '0'


String RemarksDiagnosa = 'Currently testing Remarks by Automation. Thanks. Regards - MasTe'

String RemarksTreatmentInformation = 'Currently testing by Automation. Thanks. Regards - MasTe'

String DoctorName = 'Doctor MasTe'//'NBH ' + findTestData('MemberNameSahabatFinansialKeluarga').getValue(3, 1)

String TreatmentRBClass = ''//'Kelas II'

String RoomOptionAvailability = 'On Plan'

String MedicalTreatment = ''
String NonMedicalItem = ''

String Action = 'Process'

String PreviewDocument = 'Process'

ArrayList Validasi = [GlobalVariable.ValidasiDijaminkan]

//Claim Revisi

String GLTypeR = 'Revisi'

ArrayList DiagnosisR = ['']

ArrayList StatusDiagnosaR = ['']

ArrayList DiagnosisIDR = ['']

String MedicalTreatment2 = 'Unreg Medical 01'

ArrayList ValidasiR = [GlobalVariable.ValidasiDijaminkan]

WebUI.comment(Member)

WebUI.callTestCase(findTestCase('Pages/Web/GardaMedikaAkses/Login/Login'), [('UserID') : UserID, ('Password') : Password])

WebUI.callTestCase(findTestCase('Pages/Web/GardaMedikaAkses/Home/Home'), [('Menu') : Menu, ('SubMenu') : SubMenu])

WebUI.callTestCase(findTestCase('Pages/Web/GardaMedikaAkses/GL Inquiry/GL Inquiry'), [('Phase') : '1', ('MemberNo') : MemberNo])

WebUI.callTestCase(findTestCase('Pages/Web/GardaMedikaAkses/Claim/Claim'), 
	[('Phase') : '1'
	, ('Member') : Member
	, ('ProductType') : ProductType
	, ('GLType') : GLType
	, ('IsODSODC') : IsODSODC
	, ('EditTreatmentPeriod') : EditTreatmentPeriod
	, ('AdminName') : AdminName
	, ('Email') : Email
	, ('ProviderPhoneExt') : ProviderPhoneExt
	, ('Diagnosis') : Diagnosis
	, ('StatusDiagnosa') : StatusDiagnosa
	, ('DiagnosisID') : DiagnosisID
	, ('Gravida') : Gravida
	, ('Partus') : Partus
	, ('Abortus') : Abortus
	, ('GestationalAge') : GestationalAge
	, ('RemarksDiagnosa') : RemarksDiagnosa
	, ('RemarksTreatmentInformation') : RemarksTreatmentInformation
	, ('DoctorName') : DoctorName
	, ('TreatmentRBClass') : TreatmentRBClass
	, ('RoomOptionAvailability') : RoomOptionAvailability
	, ('MedicalTreatment') : MedicalTreatment
	, ('NonMedicalItem') : NonMedicalItem
	, ('Action') : Action
	, ('PreviewDocument') : PreviewDocument
	, ('Validasi') : Validasi])

WebUI.callTestCase(findTestCase('Pages/Web/GardaMedikaAkses/Home/Home'), [('Menu') : Menu, ('SubMenu') : SubMenu])

WebUI.callTestCase(findTestCase('Pages/Web/GardaMedikaAkses/GL Inquiry/GL Inquiry'), [('Phase') : '2', ('MemberNo') : MemberNo])

WebUI.callTestCase(findTestCase('Pages/Web/GardaMedikaAkses/Claim/Claim'), 
	[('Phase') : '2'
	, ('Member') : Member
	, ('ProductType') : ProductType
	, ('GLType') : GLTypeR
	, ('IsODSODC') : IsODSODC 
	, ('EditTreatmentPeriod') : EditTreatmentPeriod
	, ('AdminName') : AdminName
	, ('Email') : Email
	, ('ProviderPhoneExt') : ProviderPhoneExt
	, ('Diagnosis') : DiagnosisR
	, ('StatusDiagnosa') : StatusDiagnosaR
	, ('DiagnosisID') : DiagnosisIDR
	, ('Gravida') : Gravida
	, ('Partus') : Partus
	, ('Abortus') : Abortus
	, ('GestationalAge') : GestationalAge
	, ('RemarksDiagnosa') : RemarksDiagnosa
	, ('RemarksTreatmentInformation') : RemarksTreatmentInformation
	, ('DoctorName') : DoctorName
	, ('TreatmentRBClass') : TreatmentRBClass
	, ('RoomOptionAvailability') : RoomOptionAvailability
	, ('MedicalTreatment2') : MedicalTreatment2
	, ('NonMedicalItem') : NonMedicalItem
	, ('Action') : Action
	, ('PreviewDocument') : PreviewDocument
	, ('Validasi') : ValidasiR])