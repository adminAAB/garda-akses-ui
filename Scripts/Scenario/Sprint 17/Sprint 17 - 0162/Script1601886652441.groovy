import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5 as GEN5
import com.keyword.UI as UI

//CustomKeywords.'querySQL.Query.QueryFinishFollowUp'()

//Login//
String UserID = GlobalVariable.Username

String Password = GlobalVariable.Password

//Home
String Menu = 'General'

String SubMenu = 'Create Ticket'

String SubMenuFU = 'Follow Up'

//Create Ticket//
String ContactLine = 'Provider'

String Product = 'Health'

String ChannelType = 'Call'

String ContactName = findTestData('ContactName').getValue(1, 1)

String ContactType = 'Lainnya'

String ServiceType = 'Claim'

String InterruptedCall = 'No'

String ProviderName = 'OJKSH00001 SILOAM HOSPITALS KEBON JERUK'

String ActionCT = 'Next'

//Inquiry//
String Phase = '1'

//Claim
String Member = 'Existing' //Member = Existing  / New / Check

String MemberName = findTestData('MemberNameBankPermata').getValue(1, 1)

String ProductType = 'Inpatient (Rawat Inap)' //'Maternity (Persalinan)'

String GLType = 'Akhir'

String EditTreatmentPeriodStart = 'No'

String TreatmentPeriodStart = ''

String EditTreatmentPeriodEnd = 'No'

String TreatmentDuration = '3'

String SpecialCondition = 'No'

String SpecialConditionReason = ''

String IsODSODC = ''

ArrayList Diagnosis = ['New']

ArrayList StatusDiagnosa = ['Initial Primary (Diagnosa Utama)']

ArrayList DiagnosisID = ['A09']

String RemarksDiagnosa = 'Currently testing Remarks Diagnosa by Automation. Thanks. Regards - Me'

String DoctorName = 'NBH ' + findTestData('MemberNameBankPermata').getValue(3, 1)

String Rujuk = 'No'

String Reason = ''

//Medical Treatment
ArrayList MTTindakanMedis = ['']

ArrayList MTDiagnosis = ['SINGLE SPONTANEOUS DELIVERY/NORMAL DELIVERY']

ArrayList MedicalTreatment = ['']

String UnregisteredMT = 'tindakan tambahan caesar'

String Billed = '15000000'

//Family planning Item
String AddFP = 'No'

ArrayList FamilyPlanningItem = ['New']

ArrayList FamilyPlanning = ['KONDOM / DIAFRAGMA / CUP SERVIKS']

String FPBilled = '300000'

String TotalBilled = '1000000'

String AppropriateRBClass = 'STANDARD'

String TreatmentRBClass = 'STANDARD'

String RoomOptionAvailability = 'On Plan'

String NeedFollowUp = ''

String NonMedicalItem = ''

String NewDocument = 'No'

ArrayList UploadDocument = ['New', 'New']

ArrayList InputDocType = ['Billing Perawatan Asli', 'Billing Perbandingan']

ArrayList DocumentType = ['Billing Perawatan Asli', 'Billing Perbandingan']

String DocSource = 'WhatsApp'

String DocValidity = 'Dokumen Valid'

String FileLocation = 'D:\\'

String FileName = 'foto ktp'

String EditDocument = 'No'

String DeleteDocument = 'No'

String ActionGL = 'Process'

String PreviewDocument = 'Process'

ArrayList Validasi = [GlobalVariable.ValidasiDiagnosa, GlobalVariable.ValidasiPerhitunganExcess, GlobalVariable.ValidasiDocument]

//Exit Confirmation
String ECAction1 = 'Tidak'

String ECAction2 = 'Puas'

String Comment = 'Currently testing by Automation. Thanks. Regards - Me'

//Inq Follow Up
//Follow Up
String ContactNameFU = (ContactName + ' - ') + ContactType

String ClientNameFU = findTestData('MemberNameBankPermata').getValue(5, 1)

String MemberNameFU = findTestData('MemberNameBankPermata').getValue(4, 1)

String ProviderNameFU = 'OJKSH00001 - SILOAM HOSPITALS KEBON JERUK'

//Diagnosis Confirmation
ArrayList DiagnosisConfirmation = ['New']

ArrayList PICDiagnosis = ['Account Manager']

ArrayList AccountManagerNameDiagnosis = ['Arrio Probo Wibowo']

ArrayList ConfirmationDiagnosis = ['Covered']

String ChannelDiagnosis = 'Call'

ArrayList BenefitCoverageDiagnosis = ['IP - Cashless']

String RemarksDiagnosis = 'Currently testing by Automation. Thanks. Regards - NBH'

String ActionFUDiagnosis = 'Save'

//Document Confirmation
ArrayList DocumentConfirmation = ['New']

ArrayList PICDocuments = ['AM']

ArrayList AccountManagerNameDocuments = ['Arrio Probo Wibowo']

ArrayList ConfirmationDocuments = ['Covered']

String ChannelDocuments = 'Call'

ArrayList BenefitCoverageDocuments = ['IP - Cashless']

String RemarksDocuments = 'Currently testing by Automation. Thanks. Regards - NBH'

String ActionDocuments = 'Save'

//Excess Calculation Confirmation
ArrayList ExcessCalculationConfirmation = ['New']

ArrayList PICExcessCalculation = ['Account Manager']

ArrayList AccountManagerNameExcessCalculation = ['Arrio Probo Wibowo']

String ChannelExcessCalculation = 'Call'

ArrayList ConfirmationExcessCalculation = ['Document Received']

String RemarksExcessCalculation = 'Currently testing by Automation. Thanks. Regards - NBH'

String ActionExcessCalculation = 'Save'

String ActionPreviewDocumentFU = 'Process'

String FinalConfirmationFU = 'Tidak'

ArrayList ValidasiFU = [GlobalVariable.ValidasiDijaminkan]

WebUI.comment(MemberName)

//Script//
WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Login/Login'), [('UserID') : UserID, ('Password') : Password])

//==================== PHASE 1 ====================
WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Home/Home'), [('Menu') : Menu, ('SubMenu') : SubMenu])

WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Create Ticket/Create Ticket'), [('ContactLine') : ContactLine, ('Product') : Product
        , ('ChannelType') : ChannelType, ('ContactName') : ContactName, ('ContactType') : ContactType, ('ServiceType') : ServiceType
        , ('InterruptedCall') : InterruptedCall, ('ProviderName') : ProviderName, ('Action') : ActionCT])

WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim Inquiry'), [('Phase') : '1'])

WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim'), [('Member') : Member, ('MemberName') : MemberName
        , ('ProductType') : ProductType, ('GLType') : GLType, ('SpecialCondition') : SpecialCondition, ('EditTreatmentPeriodStart') : EditTreatmentPeriodStart
        , ('TreatmentPeriodStart') : TreatmentPeriodStart, ('EditTreatmentPeriodEnd') : EditTreatmentPeriodEnd, ('TreatmentDuration') : TreatmentDuration
        , ('IsODSODC') : IsODSODC, ('Diagnosis') : Diagnosis, ('StatusDiagnosa') : StatusDiagnosa, ('DiagnosisID') : DiagnosisID
        , ('RemarksDiagnosa') : RemarksDiagnosa, ('DoctorName') : DoctorName, ('TotalBilled') : TotalBilled, ('MTTindakanMedis') : MTTindakanMedis
        , ('MTDiagnosis') : MTDiagnosis, ('MedicalTreatment') : MedicalTreatment, ('UnregisteredMT') : UnregisteredMT, ('Billed') : Billed
        , ('AddFP') : AddFP, ('FamilyPlanningItem') : FamilyPlanningItem, ('FamilyPlanning') : FamilyPlanning, ('FPBilled') : FPBilled
        , ('Rujuk') : Rujuk, ('Reason') : Reason, ('SpecialCondition') : SpecialCondition, ('SpecialConditionReason') : SpecialConditionReason
        , ('AppropriateRBClass') : AppropriateRBClass, ('TreatmentRBClass') : TreatmentRBClass, ('RoomOptionAvailability') : RoomOptionAvailability
        , ('NeedFollowUp') : NeedFollowUp, ('NonMedicalItem') : NonMedicalItem, ('NewDocument') : NewDocument, ('UploadDocument') : UploadDocument
        , ('InputDocType') : InputDocType, ('DocumentType') : DocumentType, ('DocSource') : DocSource, ('DocValidity') : DocValidity
        , ('FileLocation') : FileLocation, ('FileName') : FileLocation, ('EditDocument') : EditDocument, ('DeleteDocument') : DeleteDocument
        , ('ActionGL') : ActionGL, ('PreviewDocument') : PreviewDocument, ('Validasi') : Validasi, ('Phase') : '1'])

WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Exit Confirmation/Exit Confirmation'), [('ECAction1') : ECAction1
        , ('ECAction2') : ECAction2, ('Comment') : Comment])

CustomKeywords.'querySQL.Query.QueryContactName'()

WebUI.comment(GlobalVariable.TicketID1)

WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Home/Home'), [('Menu') : Menu, ('SubMenu') : SubMenuFU])

WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Follow Up/Follow Up - Inquiry'), [:])

WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Follow Up/Follow Up - Provider Health Claim'), 
	[('PhaseFU') : '1'
        , ('PhaseGL') : '1'
		, ('UserID') : UserID
		, ('Password') : Password
		, ('Menu') : Menu
		, ('SubMenuFU') : SubMenuFU
		, ('FUContactName') : ContactNameFU
        , ('FUClientName') : ClientNameFU
		, ('FUMemberName') : MemberNameFU
		, ('MemberName') : MemberName
		, ('ProductType') : ProductType
        , ('GLType') : GLType
		, ('Provider') : ProviderNameFU
		, ('Validasi') : Validasi		
		//Diagnosis Confirmation
		, ('DiagnosisConfirmation') : DiagnosisConfirmation
		, ('PICDiagnosis') : PICDiagnosis
        , ('AccountManagerNameDiagnosis') : AccountManagerNameDiagnosis
		, ('ConfirmationDiagnosis') : ConfirmationDiagnosis
		, ('ChannelDiagnosis') : ChannelDiagnosis
		, ('BenefitCoverageDiagnosis') : BenefitCoverageDiagnosis
        , ('RemarksFUDiagnosis') : RemarksDiagnosis
		, ('ActionFUDiagnosis') : ActionFUDiagnosis
		//Document Confirmation
		, ('DocumentConfirmation') : DocumentConfirmation
		, ('PICDocuments') : PICDocuments
		, ('AccountManagerNameDocuments') : AccountManagerNameDocuments
		, ('ConfirmationDocuments') : ConfirmationDocuments
		, ('ChannelDocuments') : ChannelDocuments
		, ('BenefitCoverageDocuments') : BenefitCoverageDocuments
		, ('RemarksDocuments') : RemarksDocuments
		, ('ActionDocuments') : ActionDocuments
		//Excess Calculation Confirmation
		, ('ExcessCalculationConfirmation') : ExcessCalculationConfirmation
		, ('PICExcessCalculation') : PICExcessCalculation
		, ('AccountManagerNameExcessCalculation') : AccountManagerNameExcessCalculation
		, ('ChannelExcessCalculation') : ChannelExcessCalculation
		, ('ConfirmationExcessCalculation') : ConfirmationExcessCalculation
		, ('RemarksExcessCalculation') : RemarksExcessCalculation
		, ('ActionExcessCalculation') : ActionExcessCalculation
		, ('ActionPreviewDocumentFU') : ActionPreviewDocumentFU
        , ('FinalConfirmationFU') : FinalConfirmationFU
		, ('ValidasiFU') : ValidasiFU])

WebUI.comment(MemberName)

WebUI.comment(GlobalVariable.TicketID1)

WebUI.comment(GlobalVariable.TrID1)