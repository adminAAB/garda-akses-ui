import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5 as GEN5
import com.keyword.UI as UI

//Login//
String UserID = 'DNS'

String Password = 'ITG@nt1P455QC'

//Home
String Menu = 'General'

String SubMenu = 'Create Ticket'

//Create Ticket//
String ContactLine = 'Provider'

String Product = 'Health'

String ChannelType = 'Call'

String ContactName = findTestData('ContactName').getValue(1, 1)

String ContactType = 'Dokter'

String ServiceType = 'Claim'

String InterruptedCall = 'No'

String ProviderName = 'OJKSH00001 SILOAM HOSPITALS KEBON JERUK'

String ValidasiProviderName = 'OJKSH00001 - SILOAM HOSPITALS KEBON JERUK'

String ActionCT = 'Next'

//Inquiry//
String MemberNo = 'D/00051372'

//Claim
String Member = 'Existing' //Member = Existing  / New / Check Member / Check New Member

String MemberName = '819 - D/00051372 - DWI RATNANINGSIH - PT FEDERAL IZUMI MANUFACTURING'

String NewMemberType = 'Employee'

String NewMemberName = findTestData('NewMemberName').getValue(1, 1)

String ClientName = 'PT Federal Izumi Manufacturing'

String EmployeeID = findTestData('NewEmployeeID').getValue(1, 1)

String DOB = '01/Aug/2019'

String Gender = 'Female'

String ProductType =  'Maternity (Persalinan)' //'Inpatient (Rawat Inap)'

String GLType = 'Awal'

String GLType2 = 'Revisi'

String EditTreatmentPeriodStart = 'No'

String EditTreatmentPeriodEnd = 'No'

String TreatmentDuration = '3'

String SpecialCondition = 'No'

String SpecialConditionReason = ''

String IsODSODC = ''

String IsODSODC2 = 'ODS'

ArrayList Diagnosis = ['New']

ArrayList StatusDiagnosa = ['Initial Primary (Diagnosa Utama)']

ArrayList DiagnosisID = ['O80 SINGLE SPONTANEOUS DELIVERY/NORMAL DELIVERY (PERSALINAN NORMAL)']

ArrayList Diagnosis2 = []

ArrayList StatusDiagnosa2 = []

ArrayList DiagnosisID2 = []

String Gravida = '1'

String Partus = '1'

String Abortus = '0'

String GestationalAge = '20'

String RemarksDiagnosa = 'Testing GA - MA - GL Revisi - Validasi ODS Reject Client E-GL - RRU'

String RemarksDiagnosa2 = null

String DiagnosisQuestion = ''

String RemarksDiagnosis = ''

String MaternityTreatment = 'Persalinan Normal dengan Dokter'

String DoctorName = 'Automation Doctor - RRU'

String Rujuk = 'No'

String Reason = ''

String AddFP = ''

String MTTindakanMedis = ''

String MTDiagnosis = ''

String MedicalTreatment = ''

String UnregisteredMT = ''

String AppropriateRBClass = 'BASIC'

String TreatmentRBClass = 'BASIC'

String RoomOptionAvailability = 'On Plan'

String PackagePrice = '100000'

String TotalBilled = '100000'

String NeedFollowUp = ''

String NonMedicalItem = ''

String NewDocument = 'No'

String EditDocument = 'No'

String DeleteDocument = 'No'

String ActionGL = 'Process'

String PreviewDocument = 'Process'

String NeedFU = 'Tidak'

String FUPreAdm = ''

ArrayList Validasi = [GlobalVariable.ValidasiDijaminkan]

ArrayList Validasi2 = [GlobalVariable.ValidasiTidakDijaminkan]

////Phase 2
//String Member2 = 'Check New Member'
//
//String AccountManager = 'Automation NBH'
//
//ArrayList Validasi2 = [GlobalVariable.ValidasiDijaminkan]

//Exit Confirmation
String ECAction1 = 'Tidak'

String ECAction2 = 'Puas'

String Comment = 'Currently testing by Automation. Thanks. Regards - RRU'

//Script//
WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Login/Login'), [('UserID') : UserID, ('Password') : Password])

//==================== PHASE 1 ====================
WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Home/Home'), [('Menu') : Menu, ('SubMenu') : SubMenu])

WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Create Ticket/Create Ticket'), [('ContactLine') : ContactLine, ('Product') : Product
		, ('ChannelType') : ChannelType, ('ContactName') : ContactName, ('ContactType') : ContactType, ('ServiceType') : ServiceType
		, ('InterruptedCall') : InterruptedCall, ('ProviderName') : ProviderName, ('Action') : ActionCT])

CustomKeywords.'querySQL.Query.QueryContactName'()

WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim Inquiry'), [('Phase') : '1'])

CustomKeywords.'querySQL.Query.QueryNewMemberName'()

CustomKeywords.'querySQL.Query.QueryEmployeeID'()

WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim'),
	[('Phase') : '1'
		, ('Member') : Member
		, ('MemberName') : MemberName
		//, ('NewMemberType') : NewMemberType
		//, ('NewMemberName') : NewMemberName
		//, ('ClientName') : ClientName
		//, ('EmployeeID') : EmployeeID
		//, ('DOB') : DOB
		//, ('Gender') : Gender
		, ('ProductType') : ProductType
		, ('GLType') : GLType
		, ('EditTreatmentPeriodStart') : EditTreatmentPeriodStart
		, ('EditTreatmentPeriodEnd') : EditTreatmentPeriodEnd
		, ('TreatmentDuration') : TreatmentDuration
		, ('SpecialCondition') : SpecialCondition
		, ('SpecialConditionReason') : SpecialConditionReason
		, ('IsODSODC') : IsODSODC
		, ('Diagnosis') : Diagnosis
		, ('StatusDiagnosa') : StatusDiagnosa
		, ('DiagnosisID') : DiagnosisID
		, ('Gravida') : Gravida
		, ('Partus') : Partus
		, ('Abortus') : Abortus
		, ('GestationalAge') : GestationalAge
        , ('RemarksDiagnosa') : RemarksDiagnosa
		, ('DiagnosisQuestion') : DiagnosisQuestion
		, ('RemarksDiagnosis') : RemarksDiagnosis
		, ('MaternityTreatment') : MaternityTreatment
		, ('DoctorName') : DoctorName
		, ('Rujuk') : Rujuk
		, ('Reason') : Reason
		, ('AddFP') : AddFP
		, ('MTTindakanMedis') : MTTindakanMedis
		, ('MTDiagnosis') : MTDiagnosis
		, ('MedicalTreatment') : MedicalTreatment
		, ('UnregisteredMT') : UnregisteredMT
		, ('AppropriateRBClass') : AppropriateRBClass
		, ('TreatmentRBClass') : TreatmentRBClass
		, ('RoomOptionAvailability') : RoomOptionAvailability
		, ('PackagePrice') : PackagePrice
		, ('TotalBilled') : TotalBilled
		, ('NeedFollowUp') : NeedFollowUp
		, ('NonMedicalItem') : NonMedicalItem
		, ('NewDocument') : NewDocument
		, ('EditDocument') : EditDocument
		, ('DeleteDocument') : DeleteDocument
		, ('ActionGL') : ActionGL
		, ('PreviewDocument') : PreviewDocument
		, ('NeedFU') : NeedFU
		, ('FUPreAdm') : FUPreAdm
		, ('Validasi') : Validasi])

WebUI.comment(MemberName)

WebUI.comment(GlobalVariable.TicketID1)

CustomKeywords.'querySQL.Query.QueryContactName'()

CustomKeywords.'querySQL.Query.QueryEmployeeID'()

WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Exit Confirmation/Exit Confirmation'), [('ECAction1') : ECAction1
        , ('ECAction2') : ECAction2, ('Comment') : Comment])

String URL = '172.16.94.70'
String DB_Name = 'SEA'
String Query = 'SELECT UPPER(CONCAT(RTRIM(ProviderID), SPACE(1), \'-\', SPACE(1), RTRIM(( ProviderName )))) AS ProviderName, ProviderPhoneNo AS ProviderPhoneNo, ProviderEmail AS ProviderEmail, TicketNo AS TicketNo, FamilyPhone AS FamilyPhone, Doctor AS Doctor FROM CONTACTCENTER.TempGL WHERE TicketNo = \'' + GlobalVariable.TicketID1 +'\' ORDER BY CreatedDate DESC'
ArrayList VerifyTicket1 = [ValidasiProviderName, GlobalVariable.PhoneNumber, GlobalVariable.Email, GlobalVariable.TicketID1, GlobalVariable.PhoneNumber, DoctorName]

GEN5.compareRowDBtoArray(URL, DB_Name, Query, VerifyTicket1)

CustomKeywords.'querySQL.Query.QueryFinishFollowUp'()

//==================== PHASE 2 ====================
WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Home/Home'), [('Menu') : Menu, ('SubMenu') : SubMenu])

WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Create Ticket/Create Ticket'), [('ContactLine') : ContactLine, ('Product') : Product
        , ('ChannelType') : ChannelType, ('ContactName') : ContactName, ('ContactType') : ContactType, ('ServiceType') : ServiceType
        , ('InterruptedCall') : InterruptedCall, ('ProviderName') : ProviderName, ('Action') : ActionCT])

WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim Inquiry'), 
	[('Phase') : '2'
		, ('MemberNo') : MemberNo])

WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim'),
	[('Phase') : '2'
		, ('Member') : Member
		, ('MemberName') : MemberName
		//, ('NewMemberType') : NewMemberType
		//, ('NewMemberName') : NewMemberName
		//, ('ClientName') : ClientName
		//, ('EmployeeID') : EmployeeID
		//, ('DOB') : DOB
		//, ('Gender') : Gender
		, ('ProductType') : ProductType
		, ('GLType') : GLType2
		, ('EditTreatmentPeriodStart') : EditTreatmentPeriodStart
		, ('EditTreatmentPeriodEnd') : EditTreatmentPeriodEnd
		, ('TreatmentDuration') : TreatmentDuration
		, ('SpecialCondition') : SpecialCondition
		, ('SpecialConditionReason') : SpecialConditionReason
		, ('IsODSODC') : IsODSODC2
		, ('Diagnosis') : Diagnosis2
		, ('StatusDiagnosa') : StatusDiagnosa2
		, ('DiagnosisID') : DiagnosisID2
		, ('Gravida') : Gravida
		, ('Partus') : Partus
		, ('Abortus') : Abortus
		, ('GestationalAge') : GestationalAge
		, ('RemarksDiagnosa') : RemarksDiagnosa2
		, ('DiagnosisQuestion') : DiagnosisQuestion
		, ('RemarksDiagnosis') : RemarksDiagnosis
		, ('MaternityTreatment') : MaternityTreatment
		, ('DoctorName') : DoctorName
		, ('Rujuk') : Rujuk
		, ('Reason') : Reason
		, ('AddFP') : AddFP
		, ('MTTindakanMedis') : MTTindakanMedis
		, ('MTDiagnosis') : MTDiagnosis
		, ('MedicalTreatment') : MedicalTreatment
		, ('UnregisteredMT') : UnregisteredMT
		, ('AppropriateRBClass') : AppropriateRBClass
		, ('TreatmentRBClass') : TreatmentRBClass
		, ('RoomOptionAvailability') : RoomOptionAvailability
		, ('PackagePrice') : PackagePrice
		, ('TotalBilled') : TotalBilled
		, ('NeedFollowUp') : NeedFollowUp
		, ('NonMedicalItem') : NonMedicalItem
		, ('NewDocument') : NewDocument
		, ('EditDocument') : EditDocument
		, ('DeleteDocument') : DeleteDocument
		, ('ActionGL') : ActionGL
		, ('PreviewDocument') : PreviewDocument
		, ('NeedFU') : NeedFU
		, ('FUPreAdm') : FUPreAdm
		, ('Validasi') : Validasi2])

WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Exit Confirmation/Exit Confirmation'), [('ECAction1') : ECAction1
        , ('ECAction2') : ECAction2, ('Comment') : Comment])

WebUI.comment(MemberName)

WebUI.comment(GlobalVariable.TicketID1)

WebUI.comment(GlobalVariable.TicketID2)