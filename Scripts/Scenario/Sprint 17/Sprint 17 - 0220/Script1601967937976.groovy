import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5 as GEN5
import com.keyword.UI as UI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

CustomKeywords.'querySQL.Query.QueryFinishFollowUp'()

//Login//
String UserID = GlobalVariable.Username

String Password = GlobalVariable.Password

//Home
String Menu = 'General'

String SubMenu = 'Create Ticket'

String SubMenuFU = 'Follow Up'

//Create Ticket//
String ContactLine = 'Provider'

String Product = 'Health'

String ChannelType = 'Call'

String ContactName = findTestData('ContactName').getValue(1, 1)

String ContactType = 'Lainnya'

String ServiceType = 'Claim'

String InterruptedCall = 'No'

String ProviderName = 'OJKSH00001 SILOAM HOSPITALS KEBON JERUK'

String ActionCT = 'Next'

//Inquiry//
//String Phase = '1'
String MemberNo = findTestData('MemberNameFederalInternationalFinanceMA').getValue(3, 50)

//Claim
String Member = 'Existing' //Member = Existing  / New / Check

String MemberName = findTestData('MemberNameFederalInternationalFinanceMA').getValue(1, 50)

String ProductType = 'Maternity (Persalinan)' //'Maternity (Persalinan)'

String GLType = 'Awal'

String GLType2 = 'Revisi'

String IsODSODC = ''

String EditTreatmentPeriodStart = 'No'

String TreatmentPeriodStart = '27/Feb/2020'

String EditTreatmentPeriodEnd = 'No'

String TreatmentDuration = '3'

String SpecialCondition = 'No'

String SpecialConditionReason = ''
//GL Awal
ArrayList Diagnosis = ['New']

ArrayList StatusDiagnosa = ['Initial Primary (Diagnosa Utama)']

ArrayList DiagnosisID = ['O80']

String Gravida = '1'

String Partus = '0'

String Abortus = '0'

String GestationalAge = '35'

String MaternityTreatment = 'Persalinan Normal dengan Dokter'

String RemarksDiagnosa = 'Currently testing Remarks Diagnosa by Automation. Thanks. Regards - Me'

//GL Revisi
ArrayList Diagnosis2 = ['']

ArrayList StatusDiagnosa2 = ['Initial Primary (Diagnosa Utama)']

ArrayList DiagnosisID2 = ['O80']

String Gravida2 = '1'

String Partus2 = '0'

String Abortus2 = '0'

String GestationalAge2 = '35'

String RemarksTreatmentInformation = 'OAS - 0220'

String MaternityTreatment2 = 'Persalinan Normal dengan Dokter'

String RemarksDiagnosa2 = 'Currently testing Remarks Diagnosa by Automation. Thanks. Regards - Me'

String DoctorName = 'OAS ' + findTestData('MemberNameFederalInternationalFinanceMA').getValue(3, 50)

String Rujuk = 'No'

String MedicalTreatment = ''

//Family Planning Item
//GL Awal
String AddFP = 'No'

ArrayList FamilyPlanningItem = ['']

//ArrayList FamilyPlanning = ['KONDOM / DIAFRAGMA / CUP SERVIKS']

ArrayList FamilyPlanning = ['KB IMPLAN / SUSUK KB']

String FPBilled = '300000'

//GL revisi
String AddFP2 = 'Yes'

ArrayList FamilyPlanningItem2 = ['New']

ArrayList FamilyPlanning2 = ['KONDOM / DIAFRAGMA / CUP SERVIKS']

//ArrayList FamilyPlanning2 = ['KB IMPLAN / SUSUK KB']

String FPBilled2 = '300000'


String Reason = ''

String PackagePrice = '3000000'

String TotalBilled = ''

String AppropriateRBClass = 'STANDARD'

String TreatmentRBClass = 'STANDARD'

String RoomOptionAvailability = 'On Plan'

String NeedFollowUp = ''

String NonMedicalItem = ''

//DOKUMEN

String NewDocument = 'No'

String InputDocType = 'Billing Perawatan Asli'

String DocType = 'Billing Perawatan Asli'

String DocSource = 'WhatsApp'

String DocValidity = 'Dokumen Valid'

String FileLocation = 'D:\\'

String FileName = 'foto ktp'

String EditDocument = 'No'

String DeleteDocument = 'No'

String ActionGL = 'Process'

String PreviewDocument = 'Process'

String PreviewDocument2 = ''

ArrayList Validasi = [ GlobalVariable.ValidasiDijaminkan]

ArrayList Validasi2 = [ GlobalVariable.ValidasiFamilyPlanning]

//Exit Confirmation
String ECAction1 = 'Tidak'

String ECAction2 = 'Puas'

String Comment = 'Currently testing by Automation. Thanks. Regards - Me'

//Inq Follow Up
//Follow Up
String ContactNameFU = (ContactName + ' - ') + ContactType

String ClientNameFU = findTestData('MemberNameFederalInternationalFinanceMA').getValue(5, 50)

String MemberNameFU = findTestData('MemberNameFederalInternationalFinanceMA').getValue(4, 50)

String ProviderNameFU = 'OJKSH00001 - SILOAM HOSPITALS KEBON JERUK'

String Coverage = 'Covered'

ArrayList PICFamilyPlanning = ['Account Manager']

ArrayList FamilyPlanningConfirmation = ['New']

String AccountManagerNameFamilyPlanning = 'Arrio Probo Wibowo' //AM

ArrayList ConfirmationFamilyPlanning = ['Covered']

String ChannelFamilyPlanning = 'Call'

String BenefitCoverageFamilyPlanning = 'IP - Cashless'

String RemarksFUFamilyPlanning = 'Currently testing by Automation. Thanks. Regards - OAS'

String ActionFUFamilyPlanning = 'Save'

String ActionPreviewDocumentFU = 'Process'

String FinalConfirmationFU = 'Tidak'

ArrayList ValidasiFU = [GlobalVariable.ValidasiDijaminkan]

//Script//
WebUI.comment(MemberName)

WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Login/Login'), [('UserID') : UserID, ('Password') : Password])

//==================== PHASE 1 ====================
WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Home/Home'), [('Menu') : Menu, ('SubMenu') : SubMenu])

WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Create Ticket/Create Ticket'), [('ContactLine') : ContactLine, ('Product') : Product
        , ('ChannelType') : ChannelType, ('ContactName') : ContactName, ('ContactType') : ContactType, ('ServiceType') : ServiceType
        , ('InterruptedCall') : InterruptedCall, ('ProviderName') : ProviderName, ('Action') : ActionCT])

WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim Inquiry'), [('Phase') : '1'])

WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim'), [('Member') : Member, ('MemberName') : MemberName
        , ('ProductType') : ProductType, ('GLType') : GLType, ('IsODSODC') : IsODSODC, ('SpecialCondition') : SpecialCondition
        , ('EditTreatmentPeriodStart') : EditTreatmentPeriodStart, ('EditTreatmentPeriodEnd') : EditTreatmentPeriodEnd,('TreatmentDuration'): TreatmentDuration
		, ('Diagnosis') : Diagnosis, ('Gravida') : Gravida, ('Partus') : Partus, ('Abortus') : Abortus, ('GestationalAge') : GestationalAge
        , ('StatusDiagnosa') : StatusDiagnosa, ('DiagnosisID') : DiagnosisID, ('RemarksDiagnosa') : RemarksDiagnosa, ('DoctorName') : DoctorName, ('MaternityTreatment') : MaternityTreatment
        , ('RemarksTreatmentInformation') : RemarksTreatmentInformation, ('TotalBilled') : TotalBilled, ('Rujuk') : Rujuk, ('MedicalTreatment') : MedicalTreatment, ('Reason') : Reason
		, ('AddFP') : AddFP, ('FamilyPlanningItem') : FamilyPlanningItem, ('FamilyPlanning') : FamilyPlanning, ('FPBilled') : FPBilled
        , ('SpecialCondition') : SpecialCondition, ('SpecialConditionReason') : SpecialConditionReason, ('AppropriateRBClass') : AppropriateRBClass
        , ('TreatmentRBClass') : TreatmentRBClass, ('RoomOptionAvailability') : RoomOptionAvailability, ('NeedFollowUp') : NeedFollowUp, ('PackagePrice') : PackagePrice
        , ('NonMedicalItem') : NonMedicalItem, ('NewDocument') : NewDocument
		, ('InputDocType') : InputDocType, ('DocType') : DocType, ('DocSource') : DocSource, ('DocValidity') : DocValidity, ('FileLocation') :FileLocation, ('FileName') : FileName
		, ('EditDocument') : EditDocument, ('DeleteDocument') : DeleteDocument
        , ('ActionGL') : ActionGL, ('PreviewDocument') : PreviewDocument, ('Validasi') : Validasi, ('Phase') : '1'])

WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Exit Confirmation/Exit Confirmation'), [('ECAction1') : ECAction1
        , ('ECAction2') : ECAction2, ('Comment') : Comment])

//==================== PHASE 2 ====================
WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Home/Home'), [('Menu') : Menu, ('SubMenu') : SubMenu])

WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Create Ticket/Create Ticket'), [('ContactLine') : ContactLine, ('Product') : Product
        , ('ChannelType') : ChannelType, ('ContactName') : ContactName, ('ContactType') : ContactType, ('ServiceType') : ServiceType
        , ('InterruptedCall') : InterruptedCall, ('ProviderName') : ProviderName, ('Action') : ActionCT])

WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim Inquiry'), 
	[('Phase') : '2'
		, ('MemberNo') : MemberNo])

WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim'), [('Member') : Member, ('MemberName') : MemberName
		, ('ProductType') : ProductType, ('GLType') : GLType2, ('IsODSODC') : IsODSODC, ('SpecialCondition') : SpecialCondition
		, ('EditTreatmentPeriodStart') : EditTreatmentPeriodStart, ('EditTreatmentPeriodEnd') : EditTreatmentPeriodEnd,('TreatmentDuration'): TreatmentDuration
		, ('Diagnosis') : Diagnosis2, ('Gravida') : Gravida2, ('Partus') : Partus2, ('Abortus') : Abortus2, ('GestationalAge') : GestationalAge2
		, ('StatusDiagnosa') : StatusDiagnosa, ('DiagnosisID') : DiagnosisID, ('RemarksDiagnosa') : RemarksDiagnosa, ('DoctorName') : DoctorName, ('MaternityTreatment') : MaternityTreatment
		, ('RemarksTreatmentInformation') : RemarksTreatmentInformation, ('TotalBilled') : TotalBilled, ('Rujuk') : Rujuk, ('MedicalTreatment') : MedicalTreatment, ('Reason') : Reason
		, ('AddFP') : AddFP2, ('FamilyPlanningItem') : FamilyPlanningItem2, ('FamilyPlanning') : FamilyPlanning2, ('FPBilled') : FPBilled2
		, ('SpecialCondition') : SpecialCondition, ('SpecialConditionReason') : SpecialConditionReason, ('AppropriateRBClass') : AppropriateRBClass
		, ('TreatmentRBClass') : TreatmentRBClass, ('RoomOptionAvailability') : RoomOptionAvailability, ('NeedFollowUp') : NeedFollowUp, ('PackagePrice') : PackagePrice
		, ('NonMedicalItem') : NonMedicalItem, ('NewDocument') : NewDocument
		, ('InputDocType') : InputDocType, ('DocType') : DocType, ('DocSource') : DocSource, ('DocValidity') : DocValidity, ('FileLocation') :FileLocation, ('FileName') : FileName
		, ('EditDocument') : EditDocument, ('DeleteDocument') : DeleteDocument
		, ('ActionGL') : ActionGL, ('PreviewDocument') : PreviewDocument, ('Validasi') : Validasi2, ('Phase') : '2'])
WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Exit Confirmation/Exit Confirmation'), [('ECAction1') : ECAction1
		, ('ECAction2') : ECAction2, ('Comment') : Comment])

WebUI.comment(GlobalVariable.TicketID1)

CustomKeywords.'querySQL.Query.QueryContactName'()

WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Home/Home'), [('Menu') : Menu, ('SubMenu') : SubMenuFU])

WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Follow Up/Follow Up - Inquiry'), [:])

WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Follow Up/Follow Up - Provider Health Claim'), [('PhaseFU') : '1'
		, ('PhaseGL') : '2'
        , ('UserID') : UserID, ('Password') : Password, ('Menu') : Menu, ('SubMenuFU') : SubMenuFU, ('FUContactName') : ContactNameFU
        , ('FUClientName') : ClientNameFU, ('FUMemberName') : MemberNameFU, ('MemberName') : MemberName, ('ProductType') : ProductType
        , ('GLType') : GLType2, ('Provider') : ProviderNameFU, ('Validasi') : Validasi2, ('Coverage') : Coverage, ('PICFamilyPlanning') : PICFamilyPlanning
        , ('FamilyPlanningConfirmation') : FamilyPlanningConfirmation, ('AccountManagerNameFamilyPlanning') : AccountManagerNameFamilyPlanning, ('ConfirmationFamilyPlanning') : ConfirmationFamilyPlanning
        , ('ChannelFamilyPlanning') : ChannelFamilyPlanning, ('BenefitCoverageFamilyPlanning') : BenefitCoverageFamilyPlanning, ('RemarksFUFamilyPlanning') : RemarksFUFamilyPlanning, ('ActionFUFamilyPlanning') : ActionFUFamilyPlanning
        , ('ActionPreviewDocumentFU') : ActionPreviewDocumentFU, ('FinalConfirmationFU') : FinalConfirmationFU, ('ValidasiFU') : ValidasiFU])

WebUI.comment(MemberName)

WebUI.comment(GlobalVariable.TicketID1)

WebUI.comment(GlobalVariable.TrID1)