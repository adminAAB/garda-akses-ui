import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

//Login//
String UserID = 'amumtazah@asuransiastra.com'

String Password = 'P@ssw0rd'

//Home
String Menu = 'Health'

String SubMenu = 'Create E-GL'

//GL Inquiry
String Phase = '1'

String MemberNo = findTestData('MemberNameClientDensoIndonesiaSunterMA').getValue(3, 51)

//Claim
String Member = findTestData('MemberNameClientDensoIndonesiaSunterMA').getValue(3, 51)

String ProductType = 'Maternity (Persalinan)' //'Inpatient (Rawat Inap)' //

String GLType = 'Awal'

String IsODSODC = ''

String EditTreatmentPeriod = 'No'

String TreatmentPeriodStart = ''

String TreatmentPeriodEnd = ''

String EditTreatmentDuration = 'No'

String TreatmentDuration = '3'

String AdminName = findTestData('ContactName').getValue(1, 1)

String Email = 'ehu@beyond.asuransi.astra.co.id'

String Phone = ''

String ProviderPhoneExt = '17241'

ArrayList Diagnosis = ['New']

ArrayList StatusDiagnosa = ['Initial Primary (Diagnosa Utama)']

ArrayList DiagnosisID = ['O80']

String RemarksDiagnosa = 'Currently testing Remarks Diagnosa by Automation. Thanks. Regards - NBH'

String Gravida = '1'

String Partus = '1'

String Abortus = '1'

String GestationalAge = '20'

String RemarksTreatmentInformation = ((((AdminName + ' GL ') + GLType) + ' ') + MemberNo) + ' By NBH'

String MaternityTreatment = 'Persalinan Normal dengan Dokter'

String DoctorName = 'NBH ' + MemberNo

String AddFP = ''

String MedicalTreatment = ''

String TreatmentRBClass = 'Kelas I'

String RoomOptionAvailability = 'On Plan'

String PackagePrice = '500000'

String NonMedicalItem = ''

String Action = 'Process'

String PreviewDocument = 'Process'

ArrayList Validasi = [GlobalVariable.ValidasiDijaminkan]

//Script
WebUI.callTestCase(findTestCase('Pages/Web/GardaMedikaAkses/Login/Login'), [('UserID') : UserID, ('Password') : Password])

//Phase 1
WebUI.callTestCase(findTestCase('Pages/Web/GardaMedikaAkses/Home/Home'), [('Menu') : Menu, ('SubMenu') : SubMenu])

WebUI.callTestCase(findTestCase('Pages/Web/GardaMedikaAkses/GL Inquiry/GL Inquiry'), [('Phase') : Phase])

WebUI.callTestCase(findTestCase('Pages/Web/GardaMedikaAkses/Claim/Claim'), [
	('Phase') : Phase
	, ('Member') : Member
	, ('ProductType') : ProductType
	, ('GLType') : GLType
	, ('IsODSODC') : IsODSODC
	, ('EditTreatmentPeriod') : EditTreatmentPeriod
	, ('AdminName') : AdminName
	, ('Email') : Email
	, ('ProviderPhoneExt') : ProviderPhoneExt
	, ('Diagnosis') : Diagnosis
	, ('StatusDiagnosa') : StatusDiagnosa
	, ('DiagnosisID') : DiagnosisID, ('Gravida') : Gravida, ('Partus') : Partus
    , ('Abortus') : Abortus, ('GestationalAge') : GestationalAge, ('RemarksDiagnosa') : RemarksDiagnosa
	, ('RemarksDiagnosa') : RemarksDiagnosa
	, ('MaternityTreatment') : MaternityTreatment
	, ('RemarksTreatmentInformation') : RemarksTreatmentInformation
	, ('DoctorName') : DoctorName
	, ('AddFP') : AddFP
	, ('MedicalTreatment') : MedicalTreatment
	, ('TreatmentRBClass') : TreatmentRBClass
	, ('RoomOptionAvailability') : RoomOptionAvailability
	, ('PackagePrice') : PackagePrice
	, ('NonMedicalItem') : NonMedicalItem
	, ('Action') : Action
	, ('PreviewDocument') : PreviewDocument
	, ('Validasi') : Validasi])

WebUI.comment(Member)

def TrID = CustomKeywords.'querySQL.Query.QueryGetTrID'(RemarksTreatmentInformation)

WebUI.comment(TrID)

CustomKeywords.'querySQL.Query.QueryContactName'()