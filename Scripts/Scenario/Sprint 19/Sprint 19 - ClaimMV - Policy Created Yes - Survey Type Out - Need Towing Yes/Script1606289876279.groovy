import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5 as GEN5
import com.keyword.UI as UI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

String vContactName = 'NBH Skenario 7'

//String vContactName = 'Req Mba Dele'

String vParameter = '1902131450'

String needTowing = 'Yes'

String vRegistrationNo = 'TBWRD'

String customerGOXC = 'Yes'

String vipRelatives = 'No'

String location = 'Kostainer 17.17'

String detailLocation = 'Kostan nomor 17, dibelakang cilandak town square'

String destination = 'Kostainer 17.17'

String detailDestination = 'Kostan nomor 17, dibelakang cilandak town square'

String vFuStatus = 'Close'

String vCreateLk = 'Yes'

//Login//
String UserID = GlobalVariable.Username

String Password = GlobalVariable.Password

//Home
String Menu = 'General'

String SubMenu = 'Create Ticket'

String SubMenuFU = 'Follow Up'

//Create Ticket//
String ContactLine = 'Customer'

String Product = 'Non Health'

String ChannelType = 'Call'

//String ContactName = findTestData('ContactName').getValue(1, 1)

String ContactName = vContactName

String ContactType = 'Teman'

String ServiceType = 'Claim'

String InterruptedCall = ''

String ProviderName = ''

String CustomerGender = 'Male'

String ActionCT = 'Next'

//Choose Customer
String ChooseCustomerCategory = 'Policy Holder / Relatives'

String ActionCC1 = 'Next'

String SearchBy = 'Policy Number'

String Parameter = vParameter //ambil dari db

String ActionCC2 = 'Next'

String ActionCC3 = 'Finish'

//Claim MV
String product = 'Garda Oto'

String policyHasBeenCreated = 'Yes'

String searchBy = 'Postal Code'

String valueSearchBy = '12430'

String locationOfAccident = 'Jakarta Selatan'

String accidentInformation = 'Mobil terendam banjir'

String causeOfLoss = 'Banjir'

String surveyType = 'Survey-out'

String dateSurveySchedule = '7/Dec/2020'

String surveyCategoryTime = 'Sore'

String customerOnLocation = 'Kostan'

String picPhoneNumber = '08123456789'

String surveyAddress = 'Sesuai dengan alamat yang tertera'

String registrationNumber = vRegistrationNo //ambil dari db

String mobilePhone1 = '08123456789'

String requestCategory = 'Flood'

String fuStatus = vFuStatus

String fuRemarks = ContactName + ' Search By ' + SearchBy + ' ' + Parameter + ', Thankyou. Regards - NBH'

String action = 'Save'

String createLk = vCreateLk

//Exit Confirmation
String ECAction1 = 'Tidak'

String ECAction2 = 'Puas'

String Comment = 'Currently testing by Automation. Thanks. Regards - NBH'

//WebUI.comment(MemberName)
//Script//
WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Login/Login'), [('UserID') : UserID, ('Password') : Password])

//==================== PHASE 1 ====================
WebUI.callTestCase(findTestCase('Pages/Web/GEN5/Home/Home'), [('Menu') : Menu, ('SubMenu') : SubMenu])

WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Create Ticket/Create Ticket'), [('ContactLine') : ContactLine, ('Product') : Product
        , ('ChannelType') : ChannelType, ('ContactName') : ContactName, ('ContactType') : ContactType, ('ServiceType') : ServiceType
        , ('InterruptedCall') : InterruptedCall, ('ProviderName') : ProviderName, ('CustomerGender') : CustomerGender, ('Action') : ActionCT])

WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Choose Customer/Choose Customer'), [('ChooseCustomerCategory') : ChooseCustomerCategory
        , ('ActionCC1') : ActionCC1, ('SearchBy') : SearchBy, ('Parameter') : Parameter, ('ActionCC2') : ActionCC2, ('ActionCC3') : ActionCC3])

WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV'), 
	[('product') : product
		, ('policyHasBeenCreated') : policyHasBeenCreated
		, ('Parameter') : Parameter
		, ('searchBy') : searchBy
		, ('valueSearchBy') : valueSearchBy
		, ('locationOfAccident') : locationOfAccident
		, ('accidentInformation') : accidentInformation
		, ('causeOfLoss') : causeOfLoss
		, ('surveyType') : surveyType
		, ('dateSurveySchedule') : dateSurveySchedule
		, ('surveyCategoryTime') : surveyCategoryTime
		, ('customerOnLocation') : customerOnLocation
		, ('picPhoneNumber') : picPhoneNumber
		, ('surveyAddress') : surveyAddress
		, ('needTowing') : needTowing
		, ('registrationNumber') : registrationNumber
		, ('customerGOXC') : customerGOXC
		, ('vipRelatives') : vipRelatives
		, ('mobilePhone1') : mobilePhone1
		, ('location') : location
		, ('detailLocation') : detailLocation
		, ('destination') : destination
		, ('detailDestination') : detailDestination
		, ('requestCategory') : requestCategory
		, ('fuStatus') : fuStatus
		, ('fuRemarks') : fuRemarks
		, ('action') : action
		, ('createLk') : createLk
		, ('ContactName') : ContactName])

WebUI.callTestCase(findTestCase('Pages/Web/GardaAkses/Exit Confirmation/Exit Confirmation'), [('ECAction1') : ECAction1, ('ECAction2') : ECAction2
        , ('Comment') : Comment])

WebUI.comment(SearchBy + ' : ' + Parameter)

WebUI.comment(GlobalVariable.dataGasi)

//CustomKeywords.'querySQL.Query.QueryContactName'()