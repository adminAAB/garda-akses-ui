import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5 as GEN5
import com.keyword.UI as UI

GEN5.ProcessingCommand()

GEN5.ProcessingCommand()

if (Phase == '1') {
    WebUI.click(findTestObject('Pages/Web/GardaMedikaAkses/GL Inquiry/Button - New GL'))
} else {
    WebUI.setText(findTestObject('Pages/Web/GardaMedikaAkses/GL Inquiry/Input - Member No'), MemberNo)

    WebUI.setText(findTestObject('Pages/Web/GardaMedikaAkses/GL Inquiry/Input - Member Name'), MemberNo)

    WebUI.delay(GlobalVariable.Delay1)

    UI.DeleteWrite(findTestObject('Pages/Web/GardaMedikaAkses/GL Inquiry/Input - Member Name'), MemberNo)

    WebUI.click(findTestObject('Pages/Web/GardaMedikaAkses/GL Inquiry/List - Member Name', [('value') : MemberNo]))

    GEN5.ProcessingCommand()

    WebUI.click(findTestObject('Pages/Web/GardaMedikaAkses/GL Inquiry/Button - Search'))

    GEN5.ProcessingCommand()

    WebUI.click(findTestObject('Pages/Web/GardaMedikaAkses/GL Inquiry/Klick - Date'))

    GEN5.ProcessingCommand()

    WebUI.doubleClick(findTestObject('Pages/Web/GardaMedikaAkses/GL Inquiry/Select - Data', [('Value') : MemberNo]))
}

