import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys1
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.By as By
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.keyword.GEN5 as GEN5
import com.keyword.UI as UI

GEN5.ProcessingCommand()

GEN5.ProcessingCommand()

//Patient Information
//Member
if (Phase == '1') {
    WebUI.setText(findTestObject('Pages/Web/GardaMedikaAkses/Claim/Patient Information/Input - Member'), 
        Member)

    WebUI.delay(GlobalVariable.Delay1)

    WebUI.setText(findTestObject('Pages/Web/GardaMedikaAkses/Claim/Patient Information/Input - Member'), 
        Member)

    WebUI.click(findTestObject('Pages/Web/GardaMedikaAkses/Claim/Patient Information/Auto Complete - Member'))

    GEN5.ProcessingCommand()
}

//Patient / Family Phone No
WebUI.setText(findTestObject('Pages/Web/GardaMedikaAkses/Claim/Patient Information/Input - Patient Family Phone No'), 
    GlobalVariable.PhoneNumber)

//Product Type
WebUI.click(findTestObject('Pages/Web/GardaMedikaAkses/Claim/Patient Information/Button - Product Type'))

WebUI.waitForElementVisible(findTestObject('Pages/Web/GardaMedikaAkses/Claim/Patient Information/Choose - Product Type', 
        [('Value') : ProductType]), GlobalVariable.Delay1)

WebUI.click(findTestObject('Pages/Web/GardaMedikaAkses/Claim/Patient Information/Choose - Product Type', 
        [('Value') : ProductType]))

GEN5.ProcessingCommand()

def PopUpNoProduct = WebUI.waitForElementVisible(findTestObject('Pages/Web/GardaMedikaAkses/Claim/Patient Information/Pop Up - No Product'), 
    GlobalVariable.Delay1)

if (PopUpNoProduct) {
    if (NoProduct == 'Continue') {
        WebUI.click(findTestObject('Pages/Web/GardaMedikaAkses/Claim/Patient Information/Button - Continue No Product'))
    } else if (NoProduct == 'Close') {
        WebUI.click(findTestObject('Pages/Web/GardaMedikaAkses/Claim/Patient Information/Button - Close No Product'))
    }
}

GEN5.ProcessingCommand()

//GL Type
WebUI.click(findTestObject('Pages/Web/GardaMedikaAkses/Claim/Patient Information/Button - GL Type'))

WebUI.click(findTestObject('Pages/Web/GardaMedikaAkses/Claim/Patient Information/Choose - GL Type', 
        [('Value') : GLType]))

GEN5.ProcessingCommand()

//Treatment Period
if (EditTreatmentPeriod == 'Yes') {
    WebUI.click(findTestObject('Pages/Web/GardaMedikaAkses/Claim/Patient Information/Date Picker - Treatment Period Start'))

    GEN5.DatePicker(TreatmentPeriodStart, findTestObject('Pages/Web/GardaMedikaAkses/Claim/Patient Information/Date Picker - Treatment Period Start Month'))

    if (GLType == 'Akhir') {
        WebUI.click(findTestObject('Pages/Web/GardaMedikaAkses/Claim/Patient Information/Date Picker - Treatment Period End'))

        GEN5.DatePicker(TreatmentPeriodEnd, findTestObject('Pages/Web/GardaMedikaAkses/Claim/Patient Information/Date Picker - Treatment Period End Month'))
    }
}

//Treatment Duration
if (GLType == 'Akhir') {
    GEN5.DeleteWrite(findTestObject('Pages/Web/GardaMedikaAkses/Claim/Patient Information/Input - Treatment Duration'), 
        TreatmentDuration)
}

//Patient ODS atau ODC
if (IsODSODC == 'ODS') {
    WebUI.click(findTestObject('Pages/Web/GardaMedikaAkses/Claim/Patient Information/Check Box - ODS'))
}

if (IsODSODC == 'ODC') {
    WebUI.click(findTestObject('Pages/Web/GardaMedikaAkses/Claim/Patient Information/Check Box - ODC'))
}

//Provider Information
//Admin Name
WebUI.setText(findTestObject('Pages/Web/GardaMedikaAkses/Claim/Provider Information/Input - Admin Name'), 
    AdminName)

//Email
WebUI.clearText(findTestObject('Pages/Web/GardaMedikaAkses/Claim/Provider Information/Input - Provider Email'))

WebUI.setText(findTestObject('Pages/Web/GardaMedikaAkses/Claim/Provider Information/Input - Provider Email'), 
    Email)

//Ext
WebUI.clearText(findTestObject('Pages/Web/GardaMedikaAkses/Claim/Provider Information/Input - Provider Phone Ext'))

WebUI.setText(findTestObject('Pages/Web/GardaMedikaAkses/Claim/Provider Information/Input - Provider Phone Ext'), 
    ProviderPhoneExt)

//Diagnosis
//Diagnosis
//===New===
int RepeatDiagnosa = DiagnosisID.size()

for (i = 0; i < RepeatDiagnosa; i++) {
    if ((Diagnosis[i]) == 'New') {
        WebUI.click(findTestObject('Pages/Web/GardaMedikaAkses/Claim/Diagnosis/Button - New'))

        //Status
        WebUI.click(findTestObject('Pages/Web/GardaMedikaAkses/Claim/Diagnosis/Add Diagnosis/Combo - Status'))

        WebUI.click(findTestObject('Pages/Web/GardaMedikaAkses/Claim/Diagnosis/Add Diagnosis/Combo List - Status', 
                [('value') : StatusDiagnosa[i]]))

        //ID/Name (Kode / Nama Diagnosa)
        WebUI.setText(findTestObject('Pages/Web/GardaMedikaAkses/Claim/Diagnosis/Add Diagnosis/Input - ID Diagnosa'), 
            DiagnosisID[i])

        WebUI.delay(GlobalVariable.Delay1)

        WebUI.setText(findTestObject('Pages/Web/GardaMedikaAkses/Claim/Diagnosis/Add Diagnosis/Input - ID Diagnosa'), 
            DiagnosisID[i])

        WebUI.click(findTestObject('Pages/Web/GardaMedikaAkses/Claim/Diagnosis/Add Diagnosis/Auto Complete - ID or Name'))

        GEN5.ProcessingCommand()

        //Gravida
        if (ProductType == 'Maternity (Persalinan)') {
            WebUI.setText(findTestObject('Pages/Web/GardaMedikaAkses/Claim/Diagnosis/Add Diagnosis/Input - Gravida'), 
                Gravida)
        }
        
        //Partus
        if (ProductType == 'Maternity (Persalinan)') {
            WebUI.setText(findTestObject('Pages/Web/GardaMedikaAkses/Claim/Diagnosis/Add Diagnosis/Input - Partus'), 
                Partus)
        }
        
        //Abortus
        if (ProductType == 'Maternity (Persalinan)') {
            WebUI.setText(findTestObject('Pages/Web/GardaMedikaAkses/Claim/Diagnosis/Add Diagnosis/Input - Abortus'), 
                Abortus)
        }
        
        //Gestational Age
        if (ProductType == 'Maternity (Persalinan)') {
            WebUI.setText(findTestObject('Pages/Web/GardaMedikaAkses/Claim/Diagnosis/Add Diagnosis/Input - Gestational Age'), 
                GestationalAge)
        }
        
        WebUI.setText(findTestObject('Pages/Web/GardaMedikaAkses/Claim/Diagnosis/Add Diagnosis/Input - Remarks'), 
            RemarksDiagnosa)

        //Diagnosis Question - Additional Info
        WebDriver driver = DriverFactory.getWebDriver()

        WebUI.switchToFrame(findTestObject('Pages/Web/GEN5/Frame'), 1)

        WebElement diagnosis = driver.findElement(By.xpath('//*[@id="DiagnosisQuestion"]/div[2]/a2is-datatable/div[2]/div/table/tbody'))

        List<WebElement> additional_info = diagnosis.findElements(By.tagName('tr'))

        WebUI.switchToDefaultContent()

        int yes = additional_info.size()

        int i = 1

        for (a = 1; a <= yes; a++) {
            WebUI.click(findTestObject('Pages/Web/GardaMedikaAkses/Claim/Diagnosis/Add Diagnosis/Radio Button - Yes', 
                    [('Value') : a]))

            WebUI.delay(1)
        }
        
        WebUI.click(findTestObject('Pages/Web/GardaMedikaAkses/Claim/Diagnosis/Add Diagnosis/Button - Submit'))

        GEN5.ProcessingCommand()
    } else if ((Diagnosis[i]) == 'Edit') {
        WebUI.click(findTestObject('Pages/Web/GardaMedikaAkses/Claim/Diagnosis/Button - Edit'))
    } else if ((Diagnosis[i]) == 'Delete') {
        WebUI.click(findTestObject('Pages/Web/GardaMedikaAkses/Claim/Diagnosis/Button - Delete'))
    }
}

//Treatment Information
//Remarks Treatment Information
WebUI.setText(findTestObject('Pages/Web/GardaMedikaAkses/Claim/Treatment Information/Input - Remarks'), 
    RemarksTreatmentInformation)

//Maternity Treatment
if (ProductType == 'Maternity (Persalinan)') {
    WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Treatment Information/Combo - Maternity Treatment'))

    WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Treatment Information/Combo List - Maternity Treatment', 
            [('value') : MaternityTreatment]))
}

//Doctor
WebUI.setText(findTestObject('Pages/Web/GardaMedikaAkses/Claim/Treatment Information/Input - Doctor'), 
    DoctorName)

//Medical Treatment (Tindakan Medis)
//Room Information
//Treatment RB Class (Ruang Kelas Perawatan)
String getValueTreatmentRBClass = WebUI.getAttribute(findTestObject('Pages/Web/GardaMedikaAkses/Claim/Room Information/Input - Treatment RB Class - Value'), 
    'value')

if (getValueTreatmentRBClass != TreatmentRBClass) {
    WebUI.click(findTestObject('Pages/Web/GardaMedikaAkses/Claim/Room Information/Look Up - Treatment RB Class'))

    GEN5.ProcessingCommand()

    WebUI.click(findTestObject('Pages/Web/GardaMedikaAkses/Claim/Room Information/Treatment RB Class/Select - Treatment RB Class', 
            [('Value') : TreatmentRBClass]))

    WebUI.click(findTestObject('Pages/Web/GardaMedikaAkses/Claim/Room Information/Treatment RB Class/Button - Choose'))
}

//WebUI.setText(findTestObject('Pages/Web/GardaMedikaAkses/Claim/Room Information/Input - Appropriate RB Rate'),
//	TreatmentRBRate)
String getValueTreatmentRBRate = WebUI.getAttribute(findTestObject('Pages/Web/GardaMedikaAkses/Claim/Room Information/Input - Treatment RB Rate - Value'), 
    'value')

if (((IsODSODC == 'ODS') || 'ODC') && (getValueTreatmentRBRate == '')) {
    WebUI.setText(findTestObject('Pages/Web/GardaMedikaAkses/Claim/Room Information/Input - Appropriate RB Rate'), 
        TreatmentRBRate)
}

//Room Option / Availability (Ketersediaan Ruang Perawatan)
CustomKeywords.'gardaAkses.General.UpdateFieldCombo'(findTestObject('Pages/Web/GardaMedikaAkses/Claim/Room Information/Combo - Room Option Availability'), 
    findTestObject('Pages/Web/GardaMedikaAkses/Claim/Room Information/Combo List - Room Option Availability', 
        [('value') : RoomOptionAvailability]), RoomOptionAvailability)

//Package Price
if (ProductType == 'Maternity (Persalinan)') {
    WebUI.setText(findTestObject('Pages/Web/GardaMedikaAkses/Claim/Room Information/Input - Package Price'), 
        PackagePrice)
}

//Total Billed
if (GLType == 'Akhir') {
    WebUI.setText(findTestObject('Pages/Web/GardaMedikaAkses/Claim/Room Information/Input - Total Billed'), 
        TotalBilled)
}

//========== Non Medical Item ===============
if (NonMedicalItem == 'Yes') {
    WebUI.click(findTestObject('Pages/Web/GardaMedikaAkses/Claim/Non Medical Item/CheckBox - Non Medical Item'))
}

//Document Management
//Action
if (Action == 'Process') {
    WebUI.click(findTestObject('Pages/Web/GardaMedikaAkses/Claim/Button - Process'))

    GEN5.ProcessingCommand()
	
	//Pop Pasien Rujuk
	def PopUpRujuk = WebUI.waitForElementVisible(findTestObject('Object Repository/Pages/Web/GardaMedikaAkses/Claim/Pop Up Pasien Rujuk/Text - Pop Up Pasien Rujuk'), GlobalVariable.Delay1)
	
	if (PopUpRujuk) {
		WebUI.click(findTestObject('Object Repository/Pages/Web/GardaMedikaAkses/Claim/Pop Up Pasien Rujuk/Button - Tidak'))
	}
	
	GEN5.ProcessingCommand()
	
	def Repeat = Validasi.size()

    for (i = 0; i < Repeat; i++) {
        WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Text - Validasi', 
                [('Value') : Validasi[i]]))
    }
    
    WebUI.click(findTestObject('Pages/Web/GardaMedikaAkses/Claim/Summary/Button - Close'))
}