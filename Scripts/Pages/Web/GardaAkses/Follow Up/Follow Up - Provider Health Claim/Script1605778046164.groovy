import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import org.junit.After as After
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.DBData as DBData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5 as GEN5
import com.keyword.UI as UI
import gardaAkses.HandleTicketFU as FU

GEN5.ProcessingCommand()

boolean PopUpInformation = WebUI.waitForElementVisible(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Pop Up - Information'), 
    GlobalVariable.Delay1)

while (PopUpInformation) {
    GEN5.ProcessingCommand()

    WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Button - Dismiss Pop Up Information'))

    PopUpInformation = WebUI.waitForElementVisible(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Pop Up - Information'), 
        GlobalVariable.Delay1)
}

FU.FollowUp(PhaseFU, PhaseGL, UserID, Password, Menu, SubMenuFU)

//=== Guarantee Letter ===
boolean ButtonVerifyGL = WebUI.waitForElementVisible(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Guarantee Letter/Button - Verify'), 
    GlobalVariable.Delay1, FailureHandling.OPTIONAL)

if (!ButtonVerifyGL) {
    boolean PopUpInformation2 = WebUI.waitForElementVisible(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Pop Up - Information'), 
        GlobalVariable.Delay1)

    while (PopUpInformation2) {
        GEN5.ProcessingCommand()

        WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Button - Dismiss Pop Up Information'))

        PopUpInformation2 = WebUI.waitForElementVisible(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Pop Up - Information'), 
            GlobalVariable.Delay1)
    }
    
    WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Menu - Guarantee Letter'))
}

//Patient Information
boolean TextPatientInformation = WebUI.waitForElementVisible(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Guarantee Letter/Patient Information/Text - Patient Information'),
	GlobalVariable.Delay1)

while (!TextPatientInformation) {
	GEN5.ProcessingCommand()

	WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Guarantee Letter/Text - Guarantee Letter'))

	TextPatientInformation = WebUI.waitForElementVisible(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Guarantee Letter/Patient Information/Text - Patient Information'),
	GlobalVariable.Delay1)
}

//Member
String MemberNameExist = WebUI.getAttribute(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Guarantee Letter/Patient Information/Input - Member'), 
    'value')

WebUI.verifyEqual(MemberName.replace(' ', ''), MemberNameExist.replace(' ', ''))

//Patient / Family Phone No
String PatientFamilyPhoneNoExist = WebUI.getAttribute(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Guarantee Letter/Patient Information/Input - Patient Family Phone No'), 
    'value')

WebUI.verifyEqual(GlobalVariable.PhoneNumber, PatientFamilyPhoneNoExist)

////Product Type
//String ProductTypeExist = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Guarantee Letter/Patient Information/Combo - Product Type'))
//
//WebUI.verifyEqual(ProductType, ProductTypeExist)

//GL Type
String GLTypeExist = WebUI.getText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Guarantee Letter/Patient Information/Combo - GL Type'))

WebUI.verifyEqual(GLType, GLTypeExist)

//Provider
String ProviderExist = WebUI.getAttribute(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Guarantee Letter/Provider Information/Input - Provider'), 
    'value')

WebUI.verifyEqual(Provider, ProviderExist)

//Email
String EmailExist = WebUI.getAttribute(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Guarantee Letter/Provider Information/Input - Email'), 
    'value')

WebUI.verifyEqual(GlobalVariable.Email, EmailExist)

//Phone
String PhoneExist = WebUI.getAttribute(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Guarantee Letter/Provider Information/Input - Phone'), 
    'value')

WebUI.verifyEqual(GlobalVariable.PhoneNumber, PhoneExist)

//Action
WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Guarantee Letter/Button - Verify'))

GEN5.ProcessingCommand()


//Product=====Product======Product=====Product======Product=====Product======Product=====Product======Product=====Product======Product=====Product======Product=====Product======Product=====Product======Product=====Product======Product=====Product======Product=====Product======

if (Validasi.contains(GlobalVariable.ValidasiCoverageCovMA) || Validasi.contains(GlobalVariable.ValidasiProductIP)) {
	def PICProductSize = PICProduct.size()

	for (i = 0; i < PICProductSize; i++) {
		if ((ProductConfirmation[i]) == 'New') {
			WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Product Confirmation/Button - New Product Confirmation'))

			GEN5.ProcessingCommand()

			WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Product Confirmation/Add Confirmation/Combo - PIC'))

			WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Product Confirmation/Add Confirmation/Combo List - PIC',
					[('value') : PICProduct[i]]))

			GEN5.ProcessingCommand()

			if ((PICProduct[i]) == 'Account Manager') {
				//Account Manager Name
				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Product Confirmation/Add Confirmation/Account Manager/Drop Down - Account Manager Name'))

				WebUI.setText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Product Confirmation/Add Confirmation/Account Manager/Input - Account Manager Name'),
					AccountManagerNameProduct)

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Product Confirmation/Add Confirmation/Account Manager/Select - Account Manager Name',
						[('value') : AccountManagerNameProduct]))

				//Date Time Confirmation
				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Product Confirmation/Add Confirmation/Account Manager/Select - Date Time Confirmation'))

				//Confirmation
				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Product Confirmation/Add Confirmation/Account Manager/Drop Down - Confirmation'))

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Product Confirmation/Add Confirmation/Account Manager/Select - Confirmation',
						[('value') : ConfirmationProduct[i]]))

				//Channel
				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Product Confirmation/Add Confirmation/Account Manager/Drop Down - Channel'))

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Product Confirmation/Add Confirmation/Account Manager/Select - Channel',
						[('value') : ChannelProduct]))
				

				//Benefit Coverage
				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Product Confirmation/Add Confirmation/Account Manager/Drop Down - Benefit Coverage'))

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Product Confirmation/Add Confirmation/Account Manager/Select - Benefit Coverage',
						[('value') : BenefitCoverageProduct]))

				//Remarks
				WebUI.setText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Product Confirmation/Add Confirmation/Account Manager/Input - Remarks'),
					RemarksFUProduct)

				//Action Diagnosa
				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Product Confirmation/Add Confirmation/Account Manager/Button - Action',
						[('value') : ActionFUProduct]))

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Product Confirmation/Add Confirmation/Account Manager/Button - OK'))
				
			} else if ((PICProduct[i]) == 'Provider') {
				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Product Confirmation/Add Confirmation/Provider/Combo - Confirmation'))

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Product Confirmation/Add Confirmation/Provider/Combo List - Confirmation',
						[('value') : ConfirmationProduct[i]]))

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Product Confirmation/Add Confirmation/Provider/Combo - Channel'))

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Product Confirmation/Add Confirmation/Provider/Combo List - Channel',
						[('value') : ChannelProduct]))
				
				//Reason
				if (ConfirmationProduct[i] == 'Hold') {
					
					WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Product Confirmation/Add Confirmation/Provider/Combo - Reason'))
					
					WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Product Confirmation/Add Confirmation/Provider/Combo List - Reason',
						[('value') : ReasonConfirmation]))
					
				}
				
				if ((ConfirmationProduct[i] == 'Hold') || (ConfirmationProduct[i] == 'Provider not available')) {
										
					WebUI.setText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Product Confirmation/Add Confirmation/Provider/Input - Follow Up Time'), ProviderFUTime)
				}
				

				WebUI.scrollToElement(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Product Confirmation/Add Confirmation/Button - Save'),
					2)

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Product Confirmation/Add Confirmation/Button - Save'))

				WebUI.waitForElementVisible(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Product Confirmation/Add Confirmation/Text - Insert Successfully'),
					1)

				GEN5.ProcessingCommand()

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Product Confirmation/Button - OK'))
			} else if ((PICProduct[i]) == 'Contact Center') {
				WebUI.click(findTestObject('null'))

				WebUI.setText(findTestObject('null'),
					UserID)

				WebUI.click(findTestObject('null',
						[('value') : UserID]))

				WebUI.click(findTestObject('null'))

				WebUI.click(findTestObject('null'))

				WebUI.click(findTestObject('null',
						[('value') : ConfirmationProduct[i]]))

				WebUI.click(findTestObject('null'))

				WebUI.click(findTestObject('null',
						[('value') : BenefitCoverageProduct]))

				WebUI.click(findTestObject('null'))

				GEN5.ProcessingCommand()

				WebUI.click(findTestObject('null'))
			} else if ((PICProduct[i]) == 'Doctor') {
				WebUI.click(findTestObject('null'))

				WebUI.click(findTestObject('null',
						[('value') : DoctorNameProduct]))

				WebUI.click(findTestObject('null'))

				WebUI.click(findTestObject('null',
						[('value') : ConfirmationProduct[i]]))

				WebUI.click(findTestObject('null'))

				WebUI.click(findTestObject('null',
						[('value') : ChannelProduct]))

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up - Provider Health Claim - Not Use/Follow Up Outbound/Product Confirmation/Add Confirmation/Doctor/Combo - Benefit Coverage'))

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up - Provider Health Claim - Not Use/Follow Up Outbound/Product Confirmation/Add Confirmation/Doctor/Combo List - Benefit Coverage',
						[('value') : BenefitCoverageProduct]))

				if (Remarks == 'Yes') {
					WebUI.setText(findTestObject('null'),
						RemarksValue)
				}
				
				WebUI.click(findTestObject('null'))

				GEN5.ProcessingCommand()

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Product Confirmation/Button - OK'))
			} else if ((PICProduct[i]) == 'Head Contact Center') {
				//Head CCO
				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Product Confirmation/Add Confirmation/Head CCO/Combo - Head CCO'))

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Product Confirmation/Add Confirmation/Head CCO/Combo List - Head CCO',
						[('value') : HeadCCOProduct]))

//                if (EditDateTimeConfirmation == 'Yes') {
//                    WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Head CCO/Date Picker - Date Time Confirmation'))
//
//                    GEN5.DatePicker(DTC, null)
//                }
				
				//Confirmation
				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Product Confirmation/Add Confirmation/Head CCO/Combo - Confirmation'))
				
				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Product Confirmation/Add Confirmation/Head CCO/Combo List - Confirmation',
					[('value') : ConfirmationProduct[i]]))
				
				//Channel
				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Product Confirmation/Add Confirmation/Head CCO/Combo - Channel'))
				
				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Product Confirmation/Add Confirmation/Head CCO/Combo List - Channel',
					[('value') : ChannelProduct]))
				
				//Benefit Coverage
				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Product Confirmation/Add Confirmation/Head CCO/Combo - Benefit Coverage'))
				
				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Product Confirmation/Add Confirmation/Head CCO/Combo List - Benefit Coverage',
					[('value') : BenefitCoverageProduct]))
				
				//Action Diagnosa
				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Product Confirmation/Add Confirmation/Head CCO/Button - Save'))
				
				GEN5.ProcessingCommand()
			}
		} else if (ProductConfirmation == 'Edit') {
		} else if (ProductConfirmation == 'Delete') {
		}
	}
}

//Product=====Product======Product=====Product======Product=====Product======Product=====Product======Product=====Product======Product=====Product======Product=====Product======Product=====Product======Product=====Product======Product=====Product======Product=====Product======

//Diagnosis Confirmation
if (Validasi.contains(GlobalVariable.ValidasiDiagnosa)) {
    def PICDiagnosisSize = PICDiagnosis.size()

    for (i = 0; i < PICDiagnosisSize; i++) {
        if ((DiagnosisConfirmation[i]) == 'New') {
            WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Button - New Diagnosis Confirmation'))

            GEN5.ProcessingCommand()

            //PIC
            WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Combo - PIC'))

            WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Combo List - PIC', 
                    [('value') : PICDiagnosis[i]]))

            GEN5.ProcessingCommand()

            if ((PICDiagnosis[i]) == 'Account Manager') {
                //Account Manager Name
                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Account Manager/Drop Down - Account Manager Name'))

                WebUI.setText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Account Manager/Input - Account Manager Name'), 
                    AccountManagerNameDiagnosis[i])

                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Account Manager/Select - Account Manager Name', 
                        [('value') : AccountManagerNameDiagnosis[i]]))

                //Date Time Confirmation
                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Account Manager/Select - Date Time Confirmation'))

                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Account Manager/Select - Date Time Confirmation'))

                //Confirmation
                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Account Manager/Drop Down - Confirmation'))

                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Account Manager/Select - Confirmation', 
                        [('value') : ConfirmationDiagnosis[i]]))

                //Channel
                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Account Manager/Drop Down - Channel'))

                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Account Manager/Select - Channel', 
                        [('value') : ChannelDiagnosis]))

                //Benefit Coverage
                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Account Manager/Drop Down - Benefit Coverage'))

                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Account Manager/Select - Benefit Coverage', 
                        [('value') : BenefitCoverageDiagnosis[i]]))

                //Remarks
                WebUI.setText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Account Manager/Input - Remarks'), 
                    RemarksFUDiagnosis)

                //Action Diagnosa
                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Account Manager/Button - Action', 
                        [('value') : ActionFUDiagnosis]))

                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Account Manager/Button - OK' //Head CCO
                        ))
            } else if ((PICDiagnosis[i]) == 'Provider') {
                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Provider/Combo - Confirmation'))

                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Provider/Combo List - Confirmation', 
                        [('value') : ConfirmationDiagnosis[i]]))

                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Provider/Combo - Channel'))

                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Provider/Combo List - Channel', 
                        [('value') : ChannelDiagnosis]))

                WebUI.scrollToElement(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Button - Save'), 
                    2)

                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Button - Save'))

                WebUI.waitForElementVisible(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Text - Insert Successfully'), 
                    1)

                GEN5.ProcessingCommand()

                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Button - OK'))
            } else if ((PICDiagnosis[i]) == 'Contact Center') {
                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Contact Center/Combo List - Contact Center Name'))

                WebUI.setText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Contact Center/Input - Contact Center Name'), 
                    UserID)

                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Contact Center/List - Contact Center Name', 
                        [('value') : UserID]))

                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Contact Center/Date Picker - Date Time Confirmation'))

                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Contact Center/Combo List - Confirmation'))

                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Contact Center/List - Confirmation', 
                        [('value') : ConfirmationDiagnosis[i]]))

                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Contact Center/Combo List - Benefit Coverage'))

                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Contact Center/List - Benefit Coverage', 
                        [('value') : BenefitCoverageDiagnosis]))

                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Contact Center/Button - Save'))

                GEN5.ProcessingCommand()

                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Contact Center/Button - OK'))
            } else if ((PICDiagnosis[i]) == 'Doctor') {
                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Doctor/Combo - Doctor Name'))

                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Doctor/Combo List - Doctor Name', 
                        [('value') : DoctorNameDiagnosis]))

                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Doctor/Combo - Confirmation'))

                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Doctor/Combo List - Confirmation', 
                        [('value') : ConfirmationDiagnosis[i]]))

                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Doctor/Combo - Channel'))

                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Doctor/Combo List - Channel', 
                        [('value') : ChannelDiagnosis]))

                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up - Provider Health Claim - Not Use/Follow Up Outbound/Diagnosis Confirmation/Add Confirmation/Doctor/Combo - Benefit Coverage'))

                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up - Provider Health Claim - Not Use/Follow Up Outbound/Diagnosis Confirmation/Add Confirmation/Doctor/Combo List - Benefit Coverage', 
                        [('value') : BenefitCoverageDiagnosis]))

                if (Remarks == 'Yes') {
                    WebUI.setText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Doctor/Input - Remarks'), 
                        RemarksValue)
                }
                
                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Doctor/Button - Save'))

                GEN5.ProcessingCommand()

                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Button - OK'))
            } else if ((PICDiagnosis[i]) == 'Head Contact Center') {
                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Head CCO/Combo - Head CCO'))

                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Head CCO/Combo List - Head CCO', 
                        [('value') : HeadCCODiagnosis]))

                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Head CCO/Combo - Confirmation'))

                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Head CCO/Combo List - Confirmation', 
                        [('value') : ConfirmationDiagnosis[i]]))

                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Head CCO/Combo - Channel'))

                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Head CCO/Combo List - Channel', 
                        [('value') : ChannelDiagnosis]))

                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Head CCO/Combo - Benefit Coverage'))

                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Head CCO/Combo List - Benefit Coverage', 
                        [('value') : BenefitCoverageDiagnosis]))

                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Head CCO/Button - Save'))

                GEN5.ProcessingCommand()
            }
        } else if ((DiagnosisConfirmation[i]) == 'Edit') {
        } else if ((DiagnosisConfirmation[i]) == 'Delete') {
        }
    }
}

//Documents Confirmation
if (Validasi.contains(GlobalVariable.ValidasiDocument)) {
    def PICDocumentsSize = PICDocuments.size()

    for (i = 0; i < PICDocumentsSize; i++) {
        if ((DocumentConfirmation[i]) == 'New') {
            //Document Confirmation
            WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Documents Confirmation/Button - New Documents Confirmation'))

            GEN5.ProcessingCommand()

            //PIC
            WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Documents Confirmation/Add Confirmation/Combo - PIC'))

            WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Documents Confirmation/Add Confirmation/Combo List - PIC', 
                    [('value') : PICDocuments[i]]))

            GEN5.ProcessingCommand()

            if ((PICDocuments[i]) == 'AM') {
                //Account Manager Name
                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Documents Confirmation/Add Confirmation/Account Manager/Combo - Account Manager Name'))

                WebUI.setText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Documents Confirmation/Add Confirmation/Account Manager/Input - Account Manager Name'), 
                    AccountManagerNameDocuments[i])

                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Documents Confirmation/Add Confirmation/Account Manager/Combo List - Account Manager Name', 
                        [('value') : AccountManagerNameDocuments[i]]))

                //Confirmation
                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Documents Confirmation/Add Confirmation/Account Manager/Combo - Confirmation'))

                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Documents Confirmation/Add Confirmation/Account Manager/Combo List - Confirmation', 
                        [('value') : ConfirmationDocuments[i]]))

                //Channel
                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Documents Confirmation/Add Confirmation/Account Manager/Combo - Channel'))

                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Documents Confirmation/Add Confirmation/Account Manager/Combo List - Channel', 
                        [('value') : ChannelDocuments]))

                //Benefit Coverage
                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Documents Confirmation/Add Confirmation/Account Manager/Combo - Benefit Coverage'))

                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Documents Confirmation/Add Confirmation/Account Manager/Combo List - Benefit Coverage', 
                        [('value') : BenefitCoverageDocuments[i]]))

                //Remarks
                WebUI.setText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Documents Confirmation/Add Confirmation/Account Manager/Input - Remarks'), 
                    RemarksDocuments)

                //Action
                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Documents Confirmation/Add Confirmation/Account Manager/Button - Action', 
                        [('value') : ActionDocuments]))

                GEN5.ProcessingCommand()

                //Information
                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Documents Confirmation/Add Confirmation/Account Manager/Button - OK'))
            } else if ((PICDocuments[i]) == 'Provider') {
                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Documents Confirmation/Add Confirmation/Provider/Combo - Confirmation'))

                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Documents Confirmation/Add Confirmation/Provider/Combo List - Confirmation', 
                        [('value') : ConfirmationDocuments]))

                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Documents Confirmation/Add Confirmation/Provider/Combo - Channel'))

                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Documents Confirmation/Add Confirmation/Provider/Combo List - Channel', 
                        [('value') : ChannelDocuments]))

                WebUI.setText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Documents Confirmation/Add Confirmation/Provider/Input - Remarks'), 
                    Remarks)

                if ((UploadDocuments[i]) == 'New') {
                    WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Documents Confirmation/Add Confirmation/Provider/Button - Document Create New'))

                    WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Documents Confirmation/Add Confirmation/Provider/Upload Document/Combo - Document Type'))

                    WebUI.setText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Documents Confirmation/Add Confirmation/Provider/Upload Document/Input - Documents Type'), 
                        DocumentsType[i])

                    WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Documents Confirmation/Add Confirmation/Provider/Upload Document/Combo List - Documents Type', 
                            [('value') : DocumentsType[i]]))

                    WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Documents Confirmation/Add Confirmation/Provider/Upload Document/Combo - Documents Validity'))

                    WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Documents Confirmation/Add Confirmation/Provider/Upload Document/Combo List - Documents Validity', 
                            [('value') : DocumentsValidity[i]]))

                    WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Documents Confirmation/Add Confirmation/Provider/Upload Document/Button - Browse'))

                    UI.UploadFile2('')

                    WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Documents Confirmation/Add Confirmation/Provider/Upload Document/Button - Upload'))

                    GEN5.ProcessingCommand()

                    WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Documents Confirmation/Add Confirmation/Provider/Upload Document/Button - Save'))
                } else if ((UploadDocuments[i]) == 'Edit') {
                } else if ((UploadDocuments[i]) == 'Delete') {
                }
                
                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Documents Confirmation/Add Confirmation/Provider/Button - Save'))

                GEN5.ProcessingCommand()

                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Documents Confirmation/Add Confirmation/Button - OK'))
            }
        } else if ((DocumentConfirmation[i]) == 'Edit') {
        } else if ((DocumentConfirmation[i]) == 'Delete') {
        }
    }
}

//Excess Calculation Confirmation
if (Validasi.contains(GlobalVariable.ValidasiDocument)) {
    def PICExcessCalculationSize = PICExcessCalculation.size()

    for (i = 0; i < PICExcessCalculationSize; i++) {
        if ((ExcessCalculationConfirmation[i]) == 'New') {
            //Excess Calculation Confirmation
            WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Excess Calculation Confirmation/Button - New Excess Calculation Confirmation'))

            GEN5.ProcessingCommand()

            //PIC
            WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Excess Calculation Confirmation/Add Confirmation/Combo - PIC'))

            WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Excess Calculation Confirmation/Add Confirmation/Combo List - PIC', 
                    [('value') : PICExcessCalculation[i]]))

            GEN5.ProcessingCommand()

            if ((PICExcessCalculation[i]) == 'Account Manager') {
                //Account Manager Name
                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Excess Calculation Confirmation/Add Confirmation/Account Manager/Combo - Account Manager Name'))

                WebUI.setText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Excess Calculation Confirmation/Add Confirmation/Account Manager/Input - Account Manager Name'), 
                    AccountManagerNameExcessCalculation[i])

                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Excess Calculation Confirmation/Add Confirmation/Account Manager/Combo List - Account Manager Name', 
                        [('value') : AccountManagerNameExcessCalculation[i]]))

                //Channel
                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Excess Calculation Confirmation/Add Confirmation/Account Manager/Combo - Channel'))

                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Excess Calculation Confirmation/Add Confirmation/Account Manager/Combo List - Channel', 
                        [('value') : ChannelExcessCalculation]))

                //Confirmation
                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Excess Calculation Confirmation/Add Confirmation/Account Manager/Combo - Confirmation'))

                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Excess Calculation Confirmation/Add Confirmation/Account Manager/Combo List - Confirmation', 
                        [('value') : ConfirmationExcessCalculation[i]]))

                //Remarks
                WebUI.setText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Excess Calculation Confirmation/Add Confirmation/Account Manager/Input - Remarks'), 
                    RemarksExcessCalculation)

                //Action
                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Excess Calculation Confirmation/Add Confirmation/Account Manager/Button - Action', 
                        [('value') : ActionExcessCalculation]))

                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Excess Calculation Confirmation/Button - OK')) //Action
                //Action
            } else if ((PICDocuments[i]) == 'Contact Center') {
                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Excess Calculation Confirmation/Add Confirmation/Contact Center/Combo - Confirmation'))

                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Excess Calculation Confirmation/Add Confirmation/Contact Center/Combo List - Confirmation', 
                        [('value') : ConfirmationExcessCalculation[i]]))

                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Excess Calculation Confirmation/Add Confirmation/Contact Center/Combo - Benefit Coverage'))

                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Excess Calculation Confirmation/Add Confirmation/Contact Center/Combo List - Benefit Coverage', 
                        [('value') : BenefitCoverageExcessCalculation[i]]))

                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Excess Calculation Confirmation/Add Confirmation/Account Manager/Button - Action', 
                        [('value') : ActionExcessCalculation]))

                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Excess Calculation Confirmation/Button - OK'))
            } else if ((PICDocuments[i]) == 'Provider') {
                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Excess Calculation Confirmation/Add Confirmation/Provider/Combo - Confirmation'))

                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Excess Calculation Confirmation/Add Confirmation/Provider/Combo List - Confirmation', 
                        [('value') : ConfirmationExcessCalculation[i]]))

                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Excess Calculation Confirmation/Add Confirmation/Provider/Combo - Channel'))

                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Excess Calculation Confirmation/Add Confirmation/Provider/Combo List - Channel', 
                        [('value') : ChannelExcessCalculation[i]]))

                WebUI.setText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Excess Calculation Confirmation/Add Confirmation/Provider/Input - Remarks'), 
                    Remarks)

                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Excess Calculation Confirmation/Add Confirmation/Account Manager/Button - Action', 
                        [('value') : ActionExcessCalculation]))

                WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Excess Calculation Confirmation/Button - OK'))
            }
        } else if ((DocumentConfirmation[i]) == 'Edit') {
        } else if ((DocumentConfirmation[i]) == 'Delete') {
        }
    }
}
//Family Planning=========Family Planning=========Family Planning=========Family Planning==================Family Planning======================Family Planning=================Family Planning=================Family Planning================Family Planning=============================Family Planning=====================Family Planning

if (Validasi.contains(GlobalVariable.ValidasiFamilyPlanning)) {
	def PICFamilyPlanningSize = PICFamilyPlanning.size()

	for (i = 0; i < PICFamilyPlanningSize; i++) {
		if ((FamilyPlanningConfirmation[i]) == 'New') {
			WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Button - New Family Planning Confirmation'))

			GEN5.ProcessingCommand()

			WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Combo - PIC'))

			WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Combo List - PIC',
					[('value') : PICFamilyPlanning[i]]))

			GEN5.ProcessingCommand()

			if ((PICFamilyPlanning[i]) == 'Account Manager') {
				//Account Manager Name
				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Account Manager/Drop Down - Account Manager Name'))

				WebUI.setText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Account Manager/Input - Account Manager Name'),
					AccountManagerNameFamilyPlanning)

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Account Manager/Select - Account Manager Name',
						[('value') : AccountManagerNameFamilyPlanning]))

				//Date Time Confirmation
				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Account Manager/Select - Date Time Confirmation'))

				//Confirmation
				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Account Manager/Drop Down - Confirmation'))

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Account Manager/Select - Confirmation',
						[('value') : ConfirmationFamilyPlanning[i]]))

				//Channel
				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Account Manager/Drop Down - Channel'))

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Account Manager/Select - Channel',
						[('value') : ChannelFamilyPlanning]))

				//Benefit Coverage
				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Account Manager/Drop Down - Benefit Coverage'))

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Account Manager/Select - Benefit Coverage',
						[('value') : BenefitCoverageFamilyPlanning]))

				//Remarks
				WebUI.setText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Account Manager/Input - Remarks'),
					RemarksFUFamilyPlanning)

				//Action Diagnosa
				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Account Manager/Button - Action',
						[('value') : ActionFUFamilyPlanning]))

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Account Manager/Button - OK'))
				
			} else if ((PICFamilyPlanning[i]) == 'Provider') {
				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Provider/Combo - Confirmation'))

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Provider/Combo List - Confirmation',
						[('value') : ConfirmationFamilyPlanning[i]]))

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Provider/Combo - Channel'))

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Provider/Combo List - Channel',
						[('value') : ChannelFamilyPlanning]))

				WebUI.scrollToElement(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Button - Save'),
					2)

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Button - Save'))

				WebUI.waitForElementVisible(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Text - Insert Successfully'),
					1)

				GEN5.ProcessingCommand()

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Button - OK'))
			} else if ((PICFamilyPlanning[i]) == 'Contact Center') {
				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Contact Center/Combo List - Contact Center Name'))

				WebUI.setText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Contact Center/Input - Contact Center Name'),
					UserID)

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Contact Center/List - Contact Center Name',
						[('value') : UserID]))

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Contact Center/Date Picker - Date Time Confirmation'))

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Contact Center/Combo List - Confirmation'))

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Contact Center/List - Confirmation',
						[('value') : ConfirmationFamilyPlanning[i]]))

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Contact Center/Combo List - Benefit Coverage'))

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Contact Center/List - Benefit Coverage',
						[('value') : BenefitCoverageFamilyPlanning]))

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Contact Center/Button - Save'))

				GEN5.ProcessingCommand()

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Contact Center/Button - OK'))
			} else if ((PICFamilyPlanning[i]) == 'Doctor') {
				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Doctor/Combo - Doctor Name'))

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Doctor/Combo List - Doctor Name',
						[('value') : DoctorNameFamilyPlanning]))

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Doctor/Combo - Confirmation'))

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Doctor/Combo List - Confirmation',
						[('value') : ConfirmationFamilyPlanning[i]]))

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Doctor/Combo - Channel'))

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Doctor/Combo List - Channel',
						[('value') : ChannelFamilyPlanning]))

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up - Provider Health Claim - Not Use/Follow Up Outbound/Family Planning Confirmation/Add Confirmation/Doctor/Combo - Benefit Coverage'))

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up - Provider Health Claim - Not Use/Follow Up Outbound/Family Planning Confirmation/Add Confirmation/Doctor/Combo List - Benefit Coverage',
						[('value') : BenefitCoverageFamilyPlanning]))

				if (Remarks == 'Yes') {
					WebUI.setText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Doctor/Input - Remarks'),
						RemarksValue)
				}
				
				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Doctor/Button - Save'))

				GEN5.ProcessingCommand()

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Button - OK'))
			} else if ((PICFamilyPlanning[i]) == 'Head Contact Center') {
				//Head CCO
				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Head CCO/Combo - Head CCO'))

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Head CCO/Combo List - Head CCO',
						[('value') : HeadCCOFamilyPlanning]))

//                if (EditDateTimeConfirmation == 'Yes') {
//                    WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Head CCO/Date Picker - Date Time Confirmation'))
//
//                    GEN5.DatePicker(DTC, null)
//                }
				
				//Confirmation
				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Head CCO/Combo - Confirmation'))
				
				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Head CCO/Combo List - Confirmation',
					[('value') : ConfirmationFamilyPlanning[i]]))
				
				//Channel
				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Head CCO/Combo - Channel'))
				
				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Head CCO/Combo List - Channel',
					[('value') : ChannelFamilyPlanning]))
				
				//Benefit Coverage
				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Head CCO/Combo - Benefit Coverage'))
				
				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Head CCO/Combo List - Benefit Coverage',
					[('value') : BenefitCoverageFamilyPlanning]))
				
				//Action Diagnosa
				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Head CCO/Button - Save'))
				
				GEN5.ProcessingCommand()
			}
		} else if (FamilyPlanningConfirmation == 'Edit') {
		} else if (FamilyPlanningConfirmation == 'Delete') {
		}
	}
}

//Family Planning=============Family Planning==============Family Planning==============Family Planning==============Family Planning==============Family Planning==============Family Planning============Family Planning==============Family Planning==============Family Planning

//Family Planning Item
if (Validasi.contains(GlobalVariable.ValidasiFamilyPlanning)) {
	def PICFamilyPlanningSize = PICFamilyPlanning.size()

	for (i = 0; i < PICFamilyPlanningSize; i++) {
		if ((FamilyPlanningConfirmation[i]) == 'New') {
			WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Button - New Family Planning Confirmation'))

			GEN5.ProcessingCommand()

			WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Combo - PIC'))

			WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Combo List - PIC',
					[('value') : PICFamilyPlanning[i]]))

			GEN5.ProcessingCommand()

			if ((PICFamilyPlanning[i]) == 'Account Manager') {
				//Account Manager Name
				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Account Manager/Drop Down - Account Manager Name'))

				WebUI.setText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Account Manager/Input - Account Manager Name'),
					AccountManagerNameFamilyPlanning)

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Account Manager/Select - Account Manager Name',
						[('value') : AccountManagerNameFamilyPlanning]))

				//Date Time Confirmation
				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Account Manager/Select - Date Time Confirmation'))

				//Confirmation
				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Account Manager/Drop Down - Confirmation'))

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Account Manager/Select - Confirmation',
						[('value') : ConfirmationFamilyPlanning[i]]))

				//Channel
				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Account Manager/Drop Down - Channel'))

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Account Manager/Select - Channel',
						[('value') : ChannelFamilyPlanning]))

				//Benefit Coverage
				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Account Manager/Drop Down - Benefit Coverage'))

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Account Manager/Select - Benefit Coverage',
						[('value') : BenefitCoverageFamilyPlanning]))

				//Remarks
				WebUI.setText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Account Manager/Input - Remarks'),
					RemarksFUFamilyPlanning)

				//Action Diagnosa
				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Account Manager/Button - Action',
						[('value') : ActionFUFamilyPlanning]))

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Account Manager/Button - OK'))
				
			} else if ((PICFamilyPlanning[i]) == 'Provider') {
				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Provider/Combo - Confirmation'))

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Provider/Combo List - Confirmation',
						[('value') : ConfirmationFamilyPlanning[i]]))

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Provider/Combo - Channel'))

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Provider/Combo List - Channel',
						[('value') : ChannelFamilyPlanning]))

				WebUI.scrollToElement(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Button - Save'),
					2)

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Button - Save'))

				WebUI.waitForElementVisible(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Text - Insert Successfully'),
					1)

				GEN5.ProcessingCommand()

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Button - OK'))
			} else if ((PICFamilyPlanning[i]) == 'Contact Center') {
				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Contact Center/Combo List - Contact Center Name'))

				WebUI.setText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Contact Center/Input - Contact Center Name'),
					UserID)

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Contact Center/List - Contact Center Name',
						[('value') : UserID]))

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Contact Center/Date Picker - Date Time Confirmation'))

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Contact Center/Combo List - Confirmation'))

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Contact Center/List - Confirmation',
						[('value') : ConfirmationFamilyPlanning[i]]))

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Contact Center/Combo List - Benefit Coverage'))

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Contact Center/List - Benefit Coverage',
						[('value') : BenefitCoverageFamilyPlanning]))

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Contact Center/Button - Save'))

				GEN5.ProcessingCommand()

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Contact Center/Button - OK'))
			} else if ((PICFamilyPlanning[i]) == 'Doctor') {
				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Doctor/Combo - Doctor Name'))

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Doctor/Combo List - Doctor Name',
						[('value') : DoctorNameFamilyPlanning]))

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Doctor/Combo - Confirmation'))

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Doctor/Combo List - Confirmation',
						[('value') : ConfirmationFamilyPlanning[i]]))

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Doctor/Combo - Channel'))

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Doctor/Combo List - Channel',
						[('value') : ChannelFamilyPlanning]))

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up - Provider Health Claim - Not Use/Follow Up Outbound/Family Planning Confirmation/Add Confirmation/Doctor/Combo - Benefit Coverage'))

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up - Provider Health Claim - Not Use/Follow Up Outbound/Family Planning Confirmation/Add Confirmation/Doctor/Combo List - Benefit Coverage',
						[('value') : BenefitCoverageFamilyPlanning]))

				if (Remarks == 'Yes') {
					WebUI.setText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Doctor/Input - Remarks'),
						RemarksValue)
				}
				
				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Doctor/Button - Save'))

				GEN5.ProcessingCommand()

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Button - OK'))
			} else if ((PICFamilyPlanning[i]) == 'Head Contact Center') {
				//Head CCO
				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Head CCO/Combo - Head CCO'))

				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Head CCO/Combo List - Head CCO',
						[('value') : HeadCCOFamilyPlanning]))

//                if (EditDateTimeConfirmation == 'Yes') {
//                    WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Head CCO/Date Picker - Date Time Confirmation'))
//
//                    GEN5.DatePicker(DTC, null)
//                }
				
				//Confirmation
				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Head CCO/Combo - Confirmation'))
				
				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Head CCO/Combo List - Confirmation',
					[('value') : ConfirmationFamilyPlanning[i]]))
				
				//Channel
				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Head CCO/Combo - Channel'))
				
				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Head CCO/Combo List - Channel',
					[('value') : ChannelFamilyPlanning]))
				
				//Benefit Coverage
				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Head CCO/Combo - Benefit Coverage'))
				
				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Head CCO/Combo List - Benefit Coverage',
					[('value') : BenefitCoverageFamilyPlanning]))
				
				//Action Diagnosa
				WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Family Planning Confirmation/Add Confirmation/Head CCO/Button - Save'))
				
				GEN5.ProcessingCommand()
			}
		} else if (FamilyPlanningConfirmation == 'Edit') {
		} else if (FamilyPlanningConfirmation == 'Delete') {
		}
	}
}

WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Button - Save'))

//Information - Apakah anda yakin ingin melanjutkan proses ini ?
WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Information/Button - Yes'))

GEN5.ProcessingCommand()

//Preview Document
def PreviewDocument = WebUI.waitForElementVisible(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Preview Document/Pop Up - Preview Document'), 
    GlobalVariable.Delay1, FailureHandling.OPTIONAL)

if (PreviewDocument) {
    WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Preview Document/Button - Preview Documents', 
            [('value') : ActionPreviewDocumentFU]))

    GEN5.ProcessingCommand()
}

//Summary
def ValidasiSize = Validasi.size()

for (i = 0; i < ValidasiSize; i++) {
    WebUI.waitForElementVisible(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Summary/Text - Validasi', 
            [('value') : Validasi[i]]), 1)
}

WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/Summary/Button - Close'))

//Final Confirmation - Apakah terdapat konfirmasi provider yang ingin ditambahkan pada transaksi ini?
def FinalConfirmation = WebUI.waitForElementVisible(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Final Confirmation/Pop Up - Final Confirmation'), 
    GlobalVariable.Delay1, FailureHandling.OPTIONAL)

if (FinalConfirmation) {
    WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Final Confirmation/Button - Final Confirmation', 
            [('value') : FinalConfirmationFU]))

    GEN5.ProcessingCommand()
}

//Exit
WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Inquiry/Button - Exit'))

WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Inquiry/Exit Confirmation/Button - Yes'))