import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5 as GEN5
import com.keyword.UI as UI
import gardaAkses.HandleTicketFU as FU


if ((DiagnosisConfirmation) == 'New') {
	WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Button - New Diagnosis Confirmation'))

	GEN5.ProcessingCommand()

	//PIC
	WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Combo - PIC'))

	WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Combo List - PIC',
			[('value') : PICDiagnosis]))

	GEN5.ProcessingCommand()

	if ((PICDiagnosis) == 'Account Manager') {
		//Account Manager Name
		WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Account Manager/Drop Down - Account Manager Name'))

		WebUI.setText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Account Manager/Input - Account Manager Name'),
			AccountManagerNameDiagnosis)

		WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Account Manager/Select - Account Manager Name',
				[('value') : AccountManagerNameDiagnosis]))

		//Date Time Confirmation
		WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Account Manager/Select - Date Time Confirmation'))

		WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Account Manager/Select - Date Time Confirmation'))

		//Confirmation
		WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Account Manager/Drop Down - Confirmation'))

		WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Account Manager/Select - Confirmation',
				[('value') : ConfirmationDiagnosis]))

		//Channel
		WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Account Manager/Drop Down - Channel'))

		WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Account Manager/Select - Channel',
				[('value') : ChannelDiagnosis]))

		//Benefit Coverage
		WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Account Manager/Drop Down - Benefit Coverage'))

		WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Account Manager/Select - Benefit Coverage',
				[('value') : BenefitCoverageDiagnosis]))

		//Remarks
		WebUI.setText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Account Manager/Input - Remarks'),
			RemarksFUDiagnosis)

		//Action Diagnosa
		WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Account Manager/Button - Action',
				[('value') : ActionFUDiagnosis]))

		WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Account Manager/Button - OK' //Head CCO
				))
	} else if ((PICDiagnosis) == 'Provider') {
		WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Provider/Combo - Confirmation'))

		WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Provider/Combo List - Confirmation',
				[('value') : ConfirmationDiagnosis]))

		WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Provider/Combo - Channel'))

		WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Provider/Combo List - Channel',
				[('value') : ChannelDiagnosis]))

		WebUI.scrollToElement(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Button - Save'),
			2)

		WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Button - Save'))

		WebUI.waitForElementVisible(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Text - Insert Successfully'),
			1)

		GEN5.ProcessingCommand()

		WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Button - OK'))
	} else if ((PICDiagnosis) == 'Contact Center') {
		WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Contact Center/Combo List - Contact Center Name'))

		WebUI.setText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Contact Center/Input - Contact Center Name'),
			UserID)

		WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Contact Center/List - Contact Center Name',
				[('value') : UserID]))

		WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Contact Center/Date Picker - Date Time Confirmation'))

		WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Contact Center/Combo List - Confirmation'))

		WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Contact Center/List - Confirmation',
				[('value') : ConfirmationDiagnosis]))

		WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Contact Center/Combo List - Benefit Coverage'))

		WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Contact Center/List - Benefit Coverage',
				[('value') : BenefitCoverageDiagnosis]))

		WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Contact Center/Button - Save'))

		GEN5.ProcessingCommand()

		WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Contact Center/Button - OK'))
	} else if ((PICDiagnosis) == 'Doctor') {
		WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Doctor/Combo - Doctor Name'))

		WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Doctor/Combo List - Doctor Name',
				[('value') : DoctorNameDiagnosis]))

		WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Doctor/Combo - Confirmation'))

		WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Doctor/Combo List - Confirmation',
				[('value') : ConfirmationDiagnosis[i]]))

		WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Doctor/Combo - Channel'))

		WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Doctor/Combo List - Channel',
				[('value') : ChannelDiagnosis]))

		WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up - Provider Health Claim - Not Use/Follow Up Outbound/Diagnosis Confirmation/Add Confirmation/Doctor/Combo - Benefit Coverage'))

		WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up - Provider Health Claim - Not Use/Follow Up Outbound/Diagnosis Confirmation/Add Confirmation/Doctor/Combo List - Benefit Coverage',
				[('value') : BenefitCoverageDiagnosis]))

		if (Remarks == 'Yes') {
			WebUI.setText(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Doctor/Input - Remarks'),
				RemarksValue)
		}
		
		WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Doctor/Button - Save'))

		GEN5.ProcessingCommand()

		WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Button - OK'))
	} else if ((PICDiagnosis[i]) == 'Head Contact Center') {
		WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Head CCO/Combo - Head CCO'))

		WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Head CCO/Combo List - Head CCO',
				[('value') : HeadCCODiagnosis]))

		WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Head CCO/Combo - Confirmation'))

		WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Head CCO/Combo List - Confirmation',
				[('value') : ConfirmationDiagnosis[i]]))

		WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Head CCO/Combo - Channel'))

		WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Head CCO/Combo List - Channel',
				[('value') : ChannelDiagnosis]))

		WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Head CCO/Combo - Benefit Coverage'))

		WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Head CCO/Combo List - Benefit Coverage',
				[('value') : BenefitCoverageDiagnosis]))

		WebUI.click(findTestObject('Pages/Web/GardaAkses/Follow Up/Follow Up Claim/FU Outbound/Diagnosis Confirmation/Add Confirmation/Head CCO/Button - Save'))

		GEN5.ProcessingCommand()
	}
} else if ((DiagnosisConfirmation[i]) == 'Edit') {
} else if ((DiagnosisConfirmation[i]) == 'Delete') {
}