import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5 as GEN5
import com.keyword.UI as UI

//Apakah ada hal lain yang dapat dibantu, Pak/Bu?
if (ECAction1 == 'Ya') {
    WebUI.click(findTestObject('Pages/Web/GardaAkses/Exit Confirmation/Button - Ya'))
	
	GEN5.ProcessingCommand()
} else if (ECAction1 == 'Tidak') {
    WebUI.click(findTestObject('Pages/Web/GardaAkses/Exit Confirmation/Button - Tidak'))
	
	GEN5.ProcessingCommand()
	
    if (ECAction2 == 'Puas') {
        WebUI.click(findTestObject('Pages/Web/GardaAkses/Exit Confirmation/Button - Puas'))

        WebUI.click(findTestObject('Pages/Web/GardaAkses/Exit Confirmation/Button - OK'))

        WebUI.click(findTestObject('Pages/Web/GardaAkses/Exit Confirmation/Button - OK')) //Terima kasih atas kesediaannya menghubungi GARDA AKSES. Selamat Pagi/Siang/Malam
		
		GEN5.ProcessingCommand()
		
		def PopUpErrorCTI = WebUI.waitForElementVisible(findTestObject('Pages/Web/GardaAkses/Create Ticket/Pop Up - 14324'), GlobalVariable.Delay1)
		
		if (PopUpErrorCTI) {
			WebUI.click(findTestObject('Pages/Web/GardaAkses/Create Ticket/Button - Ignore'))
		}
		
		WebUI.waitForElementVisible(findTestObject('Pages/Web/GardaAkses/Create Ticket/Button - Exit'), 20)

        GEN5.ProcessingCommand()

        WebUI.click(findTestObject('Pages/Web/GardaAkses/Create Ticket/Button - Exit'))

        WebUI.click(findTestObject('Pages/Web/GardaAkses/Create Ticket/Please Confirm/Button - Yes, Close Application')) //Close application and go back to Menu page?
    } else if (ECAction2 == 'Tidak Puas') {
        WebUI.setText(findTestObject('Pages/Web/GardaAkses/Exit Confirmation/Input - Comment'), Comment)

        WebUI.click(findTestObject('Pages/Web/GardaAkses/Exit Confirmation/Button - OK'))

        WebUI.click(findTestObject('Pages/Web/GardaAkses/Exit Confirmation/Button - OK'))

        WebUI.click(findTestObject('Pages/Web/GardaAkses/Exit Confirmation/Button - OK'))

        WebUI.click(findTestObject('Pages/Web/GardaAkses/Create Ticket/Button - Exit'))

        WebUI.click(findTestObject('Pages/Web/GardaAkses/Create Ticket/Please Confirm/Button - Yes, Close Application'))
    }
}

