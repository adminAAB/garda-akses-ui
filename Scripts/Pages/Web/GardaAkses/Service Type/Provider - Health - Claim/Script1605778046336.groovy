import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys1
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.By as By
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.keyword.GEN5 as GEN5
import com.keyword.UI as UI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

//========== Patient Information ==========
//Member
def PopUpNonClient = WebUI.waitForElementVisible(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Pop Up Non Client/Text - Non Client'), 
    3)

if (PopUpNonClient) {
    WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Pop Up Non Client/Button - Close'))
}

if (Member == 'Existing') {
    CustomKeywords.'gardaAkses.CreateGL.InputMemberName'(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Patient Information/Input - Member'), 
        MemberName)
} else if (Member == 'New') {
    WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Patient Information/Button - Add New Member'))

    WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Patient Information/Add New Member/Button - New Member Type'))

    WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Patient Information/Add New Member/Choose - New Member Type', 
            [('Value') : NewMemberType]))

    if (NewMemberType == 'Employee') {
        WebUI.setText(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Patient Information/Add New Member/Input - New Member Name'), 
            NewMemberName)

        WebUI.setText(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Patient Information/Add New Member/Input - Client Name'), 
            ClientName)

        WebUI.delay(5)

        WebUI.setText(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Patient Information/Add New Member/Input - Client Name'), 
            ClientName)

        WebUI.delay(5)

        WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Patient Information/Add New Member/Choose - Client Name'))

        WebUI.setText(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Patient Information/Add New Member/Input - Employee ID'), 
            EmployeeID)

        WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Patient Information/Add New Member/Date Picker - DOB'))

        GEN5.DatePicker(DOB, findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Patient Information/Add New Member/Date Picker - DOB Month'))

        WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Patient Information/Add New Member/Button - Classification'))

        WebDriver driver = DriverFactory.getWebDriver()

        WebUI.switchToFrame(findTestObject('Pages/Web/GEN5/Frame'), 3)

        WebElement Classification = driver.findElement(By.xpath('//*[@id="popUpAddNewMembership-0"]/div[1]/div/div[1]/a2is-combo/div/div/div/div'))

        List<WebElement> ClassificationList = Classification.findElements(By.tagName('button'))

        WebUI.switchToDefaultContent()

        String SelectClassificationList = ClassificationList.size()

        println(SelectClassificationList)

        WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Patient Information/Add New Member/Choose - Classification', 
                [('value') : SelectClassificationList]))

        WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Patient Information/Add New Member/Button - Gender', 
                [('Value') : Gender]))

        WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Patient Information/Add New Member/Button - Submit'))
    } else if (NewMemberType == 'Spouse') {
        WebUI.setText(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Patient Information/Add New Member/Input - Employee Name'), 
            NewMemberEmp)

        WebUI.delay(5)

        WebUI.setText(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Patient Information/Add New Member/Input - Employee Name'), 
            NewMemberEmp)

        WebUI.delay(5)

        WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Patient Information/Add New Member/Choose - Employee Name'))

        WebUI.delay(5)

        WebUI.setText(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Patient Information/Add New Member/Input - New SPO Name'), 
            NewMemberName)

        WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Patient Information/Add New Member/Date Picker - DOB'))

        GEN5.DatePicker(DOB, findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Patient Information/Add New Member/Date Picker - DOB Month'))

        WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Patient Information/Add New Member/Button - Gender', 
                [('Value') : Gender]))

        WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Patient Information/Add New Member/Button - Submit'))
    } else if (NewMemberType == 'Child') {
        WebUI.setText(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Patient Information/Add New Member/Input - Employee Name'), 
            NewMemberEmp)

        WebUI.delay(5)

        WebUI.setText(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Patient Information/Add New Member/Input - Employee Name'), 
            NewMemberEmp)

        WebUI.delay(5)

        WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Patient Information/Add New Member/Choose - Employee Name'))

        WebUI.delay(5)

        WebUI.setText(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Patient Information/Add New Member/Input - New SPO Name'), 
            NewMemberName)

        WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Patient Information/Add New Member/Date Picker - DOB'))

        GEN5.DatePicker(DOB, findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Patient Information/Add New Member/Date Picker - DOB Month'))

        WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Patient Information/Add New Member/Button - Gender', 
                [('Value') : Gender]))

        WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Patient Information/Add New Member/Button - Submit'))
    }
} else if (Member == 'Check New Member') {
    def ExistingNewMemberName = WebUI.getAttribute(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Patient Information/Add New Member/Input - New Member Name'), 
        'value')

    WebUI.verifyMatch(ExistingNewMemberName, GlobalVariable.NewMemberName, false)
} else if (Member == 'Check Member') {
    def ExistingMemberName = WebUI.getAttribute(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Patient Information/Input - Member'), 
        'value')

    WebUI.verifyMatch(ExistingMemberName, MemberName, false)
}

def PopUpNonClient3 = WebUI.waitForElementVisible(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Pop Up Non Client/Text - Non Client'), 
    3)

if (PopUpNonClient3) {
    WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Pop Up Non Client/Button - Close'))
}

GlobalVariable.NewMemberName = WebUI.getAttribute(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Patient Information/Add New Member/Input - New Member Name'), 
    'value')

//Patient / Family Phone No
CustomKeywords.'gardaAkses.General.UpdateFieldText'(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Patient Information/Input - Patient, Family Phone No'), 
    GlobalVariable.PhoneNumber)

//Product Type
CustomKeywords.'gardaAkses.General.UpdateFieldCombo'(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Patient Information/Button - Product Type'), 
    findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Patient Information/Choose - Product Type', 
        [('Value') : ProductType]), ProductType)

//GL Type
CustomKeywords.'gardaAkses.General.UpdateFieldCombo'(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Patient Information/Button - GL Type'), 
    findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Patient Information/Choose - GL Type', 
        [('Value') : GLType]), GLType)

//Treatment Period Start
if (EditTreatmentPeriodStart == 'Yes') {
    WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Patient Information/Date Picker - Treatment Period Start'))

    GEN5.DatePicker(TreatmentPeriodStart, findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Patient Information/Date Picker - Treatment Period Start Month'))
}

//Treatment Period End
if ((EditTreatmentPeriodEnd == 'Yes') && (GLType == 'Akhir')) {
    WebUI.click(null)
}

//Treatment Duration
if ((EditTreatmentDuration == 'Yes') && GLType == 'Akhir') {
    GEN5.DeleteWrite(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Patient Information/Input - Treatment Duration'), 
        TreatmentDuration)
}

//Special Condition
if (SpecialCondition == 'Yes') {
    WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Patient Information/Check Box - Special Condition'))

    WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Patient Information/Button - Reason Special Condition'))

    WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Patient Information/Choose - Reason Special Condition', 
            [('ReasonSpecialCondition') : ReasonSpecialCondition]))
}

//ODS ODC
if (IsODSODC == 'ODS') {
    WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Patient Information/Check Box - ODS'))
} else if (IsODSODC == 'ODC') {
    WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Patient Information/Check Box - ODC'))
}

//========== Provider Information ==========
def ProviderName = WebUI.getAttribute(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Provider Information/Input - Provider Name'), 
    'value')

if ((ProviderName == '') || (ProviderName == null)) {
}

//================= Diagnosis =================
//Diagnosis
//===New===
int RepeatDiagnosa = DiagnosisID.size()

for (i = 0; i < RepeatDiagnosa; i++) {
    if ((Diagnosis[i]) == 'New') {
        WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Diagnosis/Button - Create New'))

        //Status
        WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Diagnosis/Create New/Combo - Status'))

        WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Diagnosis/Create New/Combo List - Status', 
                [('value') : StatusDiagnosa[i]]))

        //ID/Name Diagnosa
        WebUI.setText(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Diagnosis/Create New/Input - ID Diagnosa'), 
            DiagnosisID[i])

        WebUI.delay(GlobalVariable.Delay1)

        WebUI.setText(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Diagnosis/Create New/Input - ID Diagnosa'), 
            DiagnosisID[i])

        WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Diagnosis/Create New/Auto Complete - ID or Name'))

        GEN5.ProcessingCommand()

        //Gravida
        if ((ProductType == 'Maternity (Persalinan)') && DiagnosisID[i].contains('O8')) {
            WebUI.setText(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Diagnosis/Create New/Input - Gravida'), 
                Gravida)
        }
        
        //Partus
        if ((ProductType == 'Maternity (Persalinan)') && DiagnosisID[i].contains('O8')) {
            WebUI.setText(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Diagnosis/Create New/Input - Partus'), 
                Partus)
        }
        
        //Abortus
        if ((ProductType == 'Maternity (Persalinan)') && DiagnosisID[i].contains('O8')) {
            WebUI.setText(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Diagnosis/Create New/Input - Abortus'), 
                Abortus)
        }
        
        //Gestational Age (Usia Kandungan)
        if ((ProductType == 'Maternity (Persalinan)') && DiagnosisID[i].contains('O8')) {
            WebUI.setText(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Diagnosis/Create New/Input - Gestational Age'), 
                GestationalAge)
        }
        
        //Remarks Diagnosis
        WebUI.setText(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Diagnosis/Create New/Input - Remarks'), 
            RemarksDiagnosa)

        GEN5.ProcessingCommand()

        //Diagnosis Question
        if (((DiagnosisID[i]) == 'O82') || ((DiagnosisID[i]) == 'O84.2')) {
            WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Diagnosis/Create New/Combo - Diagnosis Question'))

            WebUI.setText(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Diagnosis/Create New/Input - Diagnosis Question'), 
                DiagnosisQuestion)

            WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Diagnosis/Create New/Combo List - Diagnosis Question', 
                    [('value') : DiagnosisQuestion]))

            WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Diagnosis/Create New/Button - Select Diagnosis Question'))
        }
        
        //Additional Info
        WebDriver driver = DriverFactory.getWebDriver()

        def ColoumnDiagnosisQuestion = WebUI.waitForElementVisible(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Diagnosis/Create New/Table - Diagnosis Question'), 
            GlobalVariable.Delay1)

        WebUI.switchToFrame(findTestObject('Pages/Web/GEN5/Frame'), 1)

        if (ColoumnDiagnosisQuestion) {
            WebElement diagnosis = driver.findElement(By.xpath('//*[@id="DiagnosisQuestion"]/div[2]/a2is-datatable/div[2]/div/table/tbody'))

            List<WebElement> additional_info = diagnosis.findElements(By.tagName('tr'))

            WebUI.switchToDefaultContent()

            int yes = additional_info.size()

            int i = 1

            for (a = 1; a <= yes; a++) {
                WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Diagnosis/Create New/Radio Button - Yes', 
                        [('Value') : a]), FailureHandling.OPTIONAL)

                WebUI.delay(1)
            }
        }
        
        WebUI.switchToDefaultContent()

        //Delete Diagnosis Question
        //Action Add Diagnosis
        WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Diagnosis/Create New/Button - Submit'))
		
		GEN5.ProcessingCommand()
    } else if ((Diagnosis[i]) == 'Edit') {
    } else if ((Diagnosis[i]) == 'Delete') {
    }
}

//========== Treatment Information ==========
//Remarks
UI.DeleteWrite(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Treatment Information/Input - Remarks'), 
    RemarksTreatmentInformation)

//Maternity Treatment
if (ProductType == 'Maternity (Persalinan)') {
    WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Treatment Information/Combo - Maternity Treatment'))

    WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Treatment Information/Combo List - Maternity Treatment', 
            [('value') : MaternityTreatment]))
}

//Doctor
CustomKeywords.'gardaAkses.General.UpdateFieldText'(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Treatment Information/Input - Doctor'), 
    DoctorName)

//Account Manager
if (Member == 'Check New Member') {
    WebUI.setText(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Treatment Information/Input - Account Manager'), 
        AccountManager)
}

//Rujuk
if (Rujuk == 'Yes') {
	WebUI.click(findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Treatment Information/Check Box - Rujuk'))
	
	WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Treatment Information/Combo - Reason'))

    WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Treatment Information/Combo List - Reason', 
            [('Value') : ReasonReferral]))
}

//Family Planning
//======================= Family Planning ============
if (ProductType == 'Maternity (Persalinan)'){
if (AddFP == 'Yes') {
    int RepeatFamilyPlanning = FamilyPlanning.size()

    for (i = 0; i < RepeatFamilyPlanning; i++) {
        if ((FamilyPlanningItem[i]) == 'New') {
            WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Family Planning Item/Button - Create New'))

            //Diagnosis
            WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Family Planning Item/Create New/Combo - FPItem'))

            WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Family Planning Item/Create New/Combo List - FPItem', 
                    [('value') : FamilyPlanning[i]]))

            WebUI.setText(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Family Planning Item/Create New/Input - FPBilled'), 
                FPBilled)

            GEN5.ProcessingCommand()

            //Action Add Medical Treatment
            WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Family Planning Item/Create New/Button - Save'))
        } else if ((FamilyPlanningItem[i]) == 'Delete') {
        }
    }
}
}

//FP
//======================= Medical Treatment ============
int RepeatMedicalTreatment = MedicalTreatment.size()

for (i = 0; i < RepeatMedicalTreatment; i++) {
    if ((MTTindakanMedis[i]) == 'New') {
        WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Medical Treatment/Button - Create New'))

        //Diagnosis
        WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Medical Treatment/Create New/Combo - MTDiagnosis'))

        WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Medical Treatment/Create New/Combo List - MTDiagnosis', 
                [('value') : MTDiagnosis[i]]))

        //Medical Treatment
        if ((MedicalTreatment[i]) == 'Cryotherapy / Cryosurgery') {
            WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Medical Treatment/Create New/Combo - Medical Treatment'))

            WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Medical Treatment/Create New/Combo List - Medical Treatment', 
                    [('value') : MedicalTreatment[i]]))

            WebUI.setText(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Medical Treatment/Create New/Input - Billed'), 
                Billed //unregistered
                )
        } else {
            WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Medical Treatment/Create New/Combo - Medical Treatment'))

            WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Medical Treatment/Create New/Combo - Medical Treatment'))

            WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Medical Treatment/Create New/CheckBox - UnregisteredMT'))

            WebUI.setText(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Medical Treatment/Create New/Input - UnregisteredMT'), 
                UnregisteredMT)

            WebUI.setText(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Medical Treatment/Create New/Input - Billed'), 
                Billed)
        }
        
        // medical treatment question
        //Additional Info
        WebDriver driver = DriverFactory.getWebDriver()

        def ColoumnDiagnosisTreatmentQuestion = WebUI.waitForElementVisible(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Medical Treatment/Create New/Table - Diagnosis Treatment Question'), 
            GlobalVariable.Delay1)

        WebUI.switchToFrame(findTestObject('Pages/Web/GEN5/Frame'), 1)

        if (ColoumnDiagnosisTreatmentQuestion) {
            WebElement diagnosistreatment = driver.findElement(By.xpath('//*[@id="DiagnosisTreatmentQuestion"]/a2is-datatable/div[2]/div'))

            List<WebElement> additional_info = diagnosistreatment.findElements(By.tagName('tr'))

            WebUI.switchToDefaultContent()

            int yes = additional_info.size()

            int i = 1

            for (a = 1; a <= yes; a++) {
                WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Medical Treatment/Create New/Radio Button - Yes', 
                        [('Value') : a]), FailureHandling.OPTIONAL)

                WebUI.delay(1)
            }
        }
        
        WebUI.switchToDefaultContent()

        GEN5.ProcessingCommand()

        //Action Add Medical Treatment
        WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Medical Treatment/Create New/Button - Save'))
    } else if ((MTTindakanMedis[i]) == 'Edit') {
    } else if ((MTTindakanMedis[i]) == 'Delete') {
    }
}

//=======================================================
//========== Room Information ==========
//Appropriate RB Class
String getValueAppropriateRBClass = WebUI.getAttribute(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Room Information/Input - Appropriate RB Class - Value'), 
    'value')

if (getValueAppropriateRBClass != AppropriateRBClass) {
    WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Room Information/Look Up - Appropriate RB Class'))

    GEN5.ProcessingCommand()

    WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Room Information/Appropriate RB Class/Select - Appropriate RB Class', 
            [('Value') : AppropriateRBClass]))

    WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Room Information/Appropriate RB Class/Button - Choose'))
}

String getValueAppropriateRBRate = WebUI.getAttribute(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Room Information/Input - Appropriate RB Rate - Value'), 
    'value')

if (((IsODSODC == 'ODS') || 'ODC') && (getValueAppropriateRBRate == '')) {
    WebUI.setText(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Room Information/Input - Appropriate RB Rate'), 
        AppropriateRBRate)
}

//Treatment RB Class (Ruang Kelas Perawatan)
String getValueTreatmentRBClass = WebUI.getAttribute(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Room Information/Input - Treatment RB Class - Value'), 
    'value')

if (getValueTreatmentRBClass != TreatmentRBClass) {
    WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Room Information/Look Up - Treatment RB Class'))

    GEN5.ProcessingCommand()

    WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Room Information/Treatment RB Class/Select - Treatment RB Class', 
            [('Value') : TreatmentRBClass]))

    WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Room Information/Treatment RB Class/Button - Choose'))
}

String getValueTreatmentRBRate = WebUI.getAttribute(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Room Information/Input - Treatment RB Rate - Value'), 
    'value')

if (((IsODSODC == 'ODS') || 'ODC') && (getValueTreatmentRBRate == '')) {
    WebUI.setText(findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Room Information/Input - Treatment RB Rate'), 
        TreatmentRBRate)
}

//Room Option / Availability
CustomKeywords.'gardaAkses.General.UpdateFieldCombo'(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Room Information/Combo - Room Option Availability'), 
    findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Room Information/Combo List - Room Option Availability', 
        [('Value') : RoomOptionAvailability]), RoomOptionAvailability)

//Package Price
if (ProductType == 'Maternity (Persalinan)') {
    WebUI.setText(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Room Information/Input - Package Price'), 
        PackagePrice)
}

//Total Billed
if (GLType == 'Akhir') {
    WebUI.setText(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Room Information/Input - Total Billed'), 
        TotalBilled)
}

if (NeedFollowUp == 'Yes') {
    WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/CheckBox - Need Follow Up'))
}

//========== Non Medical Item ===============
if (NonMedicalItem == 'Yes') {
    WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Non Medical Item/CheckBox - Non Medical Item'))
}

//========== Document Management ==========
//New Document
if (NewDocument == 'Yes') {
    int RepeatDocument = DocumentType.size()

    for (i = 0; i < RepeatDocument; i++) {
        if ((UploadDocument[i]) == 'New') {
            WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Document Management/Button - Create New Document Management'))

            GEN5.ProcessingCommand()

            WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Document Management/Upload Document/Combo - Doc Type'))

            WebUI.setText(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Document Management/Upload Document/Input - Doc Type'), 
                InputDocType[i])

            WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Document Management/Upload Document/Combo - Select Doc Type', 
                    [('value') : DocumentType[i]]))

            WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Document Management/Upload Document/Combo - Doc Source'))

            WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Document Management/Upload Document/Combo - Select Doc Source', 
                    [('value') : DocSource]))

            WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Document Management/Upload Document/Combo - Doc Validity'))

            WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Document Management/Upload Document/Combo - Select Doc Validity', 
                    [('value') : DocValidity]))

            WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Document Management/Upload Document/Button - Upload'))

            GEN5.UploadFile(FileLocation, FileName)

            WebUI.delay(2)

            WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Document Management/Upload Document/Button - Save'))

            WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Document Management/Upload Document/Button - Information Upload Complete'))
        }
    }
}

//Edit Document
if (EditDocument == 'Yes') {
    WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Document Management/Button - Edit Document Management'))
}

//Delete Document
if (DeleteDocument == 'Yes') {
    WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Document Management/Button - Delete Document Management'))
}

//Action
if (ActionGL == 'Cancel') {
    //Pop Up Pre-Admission
    //Pop Up Client Non GL - Need FU
    //Claim Outstanding
    //Preview Document
} else if (ActionGL == 'Interupted Call') {
} else if (ActionGL == 'Pre-Admission') {
    WebUI.scrollToElement(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Button - Pre-Admission'), 
        5)

    WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Button - Pre-Admission'))

    def PopUpConfirmationPreAdmission = WebUI.waitForElementVisible(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Pop Up Pre-Admission/Pop Up - Confirmation Pre-Admission'), 
        GlobalVariable.Delay2)

    if (PopUpConfirmationPreAdmission) {
        if (FUPreAdm == 'Ya') {
            WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Pop Up Pre-Admission/Button - Ya'))
        } else if (FUPreAdm == 'Tidak') {
            WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Pop Up Pre-Admission/Button - Tidak'))
        } else if (FUPreAdm == 'Cancel') {
            WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Pop Up Pre-Admission/Button - Cancel'))
        }
    }
    
    GEN5.ProcessingCommand()

    WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Button - Close'))

    CustomKeywords.'gardaAkses.GetTicketID.ProviderHealthClaim'(Phase)
} else if (ActionGL == 'Process') {
    WebUI.scrollToElement(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Button - Process'), 
        5)
	
	GEN5.ProcessingCommand()

    WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Button - Process'))
	
	GEN5.ProcessingCommand()

    def PopUpProcessConfirmation = WebUI.waitForElementVisible(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Process Confirmation/Pop Up - Process Confirmation'), 
        GlobalVariable.Delay1)

    if (PopUpProcessConfirmation) {
        if (NeedFU == 'Ya') {
            WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Pop Up Non Client - Uncheck Need FU/Button - Ya'))
        } else if (NeedFU == 'Tidak') {
            WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Pop Up Non Client - Uncheck Need FU/Button - Tidak'))
        } else if (NeedFU == 'Cancel') {
            WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Pop Up Non Client - Uncheck Need FU/Button - Cancel'))
        }
    }
    
    GEN5.ProcessingCommand()

    def PopUpClaimOS = WebUI.waitForElementVisible(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Claim Out Standing/Pop Up - Claim OS'), 
        GlobalVariable.Delay1)

    if (PopUpClaimOS) {
        WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Claim Out Standing/Button - Tidak'))
    }
    
    GEN5.ProcessingCommand()

    def PopUpPreviewDocument = WebUI.waitForElementVisible(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Preview Document/Pop Up - Preview Document'), 
        GlobalVariable.Delay3)

    if (PopUpPreviewDocument) {
        if (PreviewDocument == 'Process') {
            WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Preview Document/Button - Process'))
        } else {
            WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Preview Document/Button - Close'))
        }
    }
    
    GEN5.ProcessingCommand()

    def Repeat = Validasi.size()

    for (i = 0; i < Repeat; i++) {
        WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Text - Validasi', 
                [('Value') : Validasi[i]]))
    }
    
    WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Button - Close'))

    CustomKeywords.'gardaAkses.GetTicketID.ProviderHealthClaim'(Phase)
} else if (ActionGL == 'Reject') {
    WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Button - Reject'))

    def Repeat = Validasi.size()

    for (i = 0; i < Repeat; i++) {
        WebUI.getText(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Text - Validasi', 
                [('Value') : Validasi[i]]))
    }
    
    WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Provider - Health - Claim/Button - Close'))
}