import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5 as GEN5
import com.keyword.UI as UI

GEN5.ProcessingCommand()

def ProductVisible = WebUI.waitForElementVisible(findTestObject('Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Combo - Product'), 
    GlobalVariable.Delay1)

while (!(ProductVisible)) {
    GEN5.ProcessingCommand()

    ProductVisible = WebUI.waitForElementVisible(findTestObject('Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Combo - Product'), 
        GlobalVariable.Delay1)
}

//Product
WebUI.click(findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Combo - Product'))

WebUI.click(findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Combo List - Product', 
        [('value') : product]))

//Claim MV
//Claim
WebUI.click(findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Tab - Claim'))

GEN5.ProcessingCommand()

//Policy has been created
WebUI.click(findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Claim/RadioButton - PolicyHasBeenCreated', 
        [('value') : policyHasBeenCreated]))

//Vehicle
WebUI.click(findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Claim/Combo - Vehicle'))

WebUI.click(findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Claim/Combo List - Vehicle', 
        [('value') : Parameter]))

WebUI.scrollToElement(findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Combo - Product'),
	GlobalVariable.Delay1)

//Location Of Vehicle
WebUI.click(findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Claim/Look Up - Location Of Vehicle'))

WebUI.click(findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Claim/Location Of Vehicle/Combo - Search By'))

WebUI.click(findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Claim/Location Of Vehicle/Combo List - Search By', 
        [('value') : searchBy]))

WebUI.setText(findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Claim/Location Of Vehicle/Input - Search By'), 
    valueSearchBy)

WebUI.click(findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Claim/Location Of Vehicle/Button - Search'))

WebUI.click(findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Claim/Location Of Vehicle/Select - Location Of Vehicle'))

WebUI.click(findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Claim/Location Of Vehicle/Button - Add'))

//Location of Accident
WebUI.setText(findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Claim/Input - Location of Accident'), 
    locationOfAccident)

//Accident Information
WebUI.setText(findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Claim/Input - Accident Information'), 
    accidentInformation)

//Cause of Loss
WebUI.click(findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Claim/Combo - Cause of Loss'))

WebUI.click(findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Claim/Combo List - Cause of Loss', 
        [('value') : causeOfLoss]))

GEN5.ProcessingCommand()

boolean popUpInformation = WebUI.waitForElementVisible(findTestObject('Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Claim/Pop Up Information/Pop Up - Information'), 
    GlobalVariable.Delay1)

if (popUpInformation) {
    WebUI.click(findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Claim/Pop Up Information/Button - Close'))
}

GEN5.ProcessingCommand()

//Survey Type
WebUI.scrollToElement(findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Combo - Product'),
	GlobalVariable.Delay1)

WebUI.click(findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Claim/Combo - Survey Type'))

WebUI.click(findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Claim/Combo List - Survey Type', 
        [('value') : surveyType]))

//Survey Area - Postal Code
WebUI.click(findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Claim/Look Up - Survey Area - Postal Code'))

WebUI.click(findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Claim/Survey Area - Postal Code/Combo - Search By'))

WebUI.click(findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Claim/Survey Area - Postal Code/ComboList - Search By', 
        [('value') : searchBy]))

WebUI.setText(findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Claim/Survey Area - Postal Code/Input - Search By'), 
    valueSearchBy)

WebUI.click(findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Claim/Survey Area - Postal Code/Button - Search'))

WebUI.click(findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Claim/Survey Area - Postal Code/Select - Survey Area - Postal Code'))

WebUI.click(findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Claim/Survey Area - Postal Code/Button - Add'))

GEN5.ProcessingCommand()

//Survey Schedule
WebUI.click(findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Claim/Button Date Picker - Survey Schedule'))

GEN5.DatePicker(dateSurveySchedule, findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Claim/Date Picker - Survey Schedule'))

GEN5.ProcessingCommand()

//Survey Category Time
WebUI.click(findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Claim/Combo - Survey Category Time'))

WebUI.click(findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Claim/Combo List - Survey Category Time', 
        [('value') : surveyCategoryTime]))

//Customer on Location
WebUI.setText(findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Claim/Input - Customer on Location'), 
    customerOnLocation)

//PIC Phone Number
WebUI.setText(findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Claim/Input - PIC Phone Number'), 
    picPhoneNumber)

//Survey Address
WebUI.setText(findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Claim/Input - Survey Address')
	, surveyAddress)

//Need Towing
WebUI.click(findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Claim/Radio Button - Need Towing', 
        [('value') : needTowing]))

//Towing
if (needTowing == 'Yes') {
	WebUI.scrollToElement(findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Combo - Product'), 
	    GlobalVariable.Delay1)
	
	WebUI.click(findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Tab - Towing'))
	
	GEN5.ProcessingCommand()
	
	//Registration Number
	String valueRegistrationNumber = WebUI.getAttribute(findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Towing/Input - Registration Number'), 
	    'value')
	
	if (valueRegistrationNumber == '') {
	    WebUI.setText(findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Towing/Input - Registration Number'), 
	        registrationNumber)
	}
	
	//Adjustment Category
	if (policyHasBeenCreated == 'No') {
		if (customerGOXC == 'Yes') {
			WebUI.click(findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Towing/Check Box - Customer GOXC is not found'))
		}
		
		if (vipRelatives == 'Yes') {
			WebUI.click(findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Towing/Check Box - VIP relatives'))
		}
	}
	
	//Mobile Phone 1
	WebUI.setText(findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Towing/Input - Mobile Phone 1'), 
	    mobilePhone1)
	
	//Location
	WebUI.click(findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Towing/Look Up - Location'))
	
	GEN5.ProcessingCommand()
	
	WebUI.setText(findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Towing/Location/Input - Location'), 
	    location)
	
	WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Towing/Location/Search - Location'))
	
	GEN5.ProcessingCommand()
	
	WebUI.setText(findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Towing/Location/Input - Detail Location'), 
	    detailLocation)
	
	WebUI.click(findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Towing/Location/Button - Submit'))
	
	GEN5.ProcessingCommand()
	
	//Destination
	WebUI.scrollToElement(findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Combo - Product'),
		GlobalVariable.Delay1)
	
	WebUI.click(findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Towing/Look Up - Destination'))
	
	GEN5.ProcessingCommand()
	
	WebUI.setText(findTestObject('Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Towing/Destination/Input - Destination'), destination)
	
	WebUI.click(findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Towing/Destination/Search - Destination'))
	
	GEN5.ProcessingCommand()
	
	WebUI.setText(findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Towing/Destination/Input - Detail Destination'), detailDestination)
	
	WebUI.click(findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Towing/Destination/Button - Submit'))
	
	GEN5.ProcessingCommand()
	
	//Request Category
	WebUI.click(findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Towing/Combo - Request Category'))
	
	WebUI.click(findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Towing/Combo List - Request Category', 
	        [('value') : requestCategory]))
}

//General
//FU Status
WebUI.click(findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/General/Combo - FU Status'))

WebUI.click(findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/General/Combo List - FU Status', 
        [('value') : fuStatus]))

//FU Remarks
WebUI.setText(findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/General/Input - FU Remarks'), 
    fuRemarks)

//Action
WebUI.click(findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Button - Action', 
	        [('value') : action]))

if (action == 'Save') {
    if (createLk == 'Yes') {
        WebUI.click(findTestObject('Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Pop Up - Confirmation Yes'))

        GEN5.ProcessingCommand()

        GlobalVariable.dataGasi = WebUI.getText(findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Pop Up - Ticket Created'))

        UI.ScreenShot(ContactName)

        WebUI.click(findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Button - OK'))
    } else if (createLk == 'No') {
        WebUI.click(findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Pop Up - Confirmation No'))

        GEN5.ProcessingCommand()

        GlobalVariable.dataGasi = WebUI.getText(findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Pop Up - Ticket Created'))

        UI.ScreenShot(ContactName)

        WebUI.click(findTestObject('Object Repository/Pages/Web/GardaAkses/Service Type/Customer - Non Health - ClaimMV/Button - OK'))
    }
}

